package com.alacritystudios.cryptotrace.util;

import android.arch.persistence.room.TypeConverter;

/**
 * @author Anuj Dutt
 */


public class TypeConverters {

    @TypeConverter
    public static Long convertSortOrderToLong(String sortOrder) {
        try {
            return Long.parseLong(sortOrder);
        } catch (NumberFormatException e) {
            return 0L;
        }
    }

    @TypeConverter
    public static String convertSortOrderToString(Long sortOrder) {
        try {
            return String.valueOf(sortOrder);
        } catch (NumberFormatException e) {
            return "0";
        }
    }
}
