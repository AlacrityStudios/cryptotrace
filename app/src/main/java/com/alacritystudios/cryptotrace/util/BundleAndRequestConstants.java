package com.alacritystudios.cryptotrace.util;

/**
 * @author Anuj Dutt
 *  Contains various bundle and request constants universal to the application.
 */

public class BundleAndRequestConstants {


    public static final int EXCHANGE_LIST_DIALOG_FRAGMENT_REQUEST_CODE = 1000;
}
