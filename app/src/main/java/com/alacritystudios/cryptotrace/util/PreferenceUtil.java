package com.alacritystudios.cryptotrace.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author Anuj Dutt
 *  A utility for updating and fetching preferences.
 */

public class PreferenceUtil {

    public static final String PREF_CONVERSION_CURRENCY_CHOICE = "pref_conversion_currency_choice";
    public static final String PREF_EXCHANGE_CHOICE = "pref_exchange_choice";


    public static void setConversionCurrencyChoice(Context mContext, String name) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putString(PREF_CONVERSION_CURRENCY_CHOICE, name).apply();
    }

    public static String getConversionCurrencyChoice(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getString(PREF_CONVERSION_CURRENCY_CHOICE, "USD");
    }

    public static void setExchangeChoice(Context mContext, String name) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putString(PREF_EXCHANGE_CHOICE, name).apply();
    }

    public static String getExchangeChoice(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getString(PREF_EXCHANGE_CHOICE, "CCCAGG");
    }
}
