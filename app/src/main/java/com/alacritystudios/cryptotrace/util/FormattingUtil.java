package com.alacritystudios.cryptotrace.util;

import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedPriceDetails;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedSocialStatsDetails;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedStatsWrapper;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyPriceFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyStatsFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyHistogramWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPriceCompleteDetailsWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencySocialStats;
import com.github.mikephil.charting.data.Entry;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * @author Anuj Dutt
 *         Utility class containing all formatters for data formating.
 */

public class FormattingUtil {

    public static CryptoCurrencyFormattedPriceDetails getFormattedCryptoCurrencyPrice(CryptoCurrencyPriceCompleteDetailsWrapperModel cryptoCurrencyPriceCompleteDetailsWrapperModel,
                                                                                      CryptoCurrencyPriceFragmentViewModel.CryptoCurrencyPriceInformationParams cryptoCurrencyPriceInformationParams) {

        CryptoCurrencyPriceCompleteDetailsWrapperModel.CurrencyDetailsRawModel currencyDetailsRawModel;
        CryptoCurrencyPriceCompleteDetailsWrapperModel.CurrencyDetailsDisplayModel currencyDetailsDisplayModel;
        CryptoCurrencyFormattedPriceDetails cryptoCurrencyFormattedPrice = new CryptoCurrencyFormattedPriceDetails();
        try {
            currencyDetailsRawModel = cryptoCurrencyPriceCompleteDetailsWrapperModel.rawCryptoCurrencyDetails.get(cryptoCurrencyPriceInformationParams.fromCurrency)
                    .get(cryptoCurrencyPriceInformationParams.toCurrency);
        } catch (Exception ex) {
            currencyDetailsRawModel = new CryptoCurrencyPriceCompleteDetailsWrapperModel.CurrencyDetailsRawModel();
        }
        try {
            currencyDetailsDisplayModel = cryptoCurrencyPriceCompleteDetailsWrapperModel.displayCryptoCurrencyDetails.get(cryptoCurrencyPriceInformationParams.fromCurrency)
                    .get(cryptoCurrencyPriceInformationParams.toCurrency);
        } catch (Exception ex) {
            currencyDetailsDisplayModel = new CryptoCurrencyPriceCompleteDetailsWrapperModel.CurrencyDetailsDisplayModel();
        }
        try {
            cryptoCurrencyFormattedPrice.price = NumberFormat.getNumberInstance().format(currencyDetailsRawModel.price);
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.price = "0.0";
        }
        try {
            cryptoCurrencyFormattedPrice.changePercent = currencyDetailsDisplayModel.changePct24Hour;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.changePercent = "0.0%";
        }
        try {
            cryptoCurrencyFormattedPrice.change = currencyDetailsDisplayModel.change24Hour;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.change = "0.0";
        }
        try {
            cryptoCurrencyFormattedPrice.changeDouble = currencyDetailsRawModel.change24Hour;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.changeDouble = 0.0;
        }
        try {
            cryptoCurrencyFormattedPrice.open = currencyDetailsDisplayModel.open24Hour;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.open = "0.0";
        }
        try {
            cryptoCurrencyFormattedPrice.high = currencyDetailsDisplayModel.high24Hour;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.high = "0.0";
        }
        try {
            cryptoCurrencyFormattedPrice.low = currencyDetailsDisplayModel.low24Hour;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.low = "0.0";
        }
        try {
            cryptoCurrencyFormattedPrice.marketCap = currencyDetailsDisplayModel.mktCap;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.marketCap = "0.0";
        }
        try {
            cryptoCurrencyFormattedPrice.supply = currencyDetailsDisplayModel.supply;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.supply = "0.0";
        }
        try {
            cryptoCurrencyFormattedPrice.volume = currencyDetailsDisplayModel.volume24Hour;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.volume = "0.0";
        }
        try {
            cryptoCurrencyFormattedPrice.fromCurrencySymbol = currencyDetailsDisplayModel.fromSymbol;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.fromCurrencySymbol = "";
        }
        try {
            cryptoCurrencyFormattedPrice.toCurrencySymbol = currencyDetailsDisplayModel.toSymbol;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.toCurrencySymbol = "";
        }
        try {
            cryptoCurrencyFormattedPrice.exchangeName = currencyDetailsDisplayModel.market;
        } catch (Exception e) {
            cryptoCurrencyFormattedPrice.exchangeName = "";
        }
        cryptoCurrencyFormattedPrice.lastUpdated = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT).format(new Date());
        return cryptoCurrencyFormattedPrice;
    }


    public static CryptoCurrencyFormattedSocialStatsDetails getFormattedSocialStatsDetails(CryptoCurrencySocialStats.Data data) {
        CryptoCurrencyFormattedSocialStatsDetails cryptoCurrencyFormattedSocialStatsDetails = new CryptoCurrencyFormattedSocialStatsDetails();
        CryptoCurrencyFormattedSocialStatsDetails.Reddit reddit;
        CryptoCurrencyFormattedSocialStatsDetails.Facebook facebook;
        CryptoCurrencyFormattedSocialStatsDetails.Twitter twitter;
        if (data.reddit != null) {
            reddit = new CryptoCurrencyFormattedSocialStatsDetails.Reddit();
            reddit.name = data.reddit.name;
            reddit.link = data.reddit.link;
            reddit.activeUsers = NumberFormat.getNumberInstance().format(data.reddit.activeUsers != null ? data.reddit.activeUsers : 0);
            reddit.subscribers = NumberFormat.getNumberInstance().format(data.reddit.subscribers != null ? data.reddit.subscribers : 0);
            reddit.postsPerDay = data.reddit.postsPerDay;
        } else {
            reddit = CryptoCurrencyFormattedSocialStatsDetails.Reddit.getNewInstance();
        }
        if (data.facebook != null) {
            facebook = new CryptoCurrencyFormattedSocialStatsDetails.Facebook();
            facebook.name = data.facebook.name;
            facebook.link = data.facebook.link;
            facebook.likes = NumberFormat.getNumberInstance().format(data.facebook.likes != null ? data.facebook.likes : 0);
            facebook.talkingAbout = NumberFormat.getNumberInstance().format(data.facebook.talkingAbout != null ? data.facebook.talkingAbout : 0);
        } else {
            facebook = CryptoCurrencyFormattedSocialStatsDetails.Facebook.getNewInstance();
        }
        if (data.twitter != null) {
            twitter = new CryptoCurrencyFormattedSocialStatsDetails.Twitter();
            twitter.name = data.twitter.name;
            twitter.link = data.twitter.link;
            twitter.following = data.twitter.following;
            twitter.followers = NumberFormat.getNumberInstance().format(data.twitter.followers != null ? data.twitter.followers : 0);
        } else {
            twitter = CryptoCurrencyFormattedSocialStatsDetails.Twitter.getNewInstance();
        }
        if (data.cryptoCompare != null && data.cryptoCompare.similarItems != null) {
            cryptoCurrencyFormattedSocialStatsDetails.similarItems = new ArrayList<>();
            for (CryptoCurrencySocialStats.SimilarItem similarItem : data.cryptoCompare.similarItems) {
                CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency similarCryptoCurrency = new CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency();
                similarCryptoCurrency.id = String.valueOf(similarItem.id != null ? similarItem.id : 0);
                similarCryptoCurrency.name = similarItem.name;
                similarCryptoCurrency.imageUrl = similarItem.imageUrl;
                cryptoCurrencyFormattedSocialStatsDetails.similarItems.add(similarCryptoCurrency);
            }

        } else {
            cryptoCurrencyFormattedSocialStatsDetails.similarItems = new ArrayList<>();
        }
        cryptoCurrencyFormattedSocialStatsDetails.reddit = reddit;
        cryptoCurrencyFormattedSocialStatsDetails.facebook = facebook;
        cryptoCurrencyFormattedSocialStatsDetails.twitter = twitter;
        return cryptoCurrencyFormattedSocialStatsDetails;
    }

    public static CryptoCurrencyFormattedStatsWrapper getFormattedCryptoCurrencyStatsWrapper(CryptoCurrencyHistogramWrapperModel cryptoCurrencyHistogramWrapperModel,
                                                                                             CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice cryptoCurrencyHistogramChoice) {

        if (cryptoCurrencyHistogramWrapperModel != null && cryptoCurrencyHistogramWrapperModel.data != null && cryptoCurrencyHistogramWrapperModel.data.size() > 0) {
            CryptoCurrencyFormattedStatsWrapper cryptoCurrencyFormattedStatsWrapper = new CryptoCurrencyFormattedStatsWrapper();
            cryptoCurrencyFormattedStatsWrapper.cryptoCurrencyFormattedStatsEntryList = new ArrayList<>();
            cryptoCurrencyFormattedStatsWrapper.cryptoCurrencyFormattedStatsList = new ArrayList<>();

            for (CryptoCurrencyHistogramWrapperModel.CryptoCurrencyStats cryptoCurrencyStats : cryptoCurrencyHistogramWrapperModel.data) {
                CryptoCurrencyFormattedStatsWrapper.CryptoCurrencyFormattedStats cryptoCurrencyFormattedStats = new CryptoCurrencyFormattedStatsWrapper.CryptoCurrencyFormattedStats();
                try {
                    switch (cryptoCurrencyHistogramChoice) {
                        case _1H:
                            cryptoCurrencyFormattedStats.date = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date((long) cryptoCurrencyStats.time * 1000));
                            break;
                        case _1D:
                            cryptoCurrencyFormattedStats.date = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date((long) cryptoCurrencyStats.time * 1000));
                            break;
                        case _1M:
                            cryptoCurrencyFormattedStats.date = new SimpleDateFormat("dd MMM", Locale.getDefault()).format(new Date((long) cryptoCurrencyStats.time * 1000));
                            break;
                        case _1Y:
                            cryptoCurrencyFormattedStats.date = new SimpleDateFormat("dd MMM yy", Locale.getDefault()).format(new Date((long) cryptoCurrencyStats.time * 1000));
                            break;
                        case _T:
                            cryptoCurrencyFormattedStats.date = new SimpleDateFormat("dd MMM yy", Locale.getDefault()).format(new Date((long) cryptoCurrencyStats.time * 1000));
                            break;
                        default:
                            cryptoCurrencyFormattedStats.date = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date((long) cryptoCurrencyStats.time * 1000));
                            break;
                    }
                } catch (Exception e) {
                    cryptoCurrencyFormattedStats.date = "";
                }
                try {
                    cryptoCurrencyFormattedStats.open = NumberFormat.getNumberInstance().format(cryptoCurrencyStats.open);
                } catch (Exception e) {
                    cryptoCurrencyFormattedStats.open = "";
                }
                try {
                    cryptoCurrencyFormattedStats.close = NumberFormat.getNumberInstance().format(cryptoCurrencyStats.close);
                } catch (Exception e) {
                    cryptoCurrencyFormattedStats.close = "";
                }
                try {
                    cryptoCurrencyFormattedStats.high = NumberFormat.getNumberInstance().format(cryptoCurrencyStats.high);
                } catch (Exception e) {
                    cryptoCurrencyFormattedStats.high = "";
                }
                try {
                    cryptoCurrencyFormattedStats.low = NumberFormat.getNumberInstance().format(cryptoCurrencyStats.low);
                } catch (Exception e) {
                    cryptoCurrencyFormattedStats.low = "";
                }
                cryptoCurrencyFormattedStatsWrapper.cryptoCurrencyFormattedStatsList.add(cryptoCurrencyFormattedStats);

                try {
                    cryptoCurrencyFormattedStatsWrapper.cryptoCurrencyFormattedStatsEntryList.add(new Entry(cryptoCurrencyStats.time.longValue(), cryptoCurrencyStats.close.floatValue()));
                } catch (Exception ex) {
                    cryptoCurrencyFormattedStatsWrapper.cryptoCurrencyFormattedStatsEntryList.add(new Entry(0, 0));
                }
            }
            return cryptoCurrencyFormattedStatsWrapper;
        } else {
            return CryptoCurrencyFormattedStatsWrapper.getInstance();
        }
    }
}
