package com.alacritystudios.cryptotrace.util;

import com.alacritystudios.cryptotrace.di.scope.CryptoTraceScope;

/**
 * @author Anuj Dutt
 *         A utility for keeping track of local data to update.
 */

@CryptoTraceScope
public class CacheState {

    private boolean isCryptoCurrencyListUpdated;

    private boolean isExchangeListUpdated;

    public CacheState() {

        isCryptoCurrencyListUpdated = false;

        isExchangeListUpdated = false;
    }

    public boolean isCryptoCurrencyListUpdated() {
        return isCryptoCurrencyListUpdated;
    }

    public void setCryptoCurrencyListUpdated(boolean cryptoCurrencyListUpdated) {
        isCryptoCurrencyListUpdated = cryptoCurrencyListUpdated;
    }

    public boolean isExchangeListUpdated() {
        return isExchangeListUpdated;
    }

    public void setExchangeListUpdated(boolean exchangeListUpdated) {
        isExchangeListUpdated = exchangeListUpdated;
    }
}
