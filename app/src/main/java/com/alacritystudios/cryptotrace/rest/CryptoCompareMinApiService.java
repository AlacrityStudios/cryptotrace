package com.alacritystudios.cryptotrace.rest;

import android.arch.lifecycle.LiveData;

import com.alacritystudios.cryptotrace.resource.ApiResponse;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyHistogramWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyListWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPriceCompleteDetailsWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPriceDisplayDetailsWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPriceRawDetailsWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.News;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Anuj Dutt
 *         Service for CryptoCompare Min API calls.
 */

public interface CryptoCompareMinApiService {

    final String BASE_URL = "https://min-api.cryptocompare.com/";

    @GET("data/all/coinlist")
    LiveData<ApiResponse<CryptoCurrencyListWrapperModel>> getCryptoCurrencyList();

    @GET("data/price")
    LiveData<ApiResponse<HashMap<String, Double>>> getCryptoCurrencyPrice(@Query("fsym") String fromSymbol, @Query("tsyms") String toSymbol, @Query("e") String exchange);

    @GET("data/pricemultifull")
    LiveData<ApiResponse<CryptoCurrencyPriceCompleteDetailsWrapperModel>> getCryptoCurrencyCompletePriceDetails(@Query("fsyms") String fromSymbol, @Query("tsyms") String toSymbol, @Query("e") String exchange);

    @GET("data/pricemultifull")
    LiveData<ApiResponse<CryptoCurrencyPriceRawDetailsWrapperModel>> getCryptoCurrencyRawPriceDetails(@Query("fsyms") String fromSymbol, @Query("tsyms") String toSymbol, @Query("e") String exchange);

    @GET("data/pricemultifull")
    LiveData<ApiResponse<CryptoCurrencyPriceDisplayDetailsWrapperModel>> getCryptoCurrencyDisplayPriceDetails(@Query("fsyms") String fromSymbol, @Query("tsyms") String toSymbol);

    @GET("data/pricemulti")
    LiveData<ApiResponse<HashMap<String, HashMap<String, Double>>>> getCryptoCurrencyPrices(@Query("fsyms") String fromSymbols, @Query("tsyms") String toSymbols);

    @GET("data/news/")
    Call<List<News>> getNewsList(@Query("lTs") Long lesserThanTime);

    @GET("data/histominute")
    LiveData<ApiResponse<CryptoCurrencyHistogramWrapperModel>> getCryptoCurrencyVariationsPerMinute(@Query("fsym") String fromSymbol, @Query("tsym") String toSymbol, @Query("e") String marketName, @Query("limit") int limit);

    @GET("data/histohour")
    LiveData<ApiResponse<CryptoCurrencyHistogramWrapperModel>> getCryptoCurrencyVariationsPerHour(@Query("fsym") String fromSymbol, @Query("tsym") String toSymbol, @Query("e") String marketName, @Query("limit") int limit);

    @GET("data/histoday")
    LiveData<ApiResponse<CryptoCurrencyHistogramWrapperModel>> getCryptoCurrencyVariationsPerDay(@Query("fsym") String fromSymbol, @Query("tsym") String toSymbol, @Query("e") String marketName, @Query("limit") int limit);
}

