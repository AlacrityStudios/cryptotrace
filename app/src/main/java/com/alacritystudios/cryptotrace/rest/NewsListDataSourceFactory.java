package com.alacritystudios.cryptotrace.rest;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.alacritystudios.cryptotrace.viewobject.News;

/**
 * @author Anuj Dutt
 *         Factory to instantiate DataSource.
 */

public class NewsListDataSourceFactory implements DataSource.Factory<Long, News> {

    public MutableLiveData<NewsListDataSource> sourceLiveData = new MutableLiveData<>();
    private CryptoCompareMinApiService cryptoCompareMinApiService;

    public NewsListDataSourceFactory(CryptoCompareMinApiService cryptoCompareMinApiService) {
        this.cryptoCompareMinApiService = cryptoCompareMinApiService;
    }

    @Override
    public DataSource<Long, News> create() {
        NewsListDataSource source = new NewsListDataSource(cryptoCompareMinApiService);
        sourceLiveData.postValue(source);
        return source;
    }
}