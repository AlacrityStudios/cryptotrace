package com.alacritystudios.cryptotrace.rest;

import android.arch.lifecycle.LiveData;

import com.alacritystudios.cryptotrace.resource.ApiResponse;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencySocialStats;
import com.alacritystudios.cryptotrace.viewobject.ExchangeListWrapperModel;

import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Anuj Dutt
 * Service for CryptoCompare API calls.
 */

public interface CryptoCompareApiService {

    final String BASE_URL = "https://www.cryptocompare.com/api/";

    @GET("data/exchanges")
    LiveData<ApiResponse<ExchangeListWrapperModel>> getExchangeList();

    @GET("data/socialstats")
    LiveData<ApiResponse<CryptoCurrencySocialStats>> getCryptoCurrencySocialStats(@Query("id") String id);
}
