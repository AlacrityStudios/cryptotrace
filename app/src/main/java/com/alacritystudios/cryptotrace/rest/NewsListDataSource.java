package com.alacritystudios.cryptotrace.rest;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.ItemKeyedDataSource;
import android.support.annotation.NonNull;

import com.alacritystudios.cryptotrace.resource.Listing;
import com.alacritystudios.cryptotrace.viewobject.News;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * @author Anuj Dutt
 *         A data source for NewsListItem. It utilises a key from previous fetches to load the next set of data
 *         from the API. {@link ItemKeyedDataSource} is utilised in this case. It provides abstract methods to be implemented
 *         by us in cases of initial loading, loading after initial case. We aren't implementing load before case.
 *         This data source is provided by a factory which is responsible for its instantiation.
 */

public class NewsListDataSource extends ItemKeyedDataSource<Long, News> {

    CryptoCompareMinApiService cryptoCompareMinApiService;

    public NewsListDataSource(CryptoCompareMinApiService cryptoCompareMinApiService) {
        this.cryptoCompareMinApiService = cryptoCompareMinApiService;
    }

    public MutableLiveData<Listing.NetworkState> networkState = new MutableLiveData<Listing.NetworkState>();

    public MutableLiveData<Listing.NetworkState> initialLoad = new MutableLiveData<Listing.NetworkState>();

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull final LoadInitialCallback<News> callback) {
        networkState.postValue(Listing.NetworkState.LOADING);
        initialLoad.postValue(Listing.NetworkState.LOADING);
        Call<List<News>> listCall = cryptoCompareMinApiService.getNewsList(params.requestedInitialKey);
        listCall.enqueue(new Callback<List<News>>() {

            @Override
            public void onResponse(@NonNull Call<List<News>> call, @NonNull Response<List<News>> response) {
                if (response.isSuccessful()) {
                    List<News> newsList = response.body();
                    if (newsList != null) {
                        callback.onResult(newsList);
                        networkState.postValue(Listing.NetworkState.LOADED);
                    } else {
                        networkState.postValue(Listing.NetworkState.error("Some error occurred while fetching news list from the API."));
                    }
                } else {
                    callback.onResult(new ArrayList<News>());
                    networkState.postValue(Listing.NetworkState.error("Some error occurred while fetching news list from the API."));
                    initialLoad.postValue(Listing.NetworkState.error("Some error occurred while fetching news list from the API."));
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<News>> call, @NonNull Throwable t) {
                callback.onResult(new ArrayList<News>());
                networkState.postValue(Listing.NetworkState.error("Some error occurred while fetching news list from the API."));
                initialLoad.postValue(Listing.NetworkState.error("Some error occurred while fetching news list from the API."));
            }
        });
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params, @NonNull final LoadCallback<News> callback) {
        networkState.postValue(Listing.NetworkState.LOADING);
        Call<List<News>> listCall = cryptoCompareMinApiService.getNewsList(params.key);
        listCall.enqueue(new Callback<List<News>>() {

            @Override
            public void onResponse(@NonNull Call<List<News>> call, @NonNull Response<List<News>> response) {
                if (response.isSuccessful()) {
                    List<News> newsList = response.body();
                    if (newsList != null) {
                        callback.onResult(newsList);
                        networkState.postValue(Listing.NetworkState.LOADED);
                    } else {
                        networkState.postValue(Listing.NetworkState.error("Some error occurred while fetching news list from the API."));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<News>> call, @NonNull Throwable t) {
                networkState.postValue(Listing.NetworkState.error("Some error occurred while fetching news list from the API."));
                initialLoad.postValue(Listing.NetworkState.error("Some error occurred while fetching news list from the API."));
            }
        });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<News> callback) {
        // ignored, since we only ever append to our initial load
    }

    @NonNull
    @Override
    public Long getKey(@NonNull News item) {
        return item.publishedOn;
    }
}
