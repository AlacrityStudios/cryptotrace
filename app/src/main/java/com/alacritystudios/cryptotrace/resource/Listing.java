package com.alacritystudios.cryptotrace.resource;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;

/**
 * @author Anuj Dutt
 *         A wrapper class for paginating through API calls.
 *         This class is similar to ApiResponse as it contains Network info along with the result.
 *         The only difference being that Listing contains a LiveData of PagedList of the desired item list type.
 */

public abstract class Listing<T> {

    public LiveData<PagedList<T>> pagedListLiveData;

    public LiveData<NetworkState> networkStateLiveData;

    public LiveData<NetworkState> refreshState;

    public Listing(LiveData<PagedList<T>> pagedListLiveData, LiveData<NetworkState> networkStateLiveData, LiveData<NetworkState> refreshState) {
        this.pagedListLiveData = pagedListLiveData;
        this.networkStateLiveData = networkStateLiveData;
        this.refreshState = refreshState;
    }

    public abstract void refresh();

    public abstract void retry();

    public static class NetworkState {

        public enum Status {
            RUNNING,
            SUCCESS,
            FAILED
        }

        public Status status;
        public String error;

        public NetworkState(Status status, String error) {
            this.status = status;
            this.error = error;
        }

        public static final NetworkState LOADING = new NetworkState(Status.RUNNING, null);

        public static final NetworkState LOADED = new NetworkState(Status.SUCCESS, null);

        public static NetworkState error(String message) {
            return new NetworkState(Status.FAILED, message);
        }
    }
}