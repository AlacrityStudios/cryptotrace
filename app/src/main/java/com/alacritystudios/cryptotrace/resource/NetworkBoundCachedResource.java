package com.alacritystudios.cryptotrace.resource;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.alacritystudios.cryptotrace.util.AppExecutor;

import java.util.Objects;

import timber.log.Timber;

/**
 * @author Anuj Dutt
 *         A wrapper class to abtracat details of a resource that is dependent on both an api response and
 *         the local Room database.
 */

public abstract class NetworkBoundCachedResource<RequestType, ResultType> {


    private AppExecutor appExecutor;
    private MediatorLiveData<Resource<ResultType>> resultTypeMediatorLiveData = new MediatorLiveData<>();

    @MainThread
    public NetworkBoundCachedResource(final AppExecutor appExecutor) {
        this.appExecutor = appExecutor;
        if (shouldFetchFromNetwork()) {
            fetchFromNetwork();
        } else {
            resultTypeMediatorLiveData.addSource(loadFromDb(), new Observer<ResultType>() {

                @Override
                public void onChanged(@Nullable ResultType resultType) {
                    resultTypeMediatorLiveData.setValue(Resource.success(resultType));
                }
            });
        }
    }

    private void fetchFromNetwork() {
        appExecutor.workerThread.execute(new Runnable() {

            @Override
            public void run() {
                final LiveData<ResultType> dbSource = loadFromDb();
                resultTypeMediatorLiveData.addSource(dbSource, new Observer<ResultType>() {

                    @Override
                    public void onChanged(@Nullable ResultType resultType) {
                        postValue(Resource.loading(resultType));
                    }
                });

                final LiveData<ApiResponse<RequestType>> apiResponse = createCall();
                resultTypeMediatorLiveData.addSource(apiResponse, new Observer<ApiResponse<RequestType>>() {

                    @Override
                    public void onChanged(final ApiResponse<RequestType> requestTypeApiResponse) {
                        resultTypeMediatorLiveData.removeSource(dbSource);
                        resultTypeMediatorLiveData.removeSource(apiResponse);
                        if (requestTypeApiResponse.isSuccessful()) {
                            appExecutor.diskIO.execute(new Runnable() {

                                @Override
                                public void run() {
                                    Timber.d("Saving response from API into DB");
                                    saveCallResult(processResponse(requestTypeApiResponse));
                                    Timber.d("Saved response from API into DB");
                                    resultTypeMediatorLiveData.addSource(dbSource, new Observer<ResultType>() {

                                        @Override
                                        public void onChanged(@Nullable ResultType resultType) {
                                            Timber.d("Fetched new  response from DB");
                                            postValue(Resource.success(resultType));
                                        }
                                    });
                                }
                            });
                        } else {
                            onFetchFailed();
                            appExecutor.diskIO.execute(new Runnable() {

                                @Override
                                public void run() {
                                    resultTypeMediatorLiveData.addSource(dbSource, new Observer<ResultType>() {

                                        @Override
                                        public void onChanged(@Nullable ResultType resultType) {
                                            postValue(Resource.error(requestTypeApiResponse.errorMessage, resultType));
                                        }
                                    });
                                }
                            });

                        }
                    }
                });
            }
        });
    }

    @MainThread
    private void setValue(Resource<ResultType> newValue) {
        if (! Objects.equals(resultTypeMediatorLiveData.getValue(), newValue)) {
            resultTypeMediatorLiveData.setValue(newValue);
        }
    }

    @WorkerThread
    private void postValue(Resource<ResultType> newValue) {
        if (! Objects.equals(resultTypeMediatorLiveData.getValue(), newValue)) {
            resultTypeMediatorLiveData.postValue(newValue);
        }
    }

    protected void onFetchFailed() {
    }

    public LiveData<Resource<ResultType>> asLiveData() {
        return resultTypeMediatorLiveData;
    }

    @NonNull
    @MainThread
    public abstract LiveData<ResultType> loadFromDb();

    @NonNull
    @MainThread
    public abstract LiveData<ApiResponse<RequestType>> createCall();

    @MainThread
    public abstract boolean shouldFetchFromNetwork();

    @WorkerThread
    public abstract void saveCallResult(RequestType requestType);

    @WorkerThread
    public RequestType processResponse(ApiResponse<RequestType> apiResponse) {
        return apiResponse.body;
    }
}
