package com.alacritystudios.cryptotrace.resource;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.alacritystudios.cryptotrace.util.AppExecutor;

import java.util.Objects;

/**
 * @author Anuj Dutt
 *         A wrapper class to abtracat details of a resource that is dependent on  an api response.
 */

public abstract class NetworkBoundResource<ResultType, RequestType> {

    MediatorLiveData<Resource<ResultType>> mediatorLiveData = new MediatorLiveData<>();

    private AppExecutor appExecutor;

    public NetworkBoundResource(AppExecutor appExecutor) {
        this.appExecutor = appExecutor;
        setInitialData();
        if (shouldFetchDataFromNetwork()) {
            fetchFromNetwork();
        }
    }

    private void fetchFromNetwork() {
        final LiveData<ApiResponse<RequestType>> apiResponse = createCall();
        mediatorLiveData.addSource(apiResponse, new Observer<ApiResponse<RequestType>>() {

            @Override
            public void onChanged(@Nullable final ApiResponse<RequestType> requestTypeApiResponse) {
                processNetworkCallResponse(requestTypeApiResponse);
            }
        });
    }

    public abstract LiveData<ApiResponse<RequestType>> createCall();

    public abstract void processNetworkCallResponse(ApiResponse<RequestType> requestTypeApiResponse);

    public abstract void setInitialData();

    public abstract boolean shouldFetchDataFromNetwork();

    public LiveData<Resource<ResultType>> asLiveData() {
        return mediatorLiveData;
    }

    @WorkerThread
    protected void postValue(Resource<ResultType> newValue) {
        if (! Objects.equals(mediatorLiveData.getValue(), newValue)) {
            mediatorLiveData.postValue(newValue);
        }
    }
}