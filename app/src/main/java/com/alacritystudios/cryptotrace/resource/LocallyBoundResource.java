package com.alacritystudios.cryptotrace.resource;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.alacritystudios.cryptotrace.util.AppExecutor;

/**
 * @author Anuj Dutt
 *         A wrapper class to fetch locally backed data on the Db.
 */

public abstract class LocallyBoundResource<ResourceType> {

    AppExecutor appExecutor;

    MediatorLiveData<Resource<ResourceType>> resultMutableLiveData = new MediatorLiveData<>();

    public LocallyBoundResource(AppExecutor appExecutor) {
        this.appExecutor = appExecutor;
        resultMutableLiveData.setValue(Resource.<ResourceType>loading(null));
        resultMutableLiveData.addSource(loadFromDb(), new Observer<ResourceType>() {

            @Override
            public void onChanged(@Nullable ResourceType resourceType) {
                resultMutableLiveData.setValue(Resource.success(resourceType));
            }
        });
    }

    public abstract LiveData<ResourceType> loadFromDb();

    public LiveData<Resource<ResourceType>> asLiveData() {
        return resultMutableLiveData;
    }
}