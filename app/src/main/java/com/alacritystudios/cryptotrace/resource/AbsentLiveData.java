package com.alacritystudios.cryptotrace.resource;

import android.arch.lifecycle.LiveData;

/**
 * @author Anuj Dutt
 * A helper class to store null value LiveData objects.
 */

public class AbsentLiveData<T> extends LiveData<T> {

    public AbsentLiveData() {

        postValue(null);
    }
}