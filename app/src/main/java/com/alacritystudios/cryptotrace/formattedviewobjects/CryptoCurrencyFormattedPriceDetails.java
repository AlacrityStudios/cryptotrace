package com.alacritystudios.cryptotrace.formattedviewobjects;

import android.text.format.DateUtils;

import java.text.DateFormat;
import java.util.Date;

/**
 * @author Anuj Dutt
 *  Contains information about a specific cryptocurrency.
 */

public class CryptoCurrencyFormattedPriceDetails {

    public String fromCurrencySymbol;

    public String toCurrencySymbol;

    public String exchangeName;

    public String change;

    public String changePercent;

    public String open;

    public String high;

    public String low;

    public String marketCap;

    public String volume;

    public String supply;

    public String price;

    public Double changeDouble;

    public String lastUpdated;

    public CryptoCurrencyFormattedPriceDetails() {
    }

    public static CryptoCurrencyFormattedPriceDetails getClearedPriceInformation() {
        CryptoCurrencyFormattedPriceDetails cryptoCurrencyFormattedPriceDetails = new CryptoCurrencyFormattedPriceDetails();
        cryptoCurrencyFormattedPriceDetails.price = "0.0";
        cryptoCurrencyFormattedPriceDetails.changePercent = "0.0%";
        cryptoCurrencyFormattedPriceDetails.change = "0";
        cryptoCurrencyFormattedPriceDetails.changeDouble = 0.0;
        cryptoCurrencyFormattedPriceDetails.open = "0";
        cryptoCurrencyFormattedPriceDetails.high = "0";
        cryptoCurrencyFormattedPriceDetails.low = "0";
        cryptoCurrencyFormattedPriceDetails.marketCap = "0";
        cryptoCurrencyFormattedPriceDetails.supply = "0";
        cryptoCurrencyFormattedPriceDetails.volume = "0";
        cryptoCurrencyFormattedPriceDetails.fromCurrencySymbol = "";
        cryptoCurrencyFormattedPriceDetails.toCurrencySymbol = "";
        cryptoCurrencyFormattedPriceDetails.exchangeName = "";
        cryptoCurrencyFormattedPriceDetails.lastUpdated = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT).format(new Date());
        return cryptoCurrencyFormattedPriceDetails;
    }
}
