package com.alacritystudios.cryptotrace.formattedviewobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Anuj Dutt
 *         Contains information about a specific cryptocurrency.
 */

public class CryptoCurrencyFormattedSocialStatsDetails {

    public List<SimilarCryptoCurrency> similarItems;

    public Facebook facebook;

    public Twitter twitter;

    public Reddit reddit;

    public static CryptoCurrencyFormattedSocialStatsDetails getNewInstance() {
        CryptoCurrencyFormattedSocialStatsDetails cryptoCurrencyFormattedSocialStatsDetails = new CryptoCurrencyFormattedSocialStatsDetails();
        cryptoCurrencyFormattedSocialStatsDetails.similarItems = new ArrayList<>();
        cryptoCurrencyFormattedSocialStatsDetails.facebook = Facebook.getNewInstance();
        cryptoCurrencyFormattedSocialStatsDetails.twitter = Twitter.getNewInstance();
        cryptoCurrencyFormattedSocialStatsDetails.reddit = Reddit.getNewInstance();
        cryptoCurrencyFormattedSocialStatsDetails.similarItems.add(SimilarCryptoCurrency.getNewInstance());
        cryptoCurrencyFormattedSocialStatsDetails.similarItems.add(SimilarCryptoCurrency.getNewInstance());
        cryptoCurrencyFormattedSocialStatsDetails.similarItems.add(SimilarCryptoCurrency.getNewInstance());
        cryptoCurrencyFormattedSocialStatsDetails.similarItems.add(SimilarCryptoCurrency.getNewInstance());
        cryptoCurrencyFormattedSocialStatsDetails.similarItems.add(SimilarCryptoCurrency.getNewInstance());
        return cryptoCurrencyFormattedSocialStatsDetails;
    }

    public static class SimilarCryptoCurrency {

        public String id;

        public String name;

        public String imageUrl;

        public static SimilarCryptoCurrency getNewInstance() {
            SimilarCryptoCurrency similarCryptoCurrency = new SimilarCryptoCurrency();
            similarCryptoCurrency.name = " ";
            similarCryptoCurrency.id = "0";
            similarCryptoCurrency.imageUrl = "-";
            return similarCryptoCurrency;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj == this) {
                return true;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            SimilarCryptoCurrency rhs = (SimilarCryptoCurrency) obj;
            return Objects.equals(name, rhs.name)
                    && Objects.equals(id, rhs.id)
                    && Objects.equals(imageUrl, rhs.imageUrl);
        }
    }

    public static class Facebook {

        public String name;

        public String link;

        public String likes;

        public String talkingAbout;

        public static Facebook getNewInstance() {
            Facebook facebook = new Facebook();
            facebook.name = "-";
            facebook.link = "-";
            facebook.likes = "0";
            facebook.talkingAbout = "0";
            return facebook;
        }
    }

    public static class Twitter {

        public String name;

        public String link;

        public String followers;

        public String following;

        public static Twitter getNewInstance() {
            Twitter twitter = new Twitter();
            twitter.name = "-";
            twitter.link = "-";
            twitter.followers = "0";
            twitter.following = "0";
            return twitter;
        }
    }

    public static class Reddit {

        public String name;

        public String link;

        public String subscribers;

        public String activeUsers;

        public String postsPerDay;

        public static Reddit getNewInstance() {
            Reddit reddit = new Reddit();
            reddit.name = "-";
            reddit.link = "-";
            reddit.subscribers = "0";
            reddit.activeUsers = "0";
            reddit.postsPerDay = "0";
            return reddit;
        }
    }

}
