package com.alacritystudios.cryptotrace.formattedviewobjects;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Anuj Dutt
 *         A model to store all stats in a simplified form to be displayed.
 */

public class CryptoCurrencyFormattedStatsWrapper {

    public List<CryptoCurrencyFormattedStats> cryptoCurrencyFormattedStatsList;
    public List<Entry> cryptoCurrencyFormattedStatsEntryList;

    public static CryptoCurrencyFormattedStatsWrapper getInstance() {
        CryptoCurrencyFormattedStatsWrapper cryptoCurrencyFormattedStatsWrapper = new CryptoCurrencyFormattedStatsWrapper();
        cryptoCurrencyFormattedStatsWrapper.cryptoCurrencyFormattedStatsList = new ArrayList<>();
        cryptoCurrencyFormattedStatsWrapper.cryptoCurrencyFormattedStatsEntryList = new ArrayList<>();
        return cryptoCurrencyFormattedStatsWrapper;
    }

    public static class CryptoCurrencyFormattedStats {

        public String date;
        public String open;
        public String close;
        public String high;
        public String low;

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj == this) {
                return true;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            CryptoCurrencyFormattedStats rhs = (CryptoCurrencyFormattedStats) obj;
            return Objects.equals(date, rhs.date)
                    && Objects.equals(open, rhs.open)
                    && Objects.equals(close, rhs.close)
                    && Objects.equals(high, rhs.high)
                    && Objects.equals(low, rhs.low);
        }
    }
}
