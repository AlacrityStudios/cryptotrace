package com.alacritystudios.cryptotrace.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;

import com.alacritystudios.cryptotrace.R;

/**
 * @author Anuj Dutt
 *         An indicator to display
 */

public class StateIndicator extends CardView {

    private Drawable mInvalidStateIcon;
    private Drawable mValidStateIcon;
    private int tintColor;
    private View.OnFocusChangeListener mOnFocusChangeListener;
    private View.OnTouchListener mOnTouchListener;

    public StateIndicator(Context context) {
        super(context);
        init(context);
    }

    public StateIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.StateIndicator, 0, 0);
        try {
            tintColor = ta.getColor(R.styleable.StateIndicator_validTintColor, ActivityCompat.getColor(context, R.color.colorAccent));
        } catch (Exception e) {
            throw new RuntimeException("One or more of the styled attributes weren't set.");
        } finally {
            ta.recycle();
        }
        init(context);
    }



    public StateIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.StateIndicator, 0, 0);
        try {
            tintColor = ta.getColor(R.styleable.StateIndicator_validTintColor, ActivityCompat.getColor(context, R.color.colorAccent));
        } catch (Exception e) {
            throw new RuntimeException("One or more of the styled attributes weren't set.");
        } finally {
            ta.recycle();
        }
        init(context);
    }

    public void init(Context context) {
        setBackgroundResource(R.drawable.ic_oval_white);
        Drawable invalidDrawable = ContextCompat.getDrawable(context, R.drawable.ic_circle_24dp);
        Drawable invalidWrappedDrawable = DrawableCompat.wrap(invalidDrawable);
        DrawableCompat.setTint(invalidWrappedDrawable, tintColor);
        mValidStateIcon = invalidWrappedDrawable;
        Drawable validDrawable = ContextCompat.getDrawable(context, R.drawable.ic_check_24dp);
        Drawable validWrappedDrawable = DrawableCompat.wrap(validDrawable);
        DrawableCompat.setTint(validWrappedDrawable, tintColor);
        mInvalidStateIcon = invalidWrappedDrawable;
        mValidStateIcon = validWrappedDrawable;
        mValidStateIcon.setBounds(0, 0, mValidStateIcon.getIntrinsicHeight(), mValidStateIcon.getIntrinsicHeight());
        mInvalidStateIcon.setBounds(0, 0, mValidStateIcon.getIntrinsicHeight(), mValidStateIcon.getIntrinsicHeight());
    }
}
