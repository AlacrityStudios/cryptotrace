package com.alacritystudios.cryptotrace.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.alacritystudios.cryptotrace.dao.AlertDao;
import com.alacritystudios.cryptotrace.dao.AlertDetailsDao;
import com.alacritystudios.cryptotrace.dao.CryptoCurrencyDao;
import com.alacritystudios.cryptotrace.dao.ExchangeDao;
import com.alacritystudios.cryptotrace.dao.FavoriteCryptoCurrencyDao;
import com.alacritystudios.cryptotrace.dao.FiatCurrencyDao;
import com.alacritystudios.cryptotrace.viewobject.Alert;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.FavoriteCryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

/**
 * @author Anuj Dutt
 *         Database of the Cryptotrace application
 */

@Database(entities = {FiatCurrency.class, CryptoCurrency.class, Exchange.class, FavoriteCryptoCurrency.class, Alert.class}, version = 1)
public abstract class CryptoTraceDatabase extends RoomDatabase {

    public abstract CryptoCurrencyDao cryptoCurrencyDao();

    public abstract ExchangeDao exchangeDao();

    public abstract FavoriteCryptoCurrencyDao favoriteCryptoCurrencyDao();

    public abstract FiatCurrencyDao fiatCurrencyDao();

    public abstract AlertDao alertDao();

    public abstract AlertDetailsDao alertDetailsDao();
}