package com.alacritystudios.cryptotrace.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewSimpleExchangeListItemBinding;
import com.alacritystudios.cryptotrace.viewobject.Exchange;

/**
 * @author Anuj Dutt
 *         An adapter to display a simple list of the exchanges
 */

public class ExchangeListSelectionAdapter extends DataBoundRecyclerViewAdapter<Exchange, RecyclerViewSimpleExchangeListItemBinding> {

    ExchangeListSelectionAdapter.ExchangeClickCallback exchangeClickCallback;

    public ExchangeListSelectionAdapter(android.databinding.DataBindingComponent dataBindingComponent, ExchangeListSelectionAdapter.ExchangeClickCallback exchangeClickCallback) {
        super(dataBindingComponent);
        this.exchangeClickCallback = exchangeClickCallback;
    }

    @Override
    RecyclerView.ViewHolder createBinding(ViewGroup parent, int viewType) {
        final RecyclerViewSimpleExchangeListItemBinding recyclerViewSimpleExchangeListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_simple_exchange_list_item, parent, false, dataBindingComponent);
        recyclerViewSimpleExchangeListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                exchangeClickCallback.onItemClick(recyclerViewSimpleExchangeListItemBinding.getExchangeDetails());
            }
        });
        return new ObjectViewHolder(recyclerViewSimpleExchangeListItemBinding);
    }

    @Override
    void bindDataToViewHolder(RecyclerView.ViewHolder holder, Exchange data) {
        ((ObjectViewHolder) holder).viewDataBinding.setExchangeDetails(data);
    }

    @Override
    boolean checkItemsSimilarity(Exchange oldItem, Exchange newItem) {
        return oldItem.id.equals(newItem.id);
    }

    @Override
    boolean checkItemsContentsSimilarity(Exchange oldItem, Exchange newItem) {
        return oldItem.equals(newItem);
    }

    public interface ExchangeClickCallback {

        void onItemClick(Exchange cryptoCurrency);
    }
}