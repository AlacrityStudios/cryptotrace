package com.alacritystudios.cryptotrace.adapter;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewCryptoCurrencyPriceListItemBinding;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPrice;

import java.text.NumberFormat;
import java.util.List;

/**
 * @author Anuj Dutt
 *         An adapter to display the list of the cryptocurrency prices.
 */

public class CryptoCurrencyPriceListAdapter extends RecyclerView.Adapter<CryptoCurrencyPriceListAdapter.CryptoCurrencyPriceListViewHolder> {

    CryptoCurrencyPriceListAdapter.CryptoCurrencyClickCallback cryptoCurrencyClickCallback;
    private List<CryptoCurrencyPrice> cryptoCurrencyList;
    private FragmentBindingComponent fragmentBindingComponent;
    private int dataVersion = 0;

    public CryptoCurrencyPriceListAdapter(FragmentBindingComponent fragmentBindingComponent, CryptoCurrencyPriceListAdapter.CryptoCurrencyClickCallback cryptoCurrencyClickCallback) {
        this.fragmentBindingComponent = fragmentBindingComponent;
        this.cryptoCurrencyClickCallback = cryptoCurrencyClickCallback;
    }

    public class CryptoCurrencyPriceListViewHolder extends RecyclerView.ViewHolder {

        RecyclerViewCryptoCurrencyPriceListItemBinding viewDataBinding;

        public CryptoCurrencyPriceListViewHolder(RecyclerViewCryptoCurrencyPriceListItemBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            this.viewDataBinding = viewDataBinding;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public void setCryptoCurrencyList(final List<CryptoCurrencyPrice> update) {
        dataVersion++;
        final int startVersion = dataVersion;
        final List<CryptoCurrencyPrice> oldItems = cryptoCurrencyList;
        new AsyncTask<Void, Void, DiffUtil.DiffResult>() {

            @Override
            protected DiffUtil.DiffResult doInBackground(Void... voids) {
                return DiffUtil.calculateDiff(new DiffUtil.Callback() {

                    @Override
                    public int getOldListSize() {
                        return oldItems != null ? oldItems.size() : 0;
                    }

                    @Override
                    public int getNewListSize() {
                        return update != null ? update.size() : 0;
                    }

                    @Override
                    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                        return oldItems.get(oldItemPosition).fromCurrencyId.equals(update.get(newItemPosition).fromCurrencyId);
                    }

                    @Override
                    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                        return oldItems.get(oldItemPosition).equals(update.get(newItemPosition));
                    }
                });
            }

            @Override
            protected void onPostExecute(DiffUtil.DiffResult diffResult) {
                if (startVersion != dataVersion) {
                    return;
                }
                cryptoCurrencyList = update;
                diffResult.dispatchUpdatesTo(CryptoCurrencyPriceListAdapter.this);
            }
        }.execute();
    }

    @Override
    public CryptoCurrencyPriceListAdapter.CryptoCurrencyPriceListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CryptoCurrencyPriceListAdapter.CryptoCurrencyPriceListViewHolder(createBinding(parent));
    }

    @Override
    public void onBindViewHolder(CryptoCurrencyPriceListAdapter.CryptoCurrencyPriceListViewHolder holder, final int position) {
        holder.viewDataBinding.setCryptoCurrencyDetails(cryptoCurrencyList.get(position));
    }

    @Override
    public int getItemCount() {
        return cryptoCurrencyList != null ? cryptoCurrencyList.size() : 0;
    }

    private RecyclerViewCryptoCurrencyPriceListItemBinding createBinding(ViewGroup parent) {
        final RecyclerViewCryptoCurrencyPriceListItemBinding recyclerViewCryptoCurrencyPriceListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_crypto_currency_price_list_item, parent, false, fragmentBindingComponent);
        recyclerViewCryptoCurrencyPriceListItemBinding.setCryptoCompareImageBaseUrl("https://www.cryptocompare.com");
        recyclerViewCryptoCurrencyPriceListItemBinding.setNumberFormat(NumberFormat.getInstance());
        recyclerViewCryptoCurrencyPriceListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                cryptoCurrencyClickCallback.onItemClick(recyclerViewCryptoCurrencyPriceListItemBinding.getCryptoCurrencyDetails().fromCurrencyId);
            }
        });
        return recyclerViewCryptoCurrencyPriceListItemBinding;
    }

    public interface CryptoCurrencyClickCallback {

        void onItemClick(String id);
    }
}