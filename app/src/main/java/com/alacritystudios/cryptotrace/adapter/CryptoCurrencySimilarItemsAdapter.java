package com.alacritystudios.cryptotrace.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewSimilarCryptoCurrenciesItemBinding;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedSocialStatsDetails;

/**
 * @author Anuj Dutt
 *         An adapter to display the list of the cryptocurrency coins similar to the selected one.
 */

public class CryptoCurrencySimilarItemsAdapter extends DataBoundRecyclerViewAdapter<CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency, RecyclerViewSimilarCryptoCurrenciesItemBinding> {

    CryptoCurrencySimilarItemsAdapter.CryptoCurrencyClickCallback cryptoCurrencyClickCallback;

    public CryptoCurrencySimilarItemsAdapter(android.databinding.DataBindingComponent dataBindingComponent, CryptoCurrencySimilarItemsAdapter.CryptoCurrencyClickCallback cryptoCurrencyClickCallback) {
        super(dataBindingComponent);
        this.cryptoCurrencyClickCallback = cryptoCurrencyClickCallback;
    }

    @Override
    RecyclerView.ViewHolder createBinding(ViewGroup parent, int viewType) {
        final RecyclerViewSimilarCryptoCurrenciesItemBinding recyclerViewSimilarCryptoCurrenciesItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_similar_crypto_currencies_item, parent, false, dataBindingComponent);
        recyclerViewSimilarCryptoCurrenciesItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                cryptoCurrencyClickCallback.onItemClick(recyclerViewSimilarCryptoCurrenciesItemBinding.getCryptoCurrency().id);
            }
        });
        return new ObjectViewHolder(recyclerViewSimilarCryptoCurrenciesItemBinding);
    }

    @Override
    void bindDataToViewHolder(RecyclerView.ViewHolder holder, CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency data) {
        ((ObjectViewHolder) holder).viewDataBinding.setCryptoCurrency(data);
    }

    @Override
    boolean checkItemsSimilarity(CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency oldItem, CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency newItem) {
        return oldItem.id.equals(newItem.id);
    }

    @Override
    boolean checkItemsContentsSimilarity(CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency oldItem, CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency newItem) {
        return oldItem.equals(newItem);
    }

    public interface CryptoCurrencyClickCallback {

        void onItemClick(String id);
    }
}