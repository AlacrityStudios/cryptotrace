package com.alacritystudios.cryptotrace.adapter;

import android.arch.paging.PagedListAdapter;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.DiffCallback;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewNewsListItemBinding;
import com.alacritystudios.cryptotrace.resource.Listing;
import com.alacritystudios.cryptotrace.viewobject.News;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.List;

/**
 * @author Anuj Dutt
 *         A PagedListAdapter implementation to display NewsList.
 */

public class NewsListAdapter extends PagedListAdapter<News, RecyclerView.ViewHolder> {

    private FragmentBindingComponent fragmentBindingComponent;

    private NewsClickCallback newsClickCallback;

    public NewsListAdapter(@NonNull DiffCallback<News> diffCallback, FragmentBindingComponent fragmentBindingComponent, NewsClickCallback newsClickCallback) {
        super(diffCallback);
        this.fragmentBindingComponent = fragmentBindingComponent;
        this.newsClickCallback = newsClickCallback;
    }

    public class NewsListViewHolder extends RecyclerView.ViewHolder {

        RecyclerViewNewsListItemBinding binding;

        public NewsListViewHolder(RecyclerViewNewsListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsListViewHolder((RecyclerViewNewsListItemBinding)
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_news_list_item, parent, false, fragmentBindingComponent));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((NewsListViewHolder) holder).binding.setNews(getItem(position));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
    }

    public static class NewsListDiffCallback extends DiffCallback<News> {

        @Override
        public Object getChangePayload(@NonNull News oldItem, @NonNull News newItem) {
            return super.getChangePayload(oldItem, newItem);
        }

        @Override
        public boolean areItemsTheSame(@NonNull News oldItem, @NonNull News newItem) {
            return oldItem.id.equals(newItem.id);
        }

        @Override
        public boolean areContentsTheSame(@NonNull News oldItem, @NonNull News newItem) {
            return oldItem.equals(newItem);
        }
    }

    public interface NewsClickCallback {
        public void onItemClick(News news);

    }
}
