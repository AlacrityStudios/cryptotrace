package com.alacritystudios.cryptotrace.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewSimpleCryptoCurrencyListItemBinding;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewSimpleFiatCurrencyListItemBinding;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

/**
 * @author Anuj Dutt
 *         An adapter to display a simple list of the fiatcurrency coins.
 */

public class FiatCurrencyListSelectionAdapter extends DataBoundRecyclerViewAdapter<FiatCurrency, RecyclerViewSimpleFiatCurrencyListItemBinding> {

    FiatCurrencyListSelectionAdapter.FiatCurrencyClickCallback fiatCurrencyClickCallback;

    public FiatCurrencyListSelectionAdapter(android.databinding.DataBindingComponent dataBindingComponent, FiatCurrencyListSelectionAdapter.FiatCurrencyClickCallback fiatCurrencyClickCallback) {
        super(dataBindingComponent);
        this.fiatCurrencyClickCallback = fiatCurrencyClickCallback;
    }

    @Override
    RecyclerView.ViewHolder createBinding(ViewGroup parent, int viewType) {
        final RecyclerViewSimpleFiatCurrencyListItemBinding recyclerViewSimpleFiatCurrencyListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_simple_fiat_currency_list_item, parent, false, dataBindingComponent);
        recyclerViewSimpleFiatCurrencyListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                fiatCurrencyClickCallback.onItemClick(recyclerViewSimpleFiatCurrencyListItemBinding.getFiatCurrencyDetails());
            }
        });
        return new ObjectViewHolder(recyclerViewSimpleFiatCurrencyListItemBinding);
    }

    @Override
    void bindDataToViewHolder(RecyclerView.ViewHolder holder, FiatCurrency data) {
        ((ObjectViewHolder) holder).viewDataBinding.setFiatCurrencyDetails(data);
    }

    @Override
    boolean checkItemsSimilarity(FiatCurrency oldItem, FiatCurrency newItem) {
        return oldItem.id.equals(newItem.id);
    }

    @Override
    boolean checkItemsContentsSimilarity(FiatCurrency oldItem, FiatCurrency newItem) {
        return oldItem.equals(newItem);
    }

    public interface FiatCurrencyClickCallback {

        void onItemClick(FiatCurrency fiatCurrency);
    }
}
