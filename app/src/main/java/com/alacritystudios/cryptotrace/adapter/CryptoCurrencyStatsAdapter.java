package com.alacritystudios.cryptotrace.adapter;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewCryptoCurrencyStatsItemBinding;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedStatsWrapper;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyFormattedStats;

import java.util.List;

/**
 * @author Anuj Dutt
 *         Adapter to display stats of a crypto-currency in the past month.
 */

public class CryptoCurrencyStatsAdapter extends RecyclerView.Adapter<CryptoCurrencyStatsAdapter.CryptoCurrencyStatsAdapterViewHolder> {

    private List<CryptoCurrencyFormattedStatsWrapper.CryptoCurrencyFormattedStats> cryptoCurrencyFormattedStats;
    int dataVersion = 0;

    public CryptoCurrencyStatsAdapter() {
    }

    protected class CryptoCurrencyStatsAdapterViewHolder extends RecyclerView.ViewHolder {

        RecyclerViewCryptoCurrencyStatsItemBinding binding;

        public CryptoCurrencyStatsAdapterViewHolder(RecyclerViewCryptoCurrencyStatsItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public CryptoCurrencyStatsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CryptoCurrencyStatsAdapterViewHolder((RecyclerViewCryptoCurrencyStatsItemBinding)
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_crypto_currency_stats_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CryptoCurrencyStatsAdapterViewHolder holder, int position) {
        holder.binding.setCryptoCurrencyFormattedStats(cryptoCurrencyFormattedStats.get(position));
    }

    @Override
    public int getItemCount() {
        return cryptoCurrencyFormattedStats != null ? cryptoCurrencyFormattedStats.size() : 0;
    }

    @SuppressLint("StaticFieldLeak")
    public void setCryptoCurrencyFormattedStats(final List<CryptoCurrencyFormattedStatsWrapper.CryptoCurrencyFormattedStats> update) {
        dataVersion++;
        final int startVersion = dataVersion;
        final List<CryptoCurrencyFormattedStatsWrapper.CryptoCurrencyFormattedStats> oldItems = cryptoCurrencyFormattedStats;
        new AsyncTask<Void, Void, DiffUtil.DiffResult>() {

            @Override
            protected void onPostExecute(DiffUtil.DiffResult diffResult) {
                if (startVersion != dataVersion) {
                    return;
                }
                cryptoCurrencyFormattedStats = update;
                diffResult.dispatchUpdatesTo(CryptoCurrencyStatsAdapter.this);
            }

            @Override
            protected DiffUtil.DiffResult doInBackground(Void... voids) {
                return DiffUtil.calculateDiff(new DiffUtil.Callback() {

                    @Override
                    public int getOldListSize() {
                        return oldItems != null ? oldItems.size() : 0;
                    }

                    @Override
                    public int getNewListSize() {
                        return update != null ? update.size() : 0;
                    }

                    @Override
                    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                        return oldItems.get(oldItemPosition).date.equals(update.get(newItemPosition).date);
                    }

                    @Override
                    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                        return oldItems.get(oldItemPosition).equals(update.get(newItemPosition));
                    }
                });
            }
        }.execute();
    }
}
