package com.alacritystudios.cryptotrace.adapter;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewSimilarCryptoCurrenciesItemBinding;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedSocialStatsDetails;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencySocialStats;

import java.util.List;

/**
 * @author Anuj Dutt
 *         An adapter to display the list of the cryptocurrency coins similar to the selected one.
 */

public class SimilarCryptoCurrencyAdapter extends RecyclerView.Adapter<SimilarCryptoCurrencyAdapter.CryptoCurrencyListViewHolder> {


    SimilarCryptoCurrencyAdapter.CryptoCurrencyClickCallback cryptoCurrencyClickCallback;
    private List<CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency> cryptoCurrencyList;
    private FragmentBindingComponent fragmentBindingComponent;
    private int dataVersion = 0;

    public SimilarCryptoCurrencyAdapter(CryptoCurrencyClickCallback cryptoCurrencyClickCallback, FragmentBindingComponent fragmentBindingComponent) {
        this.cryptoCurrencyClickCallback = cryptoCurrencyClickCallback;
        this.fragmentBindingComponent = fragmentBindingComponent;
    }

    public class CryptoCurrencyListViewHolder extends RecyclerView.ViewHolder {

        RecyclerViewSimilarCryptoCurrenciesItemBinding viewDataBinding;

        public CryptoCurrencyListViewHolder(RecyclerViewSimilarCryptoCurrenciesItemBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            this.viewDataBinding = viewDataBinding;
        }
    }


    @Override
    public CryptoCurrencyListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CryptoCurrencyListViewHolder(createBinding(parent));
    }

    @Override
    public void onBindViewHolder(CryptoCurrencyListViewHolder holder, final int position) {
        holder.viewDataBinding.setCryptoCurrency(cryptoCurrencyList.get(position));
    }

    private RecyclerViewSimilarCryptoCurrenciesItemBinding createBinding(ViewGroup parent) {
        final RecyclerViewSimilarCryptoCurrenciesItemBinding recyclerViewSimilarCryptoCurrenciesItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_similar_crypto_currencies_item, parent, false, fragmentBindingComponent);
        recyclerViewSimilarCryptoCurrenciesItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                cryptoCurrencyClickCallback.onItemClick(recyclerViewSimilarCryptoCurrenciesItemBinding.getCryptoCurrency().id);
            }
        });
        return recyclerViewSimilarCryptoCurrenciesItemBinding;
    }

//    @SuppressLint("StaticFieldLeak")
//    public void setCryptoCurrencyList(final List<CryptoCurrencySocialStats.SimilarItem> update) {
//        dataVersion++;
//        final int startVersion = dataVersion;
//        final List<CryptoCurrencySocialStats.SimilarItem> oldItems = cryptoCurrencyList;
//        new AsyncTask<Void, Void, DiffUtil.DiffResult>() {
//
//            @Override
//            protected DiffUtil.DiffResult doInBackground(Void... voids) {
//                return DiffUtil.calculateDiff(new DiffUtil.Callback() {
//
//                    @Override
//                    public int getOldListSize() {
//                        return oldItems != null ? oldItems.size() : 0;
//                    }
//
//                    @Override
//                    public int getNewListSize() {
//                        return update != null ? update.size() : 0;
//                    }
//
//                    @Override
//                    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
//                        return oldItems.get(oldItemPosition).id.equals(update.get(newItemPosition).id);
//                    }
//
//                    @Override
//                    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
//                        return oldItems.get(oldItemPosition).equals(update.get(newItemPosition));
//                    }
//                });
//            }
//
//            @Override
//            protected void onPostExecute(DiffUtil.DiffResult diffResult) {
//                if (startVersion != dataVersion) {
//                    return;
//                }
//                cryptoCurrencyList = update;
//                diffResult.dispatchUpdatesTo(SimilarCryptoCurrencyAdapter.this);
//            }
//        }.execute();
//    }

    @Override
    public int getItemCount() {
        return cryptoCurrencyList != null ? cryptoCurrencyList.size() : 0;
    }

    public interface CryptoCurrencyClickCallback {

        void onItemClick(String id);
    }
}
