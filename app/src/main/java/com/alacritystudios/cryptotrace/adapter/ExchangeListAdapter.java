package com.alacritystudios.cryptotrace.adapter;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewExchangeListItemBinding;
import com.alacritystudios.cryptotrace.viewobject.Exchange;

import java.util.List;

/**
 * @author Anuj Dutt
 *         Adapter to display the list of exchanges.
 */

public class ExchangeListAdapter extends RecyclerView.Adapter<ExchangeListAdapter.ExchangeListViewHolder> {

    private List<Exchange> exchanges;
    private FragmentBindingComponent fragmentBindingComponent;
    private int dataVersion = 0;
    private ExchangeListAdapterListener exchangeListAdapterListener;

    public ExchangeListAdapter(FragmentBindingComponent fragmentBindingComponent, ExchangeListAdapterListener exchangeListAdapterListener) {
        this.fragmentBindingComponent = fragmentBindingComponent;
        this.exchangeListAdapterListener = exchangeListAdapterListener;
    }

    public class ExchangeListViewHolder extends RecyclerView.ViewHolder {

        RecyclerViewExchangeListItemBinding recyclerViewExchangeListItemBinding;

        public ExchangeListViewHolder(RecyclerViewExchangeListItemBinding recyclerViewExchangeListItemBinding) {
            super(recyclerViewExchangeListItemBinding.getRoot());
            this.recyclerViewExchangeListItemBinding = recyclerViewExchangeListItemBinding;
        }
    }

    @Override
    public ExchangeListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final RecyclerViewExchangeListItemBinding recyclerViewExchangeListItemBinding = (RecyclerViewExchangeListItemBinding)
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_exchange_list_item, parent, false, fragmentBindingComponent);
        recyclerViewExchangeListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                exchangeListAdapterListener.onItemClick(recyclerViewExchangeListItemBinding.getExchangeDetails());
            }
        });
        return new ExchangeListViewHolder(recyclerViewExchangeListItemBinding);
    }

    @Override
    public void onBindViewHolder(final ExchangeListViewHolder holder, int position) {
        holder.recyclerViewExchangeListItemBinding.setExchangeDetails(exchanges.get(position));
        holder.recyclerViewExchangeListItemBinding.setCryptoCompareImageBaseUrl("https://www.cryptocompare.com");
        holder.recyclerViewExchangeListItemBinding.ivExchangeLogo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                exchanges.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return exchanges != null ? exchanges.size() : 0;
    }

    @SuppressLint("StaticFieldLeak")
    public void replace(final List<Exchange> update) {
        dataVersion++;
        final int startVersion = dataVersion;
        final List<Exchange> oldItems = exchanges;
        new AsyncTask<Void, Void, DiffUtil.DiffResult>() {

            @Override
            protected void onPostExecute(DiffUtil.DiffResult diffResult) {
                if (startVersion != dataVersion) {
                    return;
                }
                exchanges = update;
                diffResult.dispatchUpdatesTo(ExchangeListAdapter.this);
            }

            @Override
            protected DiffUtil.DiffResult doInBackground(Void... voids) {
                return DiffUtil.calculateDiff(new DiffUtil.Callback() {

                    @Override
                    public int getOldListSize() {
                        return oldItems != null ? oldItems.size() : 0;
                    }

                    @Override
                    public int getNewListSize() {
                        return update != null ? update.size() : 0;
                    }

                    @Override
                    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                        return oldItems.get(oldItemPosition).id.equals(update.get(newItemPosition).id);
                    }

                    @Override
                    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                        return oldItems.get(oldItemPosition).equals(update.get(newItemPosition));
                    }
                });
            }
        }.execute();
    }

    public interface ExchangeListAdapterListener {

        void onItemClick(Exchange exchange);
    }
}
