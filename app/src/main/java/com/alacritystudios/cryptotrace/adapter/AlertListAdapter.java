package com.alacritystudios.cryptotrace.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewAlertListItemBinding;
import com.alacritystudios.cryptotrace.viewobject.AlertDisplayDetails;

/**
 * @author Anuj Dutt
 *         An adapter to display the list of the alerts.
 */

public class AlertListAdapter extends DataBoundRecyclerViewAdapter<AlertDisplayDetails, RecyclerViewAlertListItemBinding> {

    public AlertListAdapter(android.databinding.DataBindingComponent dataBindingComponent) {
        super(dataBindingComponent);
    }

    @Override
    RecyclerView.ViewHolder createBinding(ViewGroup parent, int viewType) {
        final RecyclerViewAlertListItemBinding recyclerViewAlertListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_alert_list_item, parent, false, dataBindingComponent);
        return new ObjectViewHolder(recyclerViewAlertListItemBinding);
    }

    @Override
    void bindDataToViewHolder(RecyclerView.ViewHolder holder, AlertDisplayDetails data) {
        ((ObjectViewHolder) holder).viewDataBinding.setAlertDetails(data);
    }

    @Override
    boolean checkItemsSimilarity(AlertDisplayDetails oldItem, AlertDisplayDetails newItem) {
        return oldItem.id.equals(newItem.id);
    }

    @Override
    boolean checkItemsContentsSimilarity(AlertDisplayDetails oldItem, AlertDisplayDetails newItem) {
        return oldItem.equals(newItem);
    }
}
