package com.alacritystudios.cryptotrace.adapter;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewCryptoCurrencyListItemBinding;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

import java.util.List;

/**
 * @author Anuj Dutt
 *         An adapter to display the list of the cryptocurrency coins.
 */

public class CryptoCurrencyListAdapter extends RecyclerView.Adapter<CryptoCurrencyListAdapter.CryptoCurrencyListViewHolder> {

    CryptoCurrencyClickCallback cryptoCurrencyClickCallback;
    private List<CryptoCurrency> cryptoCurrencyList;
    private FragmentBindingComponent fragmentBindingComponent;
    private int dataVersion = 0;

    public CryptoCurrencyListAdapter(FragmentBindingComponent fragmentBindingComponent, CryptoCurrencyClickCallback cryptoCurrencyClickCallback) {
        this.fragmentBindingComponent = fragmentBindingComponent;
        this.cryptoCurrencyClickCallback = cryptoCurrencyClickCallback;
        setHasStableIds(true);
    }

    public class CryptoCurrencyListViewHolder extends RecyclerView.ViewHolder {

        RecyclerViewCryptoCurrencyListItemBinding viewDataBinding;

        public CryptoCurrencyListViewHolder(RecyclerViewCryptoCurrencyListItemBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            this.viewDataBinding = viewDataBinding;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public void setCryptoCurrencyList(final List<CryptoCurrency> update) {
        dataVersion++;
        if (cryptoCurrencyList == null) {
            if (update == null) {
                return;
            }
            cryptoCurrencyList = update;
            notifyDataSetChanged();
        } else if (update == null) {
            int oldSize = cryptoCurrencyList.size();
            cryptoCurrencyList = null;
            notifyItemRangeRemoved(0, oldSize);
        } else {
            final int startVersion = dataVersion;
            final List<CryptoCurrency> oldItems = cryptoCurrencyList;
            new AsyncTask<Void, Void, DiffUtil.DiffResult>() {

                @Override
                protected DiffUtil.DiffResult doInBackground(Void... voids) {
                    return DiffUtil.calculateDiff(new DiffUtil.Callback() {

                        @Override
                        public int getOldListSize() {
                            return oldItems.size();
                        }

                        @Override
                        public int getNewListSize() {
                            return update.size();
                        }

                        @Override
                        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                            return oldItems.get(oldItemPosition).id.equals(update.get(newItemPosition).id);
                        }

                        @Override
                        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                            return oldItems.get(oldItemPosition).equals(update.get(newItemPosition));
                        }
                    });
                }

                @Override
                protected void onPostExecute(DiffUtil.DiffResult diffResult) {
                    if (startVersion != dataVersion) {
                        return;
                    }
                    cryptoCurrencyList = update;
                    diffResult.dispatchUpdatesTo(CryptoCurrencyListAdapter.this);
                }
            }.execute();
        }
    }

    @Override
    public CryptoCurrencyListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CryptoCurrencyListViewHolder(createBinding(parent));
    }

    @Override
    public void onBindViewHolder(CryptoCurrencyListViewHolder holder, int position) {
        holder.viewDataBinding.setCryptoCurrencyDetails(cryptoCurrencyList.get(position));
        holder.viewDataBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return cryptoCurrencyList != null ? cryptoCurrencyList.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(cryptoCurrencyList.get(position).id);
    }

    private RecyclerViewCryptoCurrencyListItemBinding createBinding(ViewGroup parent) {
        final RecyclerViewCryptoCurrencyListItemBinding recyclerViewCryptoCurrencyListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_crypto_currency_list_item, parent, false, fragmentBindingComponent);
        recyclerViewCryptoCurrencyListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                cryptoCurrencyClickCallback.onItemClick(recyclerViewCryptoCurrencyListItemBinding.getCryptoCurrencyDetails().id, recyclerViewCryptoCurrencyListItemBinding.ivCurrencyLogo, recyclerViewCryptoCurrencyListItemBinding.tvCurrencyName);
            }
        });
        recyclerViewCryptoCurrencyListItemBinding.llCurrencyFavourite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                cryptoCurrencyClickCallback.onFavoritesClick(recyclerViewCryptoCurrencyListItemBinding.getCryptoCurrencyDetails(), ! recyclerViewCryptoCurrencyListItemBinding.getCryptoCurrencyDetails().isFavorite);
            }
        });
        return recyclerViewCryptoCurrencyListItemBinding;
    }

    public interface CryptoCurrencyClickCallback {

        void onItemClick(String id, ImageView sharedImageView, TextView sharedTextView);

        void onFavoritesClick(CryptoCurrency cryptoCurrency, boolean shouldAdd);
    }
}