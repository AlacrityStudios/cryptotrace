package com.alacritystudios.cryptotrace.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.databinding.RecyclerViewSimpleCryptoCurrencyListItemBinding;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

/**
 * @author Anuj Dutt
 *         An adapter to display a simple list of the cryptocurrency coins.
 */

public class CryptoCurrencyListSelectionAdapter extends DataBoundRecyclerViewAdapter<CryptoCurrency, RecyclerViewSimpleCryptoCurrencyListItemBinding> {

    CryptoCurrencyListSelectionAdapter.CryptoCurrencyClickCallback cryptoCurrencyClickCallback;

    public CryptoCurrencyListSelectionAdapter(android.databinding.DataBindingComponent dataBindingComponent, CryptoCurrencyClickCallback cryptoCurrencyClickCallback) {
        super(dataBindingComponent);
        this.cryptoCurrencyClickCallback = cryptoCurrencyClickCallback;
    }

    @Override
    RecyclerView.ViewHolder createBinding(ViewGroup parent, int viewType) {
        final RecyclerViewSimpleCryptoCurrencyListItemBinding recyclerViewSimpleCryptoCurrencyListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_view_simple_crypto_currency_list_item, parent, false, dataBindingComponent);
        recyclerViewSimpleCryptoCurrencyListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                cryptoCurrencyClickCallback.onItemClick(recyclerViewSimpleCryptoCurrencyListItemBinding.getCryptoCurrencyDetails());
            }
        });
        return new ObjectViewHolder(recyclerViewSimpleCryptoCurrencyListItemBinding);
    }

    @Override
    void bindDataToViewHolder(RecyclerView.ViewHolder holder, CryptoCurrency data) {
        ((ObjectViewHolder) holder).viewDataBinding.setCryptoCurrencyDetails(data);
    }

    @Override
    boolean checkItemsSimilarity(CryptoCurrency oldItem, CryptoCurrency newItem) {
        return oldItem.id.equals(newItem.id);
    }

    @Override
    boolean checkItemsContentsSimilarity(CryptoCurrency oldItem, CryptoCurrency newItem) {
        return oldItem.equals(newItem);
    }

    public interface CryptoCurrencyClickCallback {

        void onItemClick(CryptoCurrency cryptoCurrency);
    }
}
