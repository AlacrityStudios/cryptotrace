package com.alacritystudios.cryptotrace.glide;

import android.content.Context;

import com.alacritystudios.cryptotrace.application.CryptoTraceApplication;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.AppGlideModule;

import java.io.InputStream;

import javax.inject.Inject;

import okhttp3.OkHttpClient;

/**
 * @author Anuj Dutt
 *  A module to construct Glide implementation at compile time and add a global application OkHttpClient to it.
 */

@GlideModule
public class CryptoTraceGlideModule extends AppGlideModule{

    @Inject
    OkHttpClient okHttpClient;

    public CryptoTraceGlideModule() {

        CryptoTraceApplication.getInstance().getApplicationComponent().inject(this);
    }

    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {
        super.registerComponents(context, glide, registry);
        OkHttpUrlLoader.Factory factory = new OkHttpUrlLoader.Factory(okHttpClient);
        registry.append(GlideUrl.class, InputStream.class, factory);
    }
}
