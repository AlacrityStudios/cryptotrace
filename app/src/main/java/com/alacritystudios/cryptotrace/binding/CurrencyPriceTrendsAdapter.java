package com.alacritystudios.cryptotrace.binding;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.alacritystudios.cryptotrace.R;

/**
 * @author Anuj Dutt
 *         A binding adapter for custom bindings at the fragment level.
 */

public class CurrencyPriceTrendsAdapter {

    @BindingAdapter("trendingValue")
    public void dateValue(ImageView imageView, String value) {
        if (value != null) {
            try {
                Double valueDouble = Double.parseDouble(value);
                if (valueDouble > 0.0) {
                    imageView.setImageResource(R.drawable.ic_trending_up_24dp);
                } else if (valueDouble < 0.0) {
                    imageView.setImageResource(R.drawable.ic_trending_down_24dp);
                } else {
                    imageView.setImageResource(R.drawable.ic_trending_flat_24dp);
                }
            } catch (NumberFormatException e) {
                imageView.setImageResource(R.drawable.ic_trending_flat_24dp);
            }
        } else {
            imageView.setImageResource(R.drawable.ic_trending_flat_24dp);
        }
    }
}
