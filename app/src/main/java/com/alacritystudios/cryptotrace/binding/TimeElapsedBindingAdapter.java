package com.alacritystudios.cryptotrace.binding;

import android.databinding.BindingAdapter;
import android.widget.TextView;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;

/**
 * @author Anuj Dutt
 *         A binding adapter for custom bindings at the fragment level.
 */

public class TimeElapsedBindingAdapter {

    private PrettyTime prettyTime;

    public TimeElapsedBindingAdapter(PrettyTime prettyTime) {
        this.prettyTime = prettyTime;
    }

    @BindingAdapter("app:timeElapsed")
    public void dateValue(TextView textView, Long value) {
        textView.setText(prettyTime.format(new Date(value * 1000)));
    }
}
