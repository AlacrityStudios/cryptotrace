package com.alacritystudios.cryptotrace.binding;

import android.support.v4.app.Fragment;

import org.ocpsoft.prettytime.PrettyTime;

/**
 * @author Anuj Dutt
 *         Data binding component to include custom attributes.
 */

public class FragmentBindingComponent implements android.databinding.DataBindingComponent {

    private Fragment fragment;

    public FragmentBindingComponent(Fragment fragment) {
        this.fragment = fragment;
    }

    public FragmentBindingComponent() {
    }

    @Override
    public ImageBindingAdapter getImageBindingAdapter() {
        return new ImageBindingAdapter();
    }

    @Override
    public DateBindingAdapter getDateBindingAdapter() {
        return new DateBindingAdapter();
    }

    @Override
    public TimeElapsedBindingAdapter getTimeElapsedBindingAdapter() {
        return new TimeElapsedBindingAdapter(new PrettyTime());
    }

    @Override
    public CurrencyPriceTrendsAdapter getCurrencyPriceTrendsAdapter() {
        return new CurrencyPriceTrendsAdapter();
    }
}
