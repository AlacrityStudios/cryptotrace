package com.alacritystudios.cryptotrace.binding;

import android.databinding.BindingAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Anuj Dutt
 *         A binding adapter for custom bindings at the fragment level.
 */

public class DateBindingAdapter {

    @BindingAdapter("dateValue")
    public void dateValue(TextView textView, Integer value) {
        if (value != null) {
            textView.setText(new SimpleDateFormat("dd MMM", Locale.getDefault()).format(new Date((long) value * 1000)));
        } else {
            textView.setText("-");
        }
    }
}
