package com.alacritystudios.cryptotrace.binding;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.glide.GlideApp;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.transition.Transition;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Anuj Dutt
 *         A binding adapter for custom bindings at the fragment level.
 */

public class ImageBindingAdapter {

    @BindingAdapter("imageUrl")
    public void imageUrl(ImageView imageView, String imageUrl) {
        RequestOptions requestOptions = new RequestOptions()
                .fitCenter()
                .dontAnimate();
        Glide.with(imageView.getContext().getApplicationContext())
                .load(imageUrl)
                .apply(requestOptions)
                .into(imageView);
        imageView.setClipToOutline(true);
    }

    public void imageUrl(CircleImageView imageView, String imageUrl) {
        GlideApp.with(imageView.getContext().getApplicationContext())
                .load(imageUrl)
                .centerCrop()
                .dontAnimate()
                .into(imageView);
    }

    @BindingAdapter({"app:imageUrl", "app:placeholder"})
    public void imageUrl(ImageView imageView, String imageUrl, @DrawableRes int placeholder) {
        RequestOptions requestOptions = new RequestOptions()
                .fitCenter()
                .dontAnimate()
                .placeholder(placeholder);
        Glide.with(imageView.getContext().getApplicationContext())
                .load(imageUrl)
                .apply(requestOptions)
                .into(imageView);
        imageView.setClipToOutline(true);
    }

    @BindingAdapter({"app:imageTopUrl", "app:placeholder", "app:drawableSize"})
    public void imageTopUrl(final TextView textView, String imageUrl, @DrawableRes final int placeholder, @DimenRes final int drawableSize) {
        final int drawableSizeInPx = (int) textView.getResources().getDimension(drawableSize);
        RequestOptions requestOptions = new RequestOptions()
                .fitCenter()
                .dontAnimate()
                .placeholder(placeholder);
        Glide.with(textView.getContext().getApplicationContext())
                .load(imageUrl)
                .apply(requestOptions)
                .into(new BaseTarget<Drawable>() {

                    @Override
                    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                        if (resource != null) {
                            textView.setCompoundDrawablesWithIntrinsicBounds(null, resource, null, null);
                        }
                    }

                    @Override
                    public void getSize(SizeReadyCallback cb) {
                        cb.onSizeReady(drawableSizeInPx, drawableSizeInPx);
                    }

                    @Override
                    public void removeCallback(SizeReadyCallback cb) {

                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholderDrawable) {
                        super.onLoadCleared(placeholderDrawable);
                        if (placeholderDrawable != null) {
                            placeholderDrawable.setBounds(0, 0, drawableSizeInPx, drawableSizeInPx);
                            textView.setCompoundDrawables(null, placeholderDrawable, null, null);
                        }
                    }

                    @Override
                    public void onLoadStarted(@Nullable Drawable placeholderDrawable) {
                        super.onLoadStarted(placeholderDrawable);
                        if (placeholderDrawable != null) {
                            placeholderDrawable.setBounds(0, 0, drawableSizeInPx, drawableSizeInPx);
                            textView.setCompoundDrawables(null, placeholderDrawable, null, null);
                        }
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        if (errorDrawable != null) {
                            errorDrawable.setBounds(0, 0, drawableSizeInPx, drawableSizeInPx);
                            textView.setCompoundDrawables(null, errorDrawable, null, null);
                        }
                    }
                });
    }
}
