package com.alacritystudios.cryptotrace.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import java.util.List;

/**
 * @author Anuj Dutt
 *         DAO to make database queries and alterations.
 */

@Dao
public interface FiatCurrencyDao {

    @Query("SELECT * FROM fiatcurrency ORDER BY fullName")
    LiveData<List<FiatCurrency>> fetchAllFiatCurrencies();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveFiatCurrencyList(List<FiatCurrency> fiatCurrencies);

    @Query("SELECT * FROM fiatcurrency where name = :symbol")
    LiveData<FiatCurrency> fetchFiatCurrencyBySymbol(String symbol);

    @Query("SELECT * FROM fiatcurrency where coinName like '%' || :name || '%' ORDER BY fullName LIMIT(20)")
    LiveData<List<FiatCurrency>> getSimpleFiatCurrencyListByName(String name);
}
