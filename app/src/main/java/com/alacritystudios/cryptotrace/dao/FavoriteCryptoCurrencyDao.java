package com.alacritystudios.cryptotrace.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.alacritystudios.cryptotrace.viewobject.FavoriteCryptoCurrency;

import java.util.List;

/**
 * @author Anuj Dutt
 *         Data Access Object to perform database functions on the FavoriteCryptoCurrency entity table.
 */

@Dao
public interface FavoriteCryptoCurrencyDao {

    @Query("SELECT * FROM FAVORITE_CRYPTOCURRENCY")
    public abstract LiveData<List<FavoriteCryptoCurrency>> getAllFavoriteCryptoCurrencies();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void saveFavoriteCryptoCurrency(FavoriteCryptoCurrency favoriteCryptoCurrency);

//    @Delete
//    public abstract void deleteFavoriteCryptoCurrency(FavoriteCryptoCurrency favoriteCryptoCurrency);

    @Query("DELETE FROM FAVORITE_CRYPTOCURRENCY WHERE crypto_currency_id = :cryptoCurrencyId")
    public abstract void deleteFavoriteCryptoCurrency(String cryptoCurrencyId);
}
