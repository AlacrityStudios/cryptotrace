package com.alacritystudios.cryptotrace.dao;

import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.alacritystudios.cryptotrace.viewobject.Exchange;

import java.util.List;


/**
 * @author Anuj Dutt
 *         Dao Access Object for manipulation of Exchange table.
 */

@Dao
public interface ExchangeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveExchangeList(List<Exchange> exchanges);

    @Query("SELECT * FROM exchange ORDER BY name")
    LiveData<List<Exchange>> fetchExchangeList();

    @Query("SELECT * FROM exchange where internalName = :symbol")
    LiveData<Exchange> fetchExchangeDetailsBySymbol(String symbol);

    @Query("SELECT * FROM exchange where name = :name")
    LiveData<List<Exchange>> fetchExchangeListByName(String name);

    @Query("SELECT * FROM exchange where name like '%' || :name || '%'  LIMIT(10)")
    LiveData<List<Exchange>> fetchSimpleExchangeListByName(String name);

}
