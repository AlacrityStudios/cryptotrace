package com.alacritystudios.cryptotrace.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.alacritystudios.cryptotrace.viewobject.AlertDisplayDetails;

import java.util.List;

/**
 * @author Anuj Dutt
 *         A DataAccessObject designed to fetch all alert details.
 */

@Dao
public interface AlertDetailsDao {

    @Query("SELECT alert.id, alert.crypto_currency_id, alert.fiat_currency_id, alert.exchange_id, alert.alert_price, alert.fetched_price, " +
            "cryptocurrency.symbol as cryptoCurrencyDisplayName, fiatcurrency.name as fiatCurrencyDisplayName, exchange.name as exchangeDisplayName, " +
            "cryptocurrency.imageUrl as cryptoCurrencyImageUrl, fiatcurrency.imageUrl as fiatCurrencyImageUrl, exchange.logoUrl as exchangeImageUrl " +
            "FROM alert " +
            "INNER JOIN cryptocurrency ON alert.crypto_currency_id = cryptocurrency.id " +
            "INNER JOIN fiatcurrency ON alert.fiat_currency_id = fiatcurrency.id " +
            "INNER JOIN exchange ON alert.exchange_id = exchange.id ORDER BY cryptocurrency.symbol")
    LiveData<List<AlertDisplayDetails>> fetchAllAlertDisplayDetails();
}