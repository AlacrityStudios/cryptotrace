package com.alacritystudios.cryptotrace.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

import java.util.List;

/**
 * @author Anuj Dutt
 *         DAO to make database queries and alterations.
 */

@Dao
public interface CryptoCurrencyDao {

    @Query("SELECT (EXISTS(SELECT * FROM favorite_cryptocurrency where crypto_currency_id = CryptoCurrency.id)) " +
            "AS isFavorite, id, coinName, symbol, algorithm, proofType, imageUrl FROM cryptocurrency " +
            "ORDER BY CAST(sortOrder AS INTEGER)")
    LiveData<List<CryptoCurrency>> fetchBasicCryptoCurrencyList();

    @Query("SELECT * FROM cryptocurrency where (EXISTS(SELECT * FROM favorite_cryptocurrency where crypto_currency_id = CryptoCurrency.id))")
    LiveData<List<CryptoCurrency>> getAllFavoriteCryptoCurrencies();

    @Query("SELECT (EXISTS(SELECT * FROM favorite_cryptocurrency where crypto_currency_id = CryptoCurrency.id)) " +
            "AS isFavorite, id, coinName, symbol, algorithm, proofType, imageUrl FROM cryptocurrency " +
            "WHERE coinName like '%' || :coinName || '%' ORDER BY CAST(sortOrder AS INTEGER)")
    LiveData<List<CryptoCurrency>> getCryptoCurrencyListByFullName(String coinName);

    @Query("SELECT id, coinName, symbol, algorithm, proofType, imageUrl FROM cryptocurrency " +
            "WHERE coinName like '%' || :coinName || '%' ORDER BY CAST(sortOrder AS INTEGER) LIMIT(10)")
    LiveData<List<CryptoCurrency>> getSimpleCryptoCurrencyListByFullName(String coinName);

    @Query("SELECT * FROM cryptocurrency WHERE id = :id")
    LiveData<CryptoCurrency> fetchFavoriteCryptoCurrencyDetailsById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveCryptoCurrencyList(List<CryptoCurrency> cryptoCurrencies);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateCryptoCurrency(CryptoCurrency cryptoCurrency);
}
