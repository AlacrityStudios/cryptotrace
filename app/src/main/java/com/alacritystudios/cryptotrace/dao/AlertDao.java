package com.alacritystudios.cryptotrace.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.alacritystudios.cryptotrace.viewobject.Alert;

import java.util.List;

/**
 * @author Anuj Dutt
 *         Dao Access Object for manipulation of Alert table.
 */

@Dao
public interface AlertDao {

    @Query("SELECT * FROM alert")
    LiveData<List<Alert>> fetchAlertList();

    @Insert
    Long insertAlert(Alert alert);
}
