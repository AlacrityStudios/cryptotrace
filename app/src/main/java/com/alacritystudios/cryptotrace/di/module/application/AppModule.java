package com.alacritystudios.cryptotrace.di.module.application;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.alacritystudios.cryptotrace.application.CryptoTraceApplication;
import com.alacritystudios.cryptotrace.db.CryptoTraceDatabase;
import com.alacritystudios.cryptotrace.di.scope.CryptoTraceScope;
import com.alacritystudios.cryptotrace.util.AppExecutor;
import com.alacritystudios.cryptotrace.util.CacheState;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * @author Anuj Dutt
 *         Module to provide the application dependency to the graph. This module requires an external
 *         application dependency.
 */

@Module
public class AppModule {

    CryptoTraceDatabase cryptoTraceDatabase;

    @CryptoTraceScope
    @Provides
    @Named("ApplicationContext")
    public Context getCryptoTraceApplication(CryptoTraceApplication cryptoTraceApplication) {
        return cryptoTraceApplication.getApplicationContext();
    }

    @CryptoTraceScope
    @Provides
    CryptoTraceDatabase cryptoTraceDatabase(final CryptoTraceApplication cryptoTraceApplication, final AppExecutor appExecutor) {
        cryptoTraceDatabase = Room.databaseBuilder(cryptoTraceApplication, CryptoTraceDatabase.class, "CryptoTrace.db").addCallback(new RoomDatabase.Callback() {

            @Override
            public void onCreate(@NonNull final SupportSQLiteDatabase db) {
                super.onCreate(db);
                appExecutor.diskIO.execute(new Runnable() {

                    @Override
                    public void run() {
                        List<FiatCurrency> fiatCurrencies = new ArrayList<>();
                        fiatCurrencies.add(new FiatCurrency("BTC", "Bitcoin", "BTC (Bitcoin)", "Ƀ", "https://www.cryptocompare.com/media/19633/btc.png"));
                        fiatCurrencies.add(new FiatCurrency("ETH", "Ethereum", "ETH (Ethereum)","Ξ", "https://www.cryptocompare.com/media/20646/eth_logo.png"));
                        fiatCurrencies.add(new FiatCurrency("AUD", "Australian Dollar", "AUD (Australian Dollar)", "A$", "http://flagpedia.net/data/flags/normal/au.png"));
                        fiatCurrencies.add(new FiatCurrency("INR", "Indian Rupee", "INR (Indian Rupee)", "₹", "http://flagpedia.net/data/flags/normal/in.png"));
                        fiatCurrencies.add(new FiatCurrency("EUR", "Euro", "EUR (Euro)", "€", "https://europa.eu/european-union/sites/europaeu/files/docs/body/flag_yellow_high.jpg"));
                        fiatCurrencies.add(new FiatCurrency("SGD", "Singapore Dollar", "SGD (Singapore Dollar)", "S$", "http://flagpedia.net/data/flags/normal/sg.png"));
                        fiatCurrencies.add(new FiatCurrency("USD", "US Dollar", "USD (US Dollar)", "$", "http://flagpedia.net/data/flags/normal/us.png"));
                        fiatCurrencies.add(new FiatCurrency("CNY", "Chinese Yuan Renminbi", "CNY (Chinese Yuan Renminbi)", "￥", "http://flagpedia.net/data/flags/normal/cn.png"));
                        fiatCurrencies.add(new FiatCurrency("RUB", "Russian Ruble", "RUB (Russian Ruble)", "\u20BD", "http://flagpedia.net/data/flags/normal/ru.png"));
                        fiatCurrencies.add(new FiatCurrency("BRL", "Brazilian Real", "BRL (Brazilian Real)",  "R$","http://flagpedia.net/data/flags/normal/br.png"));
                        fiatCurrencies.add(new FiatCurrency("JPY", "Japanese Yen", "JPY (Japanese Yen)",  "¥","http://flagpedia.net/data/flags/normal/jp.png"));
                        fiatCurrencies.add(new FiatCurrency("CAD", "Canadian Dollar", "CAD (Canadian Dollar)", "C$", "http://flagpedia.net/data/flags/normal/ca.png"));
                        cryptoTraceDatabase.fiatCurrencyDao().saveFiatCurrencyList(fiatCurrencies);
                        List<Exchange> exchanges = new ArrayList<>();
                        exchanges.add(new Exchange("0", "Aggregate", "/media/20567/cc-logo-vert.png", "CCCAGG", "INT", true));
                        cryptoTraceDatabase.exchangeDao().saveExchangeList(exchanges);
                    }
                });

            }
        }).build();
        return cryptoTraceDatabase;
    }

    @CryptoTraceScope
    @Provides
    AppExecutor appExecutor() {
        return new AppExecutor();
    }

    @CryptoTraceScope
    @Provides
    CacheState cacheState() {
        return new CacheState();
    }
}
