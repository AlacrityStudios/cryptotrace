package com.alacritystudios.cryptotrace.di.component;

import com.alacritystudios.cryptotrace.application.CryptoTraceApplication;
import com.alacritystudios.cryptotrace.di.module.application.ActivityModule;
import com.alacritystudios.cryptotrace.di.module.application.AppModule;
import com.alacritystudios.cryptotrace.di.module.application.CryptoCompareApiModule;
import com.alacritystudios.cryptotrace.di.module.application.DaoModule;
import com.alacritystudios.cryptotrace.di.module.application.FragmentModule;
import com.alacritystudios.cryptotrace.di.module.application.NetworkModule;
import com.alacritystudios.cryptotrace.di.module.application.RepositoryModule;
import com.alacritystudios.cryptotrace.di.module.application.ViewModelProviderModule;
import com.alacritystudios.cryptotrace.di.scope.CryptoTraceScope;
import com.alacritystudios.cryptotrace.glide.CryptoTraceGlideModule;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * @author Anuj Dutt
 *         Main application component providing application level dependencies.
 *         Dependencies are provided using modules.
 */

@CryptoTraceScope
@Component(modules = {
        AndroidInjectionModule.class,
        ActivityModule.class,
        FragmentModule.class,
        AppModule.class,
        ViewModelProviderModule.class,
        NetworkModule.class,
        CryptoCompareApiModule.class,
        RepositoryModule.class,
        DaoModule.class
})
public interface ApplicationComponent {

    void inject(CryptoTraceApplication application);

    void inject(CryptoTraceGlideModule cryptoTraceGlideModule);

    @Component.Builder
    public interface Builder {

        @BindsInstance
        Builder cryptoTraceApplication(CryptoTraceApplication cryptoTraceApplication);

        ApplicationComponent build();
    }
}
