package com.alacritystudios.cryptotrace.di.module.mainactivity;

import com.alacritystudios.cryptotrace.ui.main.MainActivity;
import com.alacritystudios.cryptotrace.ui.main.MainActivityNavigator;

import dagger.Module;
import dagger.Provides;

/**
 * @author Anuj Dutt
 */
@Module
public class MainActivityModule {

    @Provides
    MainActivityNavigator mainActivityNavigator(MainActivity mainActivity) {
        return new MainActivityNavigator(mainActivity);
    }
}