package com.alacritystudios.cryptotrace.di.module.application;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.alacritystudios.cryptotrace.di.key.ViewModelKey;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivityViewModel;
import com.alacritystudios.cryptotrace.ui.dialog.SelectFiatCurrencyDialogFragment;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.AddNewAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.AlertListFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyPriceFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencySocialStatsFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyStatsFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyListViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoTraceViewModelFactory;
import com.alacritystudios.cryptotrace.viewmodel.ExchangeListViewModel;
import com.alacritystudios.cryptotrace.viewmodel.HomeFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.NewsListFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SearchCryptoCurrencyViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectCryptoCurrencyFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectExchangeFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectFiatCurrencyFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectPriceFragmentViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * @author Anuj Dutt
 *         A module for creating bindings for various ViewModels and their providers.
 */


@Module
public abstract class ViewModelProviderModule {

    @IntoMap
    @Binds
    @ViewModelKey(HomeFragmentViewModel.class)
    abstract ViewModel contributesHomeFragmentViewModel(HomeFragmentViewModel homeFragmentViewModel);

    @IntoMap
    @Binds
    @ViewModelKey(CryptoCurrencyListViewModel.class)
    abstract ViewModel contributesCryptoCurrencyListViewModel(CryptoCurrencyListViewModel cryptoCurrencyListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ExchangeListViewModel.class)
    abstract ViewModel contributesExchangeListViewModel(ExchangeListViewModel exchangeListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NewsListFragmentViewModel.class)
    abstract ViewModel contributesNewsListFragmentViewModel(NewsListFragmentViewModel newsListFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SearchCryptoCurrencyViewModel.class)
    abstract ViewModel contributesSearchCryptoCurrencyViewModel(SearchCryptoCurrencyViewModel searchCryptoCurrencyViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CryptoDetailsActivityViewModel.class)
    abstract ViewModel contributesCryptoDetailsActivityViewModel(CryptoDetailsActivityViewModel cryptoDetailsActivityViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CryptoCurrencyStatsFragmentViewModel.class)
    abstract ViewModel contributesCryptoCurrencyStatsFragmentViewModel(CryptoCurrencyStatsFragmentViewModel cryptoCurrencyStatsFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CryptoCurrencyPriceFragmentViewModel.class)
    abstract ViewModel contributesCryptoCurrencyPriceFragmentViewModel(CryptoCurrencyPriceFragmentViewModel cryptoCurrencyPriceFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CryptoCurrencySocialStatsFragmentViewModel.class)
    abstract ViewModel contributesCryptoCurrencySocialStatsFragmentViewModel(CryptoCurrencySocialStatsFragmentViewModel cryptoCurrencySocialStatsFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AddAlertFragmentViewModel.class)
    abstract ViewModel contributesAddAlertFragmentViewModel(AddAlertFragmentViewModel addAlertFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AddAlertActivityViewModel.class)
    abstract ViewModel contributesAddAlertActivityViewModel(AddAlertActivityViewModel addAlertActivityViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SelectCryptoCurrencyFragmentViewModel.class)
    abstract ViewModel contributesSelectCryptoCurrencyFragmentViewModel(SelectCryptoCurrencyFragmentViewModel selectCryptoCurrencyFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SelectFiatCurrencyFragmentViewModel.class)
    abstract ViewModel contributesSelectFiatCurrencyFragmentViewModel(SelectFiatCurrencyFragmentViewModel selectFiatCurrencyFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SelectExchangeFragmentViewModel.class)
    abstract ViewModel contributesSelectExchangeFragmentViewModel(SelectExchangeFragmentViewModel selectExchangeFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SelectPriceFragmentViewModel.class)
    abstract ViewModel contributesSelectPriceFragmentViewModel(SelectPriceFragmentViewModel selectPriceFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AddNewAlertActivityViewModel.class)
    abstract ViewModel contributesAddNewAlertActivityViewModel(AddNewAlertActivityViewModel addNewAlertActivityViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AlertListFragmentViewModel.class)
    abstract ViewModel contributesAlertListFragmentViewModel(AlertListFragmentViewModel alertListFragmentViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(CryptoTraceViewModelFactory factory);
}
