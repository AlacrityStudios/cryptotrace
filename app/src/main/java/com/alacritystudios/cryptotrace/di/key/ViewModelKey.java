package com.alacritystudios.cryptotrace.di.key;

import android.arch.lifecycle.ViewModel;

import dagger.MapKey;

/**
 * @author Anuj Dutt
 *  A key to declare bindings into viewmodel provider map
 */


@MapKey
public @interface ViewModelKey {

    Class<? extends ViewModel> value();
}
