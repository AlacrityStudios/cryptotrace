package com.alacritystudios.cryptotrace.di.module.application;

import android.support.v4.app.Fragment;

import com.alacritystudios.cryptotrace.ui.addalert.addcryptocurrencyfragment.AddCryptoCurrencyFragment;
import com.alacritystudios.cryptotrace.ui.addalert.selectcryptocurrency.SelectCryptoCurrencyFragment;
import com.alacritystudios.cryptotrace.ui.addalert.selectexchange.SelectExchangeFragment;
import com.alacritystudios.cryptotrace.ui.addalert.selectfiatcurrency.SelectFiatCurrencyFragment;
import com.alacritystudios.cryptotrace.ui.addalert.selectprice.SelectPriceFragment;
import com.alacritystudios.cryptotrace.ui.cryptodetails.converter.CryptoCurrencyConverterFragment;
import com.alacritystudios.cryptotrace.ui.cryptodetails.price.CryptoCurrencyPriceInformationFragment;
import com.alacritystudios.cryptotrace.ui.cryptodetails.social.CryptoCurrencySocialStatsFragment;
import com.alacritystudios.cryptotrace.ui.cryptodetails.stats.CryptoCurrencyStatsFragment;
import com.alacritystudios.cryptotrace.ui.dialog.SelectCryptoCurrencyDialogFragment;
import com.alacritystudios.cryptotrace.ui.dialog.SelectExchangeDialogFragment;
import com.alacritystudios.cryptotrace.ui.dialog.SelectFiatCurrencyDialogFragment;
import com.alacritystudios.cryptotrace.ui.main.alertslist.AlertListFragment;
import com.alacritystudios.cryptotrace.ui.main.cryptocurrencylist.CryptoCurrencyListFragment;
import com.alacritystudios.cryptotrace.ui.main.home.HomeFragment;
import com.alacritystudios.cryptotrace.ui.main.newslist.NewsListFragment;
import com.alacritystudios.cryptotrace.ui.searchcrypto.search.SearchCryptoCurrencyFragment;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;

/**
 * @author Anuj Dutt
 *         A module to provide Andorid framework classes of fragments to the applciation.
 */

@Module(subcomponents = {FragmentModule.CryptoCurrencyListSubComponent.class,
        FragmentModule.NewsListFragmentSubComponent.class, FragmentModule.SearchCryptoCurrencyFragmentSubComponent.class,
        FragmentModule.CryptoCurrencyPriceFragmentSubComponent.class, FragmentModule.CryptoCurrencyHistogramFragmentSubComponent.class,
        FragmentModule.CryptoCurrencyStatsFragmentSubComponent.class, FragmentModule.CryptoCurrencyConverterFragmentSubComponent.class,
        FragmentModule.AddAlertFragmentSubComponent.class, FragmentModule.SelectCryptoCurrencyFragmentSubComponent.class,
        FragmentModule.SelectFiatCurrencyFragmentSubComponent.class, FragmentModule.SelectExchangeFragmentSubComponent.class,
        FragmentModule.SelectPriceFragmentSubComponent.class, FragmentModule.SelectCryptoCurrencyDialogFragmentSubComponent.class,
        FragmentModule.SelectFiatCurrencyDialogFragmentSubComponent.class, FragmentModule.SelectExchangeDialogFragmentSubComponent.class,
        FragmentModule.AlertListFragmentSubComponent.class})
public abstract class FragmentModule {

    // @CryptoTraceScope
    @ContributesAndroidInjector
    public abstract HomeFragment homeFragment();

    @Binds
    @IntoMap
    @FragmentKey(CryptoCurrencyListFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesCryptoCurrencyListFragmentFactory(CryptoCurrencyListSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(NewsListFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesNewsListFragmentFactory(NewsListFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(SearchCryptoCurrencyFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesSearchCryptoCurrencyFragmentFactory(SearchCryptoCurrencyFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(CryptoCurrencyPriceInformationFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesCryptoCurrencyPriceFragmentFactory(CryptoCurrencyPriceFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(CryptoCurrencySocialStatsFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesCryptoCurrencyHistogramFragmentFactory(CryptoCurrencyHistogramFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(CryptoCurrencyStatsFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesCryptoCurrencyStatsFragmentFactory(CryptoCurrencyStatsFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(CryptoCurrencyConverterFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesCryptoCurrencyConverterFragmentFactory(CryptoCurrencyConverterFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(AddCryptoCurrencyFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesAddAlertFragmentFactory(AddAlertFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(SelectCryptoCurrencyFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesSelectCryptoCurrencyFragmentFactory(SelectCryptoCurrencyFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(SelectFiatCurrencyFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesSelectFiatCurrencyFragmentFactory(SelectFiatCurrencyFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(SelectExchangeFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesSelectExchangeFragmentFactory(SelectExchangeFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(SelectPriceFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesSelectPriceFragmentFactory(SelectPriceFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(SelectCryptoCurrencyDialogFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesSelectCryptoCurrencyDialogFragmentFactory(SelectCryptoCurrencyDialogFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(SelectExchangeDialogFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesSelectExchangeDialogFragmentFactory(SelectExchangeDialogFragmentSubComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(AlertListFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesAlertListFragmentFactory(AlertListFragmentSubComponent.Builder builder);


    @Binds
    @IntoMap
    @FragmentKey(SelectFiatCurrencyDialogFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> contributesSelectFiatCurrencyDialogFragmentFactory(SelectFiatCurrencyDialogFragmentSubComponent.Builder builder);

    @Subcomponent(modules = {})
    public interface CryptoCurrencyListSubComponent extends AndroidInjector<CryptoCurrencyListFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<CryptoCurrencyListFragment> {

        }
    }

    @Subcomponent
    public interface NewsListFragmentSubComponent extends AndroidInjector<NewsListFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<NewsListFragment> {

        }
    }

    @Subcomponent
    public interface SearchCryptoCurrencyFragmentSubComponent extends AndroidInjector<SearchCryptoCurrencyFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<SearchCryptoCurrencyFragment> {

        }
    }

    @Subcomponent
    public interface CryptoCurrencyPriceFragmentSubComponent extends AndroidInjector<CryptoCurrencyPriceInformationFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<CryptoCurrencyPriceInformationFragment> {

        }
    }

    @Subcomponent
    public interface CryptoCurrencyHistogramFragmentSubComponent extends AndroidInjector<CryptoCurrencySocialStatsFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<CryptoCurrencySocialStatsFragment> {

        }
    }

    @Subcomponent
    public interface CryptoCurrencyStatsFragmentSubComponent extends AndroidInjector<CryptoCurrencyStatsFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<CryptoCurrencyStatsFragment> {

        }
    }

    @Subcomponent
    public interface CryptoCurrencyConverterFragmentSubComponent extends AndroidInjector<CryptoCurrencyConverterFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<CryptoCurrencyConverterFragment> {

        }
    }

    @Subcomponent
    public interface AddAlertFragmentSubComponent extends AndroidInjector<AddCryptoCurrencyFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<AddCryptoCurrencyFragment> {

        }
    }

    @Subcomponent
    public interface SelectCryptoCurrencyFragmentSubComponent extends AndroidInjector<SelectCryptoCurrencyFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<SelectCryptoCurrencyFragment> {

        }
    }

    @Subcomponent
    public interface SelectFiatCurrencyFragmentSubComponent extends AndroidInjector<SelectFiatCurrencyFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<SelectFiatCurrencyFragment> {

        }
    }

    @Subcomponent
    public interface SelectExchangeFragmentSubComponent extends AndroidInjector<SelectExchangeFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<SelectExchangeFragment> {

        }
    }

    @Subcomponent
    public interface SelectPriceFragmentSubComponent extends AndroidInjector<SelectPriceFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<SelectPriceFragment> {

        }
    }

    @Subcomponent
    public interface SelectCryptoCurrencyDialogFragmentSubComponent extends AndroidInjector<SelectCryptoCurrencyDialogFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<SelectCryptoCurrencyDialogFragment> {

        }
    }

    @Subcomponent
    public interface SelectFiatCurrencyDialogFragmentSubComponent extends AndroidInjector<SelectFiatCurrencyDialogFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<SelectFiatCurrencyDialogFragment> {

        }
    }

    @Subcomponent
    public interface SelectExchangeDialogFragmentSubComponent extends AndroidInjector<SelectExchangeDialogFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<SelectExchangeDialogFragment> {

        }
    }

    @Subcomponent
    public interface AlertListFragmentSubComponent extends AndroidInjector<AlertListFragment> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<AlertListFragment> {

        }
    }


}