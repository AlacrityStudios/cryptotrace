package com.alacritystudios.cryptotrace.di.module.application;

import android.support.annotation.NonNull;

import com.alacritystudios.cryptotrace.di.scope.CryptoTraceScope;
import com.alacritystudios.cryptotrace.rest.ApiResponseCallAdapterFactory;
import com.alacritystudios.cryptotrace.rest.CryptoCompareApiService;
import com.alacritystudios.cryptotrace.rest.CryptoCompareMinApiService;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Anuj Dutt
 *         Module to provide methods for
 */

@Module
public class CryptoCompareApiModule {

    @CryptoTraceScope
    @Provides
    @Named("cryptoCompareMinApiRetrofit")
    public Retrofit cryptoCompareMinApiRetrofit(@NonNull ApiResponseCallAdapterFactory apiResponseCallAdapterFactory, @NonNull OkHttpClient okHttpClient) {

        return new Retrofit.Builder()
                .baseUrl(CryptoCompareMinApiService.BASE_URL)
                .addCallAdapterFactory(apiResponseCallAdapterFactory)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @CryptoTraceScope
    @Provides
    @Named("cryptoCompareApiRetrofit")
    public Retrofit cryptoCompareApiRetrofit(@NonNull ApiResponseCallAdapterFactory apiResponseCallAdapterFactory, @NonNull OkHttpClient okHttpClient) {

        return new Retrofit.Builder()
                .baseUrl(CryptoCompareApiService.BASE_URL)
                .addCallAdapterFactory(apiResponseCallAdapterFactory)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @CryptoTraceScope
    @Provides
    @Named("cryptoCompareMinApiRetrofitDefault")
    public Retrofit CryptoCompareMinApiRetrofitDefault(@NonNull OkHttpClient okHttpClient) {

        return new Retrofit.Builder()
                .baseUrl(CryptoCompareMinApiService.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @CryptoTraceScope
    @Provides
    @Named("LiveData")
    public CryptoCompareMinApiService cryptoCompareMinApiService(@Named("cryptoCompareMinApiRetrofit") Retrofit retrofit) {
        return retrofit.create(CryptoCompareMinApiService.class);
    }

    @CryptoTraceScope
    @Provides
    @Named("Default")
    public CryptoCompareMinApiService cryptoCompareMinApiServiceDefault(@Named("cryptoCompareMinApiRetrofitDefault") Retrofit retrofit) {
        return retrofit.create(CryptoCompareMinApiService.class);
    }

    @CryptoTraceScope
    @Provides
    public CryptoCompareApiService cryptoCompareApiService(@Named("cryptoCompareApiRetrofit") Retrofit retrofit) {
        return retrofit.create(CryptoCompareApiService.class);
    }
}
