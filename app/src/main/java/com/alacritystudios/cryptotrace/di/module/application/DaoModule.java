package com.alacritystudios.cryptotrace.di.module.application;

import com.alacritystudios.cryptotrace.dao.AlertDao;
import com.alacritystudios.cryptotrace.dao.AlertDetailsDao;
import com.alacritystudios.cryptotrace.dao.CryptoCurrencyDao;
import com.alacritystudios.cryptotrace.dao.ExchangeDao;
import com.alacritystudios.cryptotrace.dao.FavoriteCryptoCurrencyDao;
import com.alacritystudios.cryptotrace.dao.FiatCurrencyDao;
import com.alacritystudios.cryptotrace.db.CryptoTraceDatabase;

import dagger.Module;
import dagger.Provides;

/**
 * @author Anuj Dutt
 *         Module to provide Dao instances.
 */

@Module
public class DaoModule {

    @Provides
    CryptoCurrencyDao cryptoCurrencyDao(CryptoTraceDatabase cryptoTraceDatabase) {
        return cryptoTraceDatabase.cryptoCurrencyDao();
    }

    @Provides
    ExchangeDao exchangeDao(CryptoTraceDatabase cryptoTraceDatabase) {
        return cryptoTraceDatabase.exchangeDao();
    }

    @Provides
    FavoriteCryptoCurrencyDao favoriteCryptoCurrencyDao(CryptoTraceDatabase cryptoTraceDatabase) {
        return cryptoTraceDatabase.favoriteCryptoCurrencyDao();
    }

    @Provides
    FiatCurrencyDao fiatCurrencyDao(CryptoTraceDatabase cryptoTraceDatabase) {
        return cryptoTraceDatabase.fiatCurrencyDao();
    }

    @Provides
    AlertDao alertDao(CryptoTraceDatabase cryptoTraceDatabase) {
        return cryptoTraceDatabase.alertDao();
    }

    @Provides
    AlertDetailsDao alertDetailsDao(CryptoTraceDatabase cryptoTraceDatabase) {
        return cryptoTraceDatabase.alertDetailsDao();
    }
}
