package com.alacritystudios.cryptotrace.di.scope;

import javax.inject.Scope;

/**
 * @author Anuj Dutt
 *         Scope to define dependencies to have a global scope in the application.
 */

@Scope
public @interface CryptoTraceScope {

}
