package com.alacritystudios.cryptotrace.di.scope;

import javax.inject.Scope;

/**
 * @author Anuj Dutt
 *         Scope to define dependencies to have a scope inside MainActivity.
 */

@Scope
public @interface MainActivityScope {

}
