package com.alacritystudios.cryptotrace.di.module.application;

import android.app.Activity;

import com.alacritystudios.cryptotrace.di.module.mainactivity.MainActivityModule;
import com.alacritystudios.cryptotrace.ui.addalert.AddAlertActivity;
import com.alacritystudios.cryptotrace.ui.addnewalert.AddNewAlertActivity;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivity;
import com.alacritystudios.cryptotrace.ui.main.MainActivity;
import com.alacritystudios.cryptotrace.ui.searchcrypto.SearchCryptoCurrencyActivity;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

/**
 * @author Anuj Dutt
 *         Module to provide the android framework classes for the application.
 */

@Module(subcomponents = {ActivityModule.MainActivitySubComponent.class, ActivityModule.SearchCryptoCurrencyActivitySubComponent.class,
        ActivityModule.CryptoDetailsActivitySubComponent.class, ActivityModule.AddAlertActivitySubComponent.class, ActivityModule.AddNewAlertActivitySubComponent.class})
public abstract class ActivityModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity> provideMainActivityInjectorFactory(MainActivitySubComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(SearchCryptoCurrencyActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity> provideSearchCryptoCurrencyActivityInjectorFactory(SearchCryptoCurrencyActivitySubComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(CryptoDetailsActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity> provideCryptoDetailsActivityInjectorFactory(CryptoDetailsActivitySubComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(AddAlertActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity> provideAddAlertActivityInjectorFactory(AddAlertActivitySubComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(AddNewAlertActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity> provideAddNewAlertActivityInjectorFactory(AddNewAlertActivitySubComponent.Builder builder);

    @Subcomponent(modules = {MainActivityModule.class})
    public interface MainActivitySubComponent extends AndroidInjector<MainActivity> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<MainActivity> {

        }
    }

    @Subcomponent
    public interface SearchCryptoCurrencyActivitySubComponent extends AndroidInjector<SearchCryptoCurrencyActivity> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<SearchCryptoCurrencyActivity> {

        }
    }

    @Subcomponent
    public interface CryptoDetailsActivitySubComponent extends AndroidInjector<CryptoDetailsActivity> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<CryptoDetailsActivity> {

        }
    }

    @Subcomponent
    public interface AddAlertActivitySubComponent extends AndroidInjector<AddAlertActivity> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<AddAlertActivity> {

        }
    }

    @Subcomponent
    public interface AddNewAlertActivitySubComponent extends AndroidInjector<AddNewAlertActivity> {

        @Subcomponent.Builder
        public abstract class Builder extends AndroidInjector.Builder<AddNewAlertActivity> {

        }
    }

}