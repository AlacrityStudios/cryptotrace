package com.alacritystudios.cryptotrace.di.module.application;

import com.alacritystudios.cryptotrace.di.scope.CryptoTraceScope;
import com.alacritystudios.cryptotrace.rest.ApiResponseCallAdapterFactory;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

/**
 * @author Anuj Dutt
 *         Module to return common network dependencies.
 */

@Module
public class NetworkModule {

    @CryptoTraceScope
    @Provides
    ApiResponseCallAdapterFactory apiResponseCallAdapterFactory() {
        return new ApiResponseCallAdapterFactory();
    }

    @CryptoTraceScope
    @Provides
    HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {

            @Override
            public void log(String message) {
                Timber.d(message);
            }
        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return httpLoggingInterceptor;
    }

    @CryptoTraceScope
    @Provides
    OkHttpClient okHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();
    }
}
