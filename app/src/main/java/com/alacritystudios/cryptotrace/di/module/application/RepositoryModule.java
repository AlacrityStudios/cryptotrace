package com.alacritystudios.cryptotrace.di.module.application;

import com.alacritystudios.cryptotrace.dao.AlertDao;
import com.alacritystudios.cryptotrace.dao.AlertDetailsDao;
import com.alacritystudios.cryptotrace.dao.CryptoCurrencyDao;
import com.alacritystudios.cryptotrace.dao.ExchangeDao;
import com.alacritystudios.cryptotrace.dao.FavoriteCryptoCurrencyDao;
import com.alacritystudios.cryptotrace.dao.FiatCurrencyDao;
import com.alacritystudios.cryptotrace.repository.AlertRepository;
import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.repository.ExchangeRepository;
import com.alacritystudios.cryptotrace.repository.FiatCurrencyRepository;
import com.alacritystudios.cryptotrace.repository.NewsRepository;
import com.alacritystudios.cryptotrace.rest.CryptoCompareApiService;
import com.alacritystudios.cryptotrace.rest.CryptoCompareMinApiService;
import com.alacritystudios.cryptotrace.util.AppExecutor;
import com.alacritystudios.cryptotrace.util.CacheState;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * @author Anuj Dutt
 *         Module to provide Repository instances.
 */

@Module
public class RepositoryModule {

    @Provides
    CryptoCurrencyRepository cryptoCurrencyRepository(AppExecutor appExecutor, CryptoCurrencyDao cryptoCurrencyDao,
                                                      FavoriteCryptoCurrencyDao favoriteCryptoCurrencyDao, @Named("LiveData") CryptoCompareMinApiService cryptoCompareMinApiService,
                                                      CryptoCompareApiService cryptoCompareApiService, CacheState cacheState) {
        return new CryptoCurrencyRepository(appExecutor, cryptoCurrencyDao, favoriteCryptoCurrencyDao, cryptoCompareMinApiService, cryptoCompareApiService, cacheState);
    }

    @Provides
    ExchangeRepository exchangeRepository(AppExecutor appExecutor, ExchangeDao exchangeDao, CryptoCompareApiService cryptoCompareApiService, CacheState cacheState) {
        return new ExchangeRepository(exchangeDao, cryptoCompareApiService, appExecutor, cacheState);
    }

    @Provides
    NewsRepository newsRepository(AppExecutor appExecutor, @Named("Default") CryptoCompareMinApiService cryptoCompareMinApiService) {
        return new NewsRepository(cryptoCompareMinApiService, appExecutor);
    }

    @Provides
    FiatCurrencyRepository fiatCurrencyRepository(AppExecutor appExecutor, FiatCurrencyDao fiatCurrencyDao) {
        return new FiatCurrencyRepository(appExecutor, fiatCurrencyDao);
    }

    @Provides
    AlertRepository alertRepository(AppExecutor appExecutor, AlertDetailsDao alertDetailsDao, AlertDao alertDao) {
        return new AlertRepository(alertDao, alertDetailsDao, appExecutor);
    }
}
