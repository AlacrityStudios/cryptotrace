package com.alacritystudios.cryptotrace.application;

import android.app.Activity;
import android.app.Application;

import com.alacritystudios.cryptotrace.di.component.ApplicationComponent;
import com.alacritystudios.cryptotrace.di.component.DaggerApplicationComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import timber.log.Timber;

/**
 * @author Anuj Dutt
 *         Main application class. This class is declared in the manifest.
 */

public class CryptoTraceApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityAndroidInjector;

    ApplicationComponent applicationComponent;

    static CryptoTraceApplication cryptoTraceApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        cryptoTraceApplication = this;
        Timber.plant(new Timber.DebugTree());
        applicationComponent =
                DaggerApplicationComponent.builder().cryptoTraceApplication(this).build();
        applicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public static CryptoTraceApplication getInstance() {
        return cryptoTraceApplication;
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityAndroidInjector;
    }
}
