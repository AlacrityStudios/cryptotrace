package com.alacritystudios.cryptotrace.viewobject;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * @author Anuj Dutt
 *         Model to receive from CryptoCompare Min Api while fetching cyrptoCurrency list.
 */

public class CryptoCurrencyListWrapperModel {

    @SerializedName("Response")
    public String response;

    @SerializedName("Message")
    public String message;

    @SerializedName("BaseImageUrl")
    public String baseImageUrl;

    @SerializedName("BaseLinkUrl")
    public String baseLinkUrl;

    @SerializedName("Data")
    public HashMap<String, CryptoCurrency> data;
}
