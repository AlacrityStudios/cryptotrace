package com.alacritystudios.cryptotrace.viewobject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * @author Anuj Dutt
 *         Stores data of price history of currencies.
 */

public class CryptoCurrencyFormattedStats {

    public String time;

    public String close;

    public String high;

    public String low;

    public String open;

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (this == other) {
            return true;
        }
        if (this.getClass() != other.getClass()) {
            return false;
        }
        CryptoCurrencyFormattedStats rhs = (CryptoCurrencyFormattedStats) other;
        return Objects.equals(time, rhs.time)
                && Objects.equals(close, rhs.close)
                && Objects.equals(high, rhs.high)
                && Objects.equals(low, rhs.low)
                && Objects.equals(open, rhs.open);
    }
}
