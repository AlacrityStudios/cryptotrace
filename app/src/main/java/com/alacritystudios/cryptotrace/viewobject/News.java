package com.alacritystudios.cryptotrace.viewobject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * @author Anuj Dutt
 *         Model for news list to be shown
 */

public class News {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("guid")
    @Expose
    public String guid;
    @SerializedName("published_on")
    @Expose
    public Long publishedOn;
    @SerializedName("imageurl")
    @Expose
    public String imageurl;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("body")
    @Expose
    public String body;
    @SerializedName("tags")
    @Expose
    public String tags;
    @SerializedName("lang")
    @Expose
    public String lang;
    @SerializedName("source_info")
    @Expose
    public SourceInfo sourceInfo;

    public static class SourceInfo {

        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("lang")
        @Expose
        public String lang;
        @SerializedName("img")
        @Expose
        public String img;

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj == this) {
                return true;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            SourceInfo rhs = (SourceInfo) obj;
            return Objects.equals(name, rhs.name)
                    && Objects.equals(lang, rhs.lang)
                    && Objects.equals(img, rhs.img);
        }
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        News rhs = (News) other;
        return Objects.equals(id, rhs.id)
                && Objects.equals(guid, rhs.guid)
                && Objects.equals(publishedOn, rhs.publishedOn)
                && Objects.equals(imageurl, rhs.imageurl)
                && Objects.equals(title, rhs.title)
                && Objects.equals(url, rhs.url)
                && Objects.equals(source, rhs.source)
                && Objects.equals(body, rhs.body)
                && Objects.equals(id, rhs.id)
                && Objects.equals(tags, rhs.tags)
                && Objects.equals(lang, rhs.lang)
                && Objects.equals(sourceInfo, rhs.sourceInfo);
    }
}
