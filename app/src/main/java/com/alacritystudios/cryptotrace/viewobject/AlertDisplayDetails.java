package com.alacritystudios.cryptotrace.viewobject;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Relation;

import java.util.Objects;

/**
 * @author Anuj Dutt
 *  A relation to hold all details of an alert that is saved.
 */

public class AlertDisplayDetails {

    public Long id;

    @ColumnInfo(name = "crypto_currency_id")
    public String cryptoCurrencyId;

    @ColumnInfo(name = "fiat_currency_id")
    public Long fiatCurrencyId;

    @ColumnInfo(name = "exchange_id")
    public String exchangeId;

    @ColumnInfo(name = "fetched_price")
    public Double price;

    @ColumnInfo(name = "alert_price")
    public Double alertPrice;

    public String cryptoCurrencyImageUrl;

    public String fiatCurrencyImageUrl;

    public String exchangeImageUrl;

    public String cryptoCurrencyDisplayName;

    public String fiatCurrencyDisplayName;

    public String exchangeDisplayName;

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        AlertDisplayDetails rhs = (AlertDisplayDetails) other;
        return Objects.equals(id, rhs.id)
                && Objects.equals(cryptoCurrencyId, rhs.cryptoCurrencyId)
                && Objects.equals(fiatCurrencyId, rhs.fiatCurrencyId)
                && Objects.equals(exchangeId, rhs.exchangeId)
                && Objects.equals(price, rhs.price)
                && Objects.equals(alertPrice, rhs.alertPrice)
                && Objects.equals(cryptoCurrencyImageUrl, rhs.cryptoCurrencyImageUrl)
                && Objects.equals(fiatCurrencyImageUrl, rhs.fiatCurrencyImageUrl)
                && Objects.equals(exchangeImageUrl, rhs.exchangeImageUrl)
                && Objects.equals(cryptoCurrencyDisplayName, rhs.cryptoCurrencyDisplayName)
                && Objects.equals(fiatCurrencyDisplayName, rhs.fiatCurrencyDisplayName)
                && Objects.equals(exchangeDisplayName, rhs.exchangeDisplayName);
    }
}