package com.alacritystudios.cryptotrace.viewobject;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

/**
 * @author Anuj Dutt
 *         An entity to store favorite cypto-currencies.
 */

@Entity(tableName = "favorite_cryptocurrency", foreignKeys = {@ForeignKey(entity = CryptoCurrency.class,
        parentColumns = {"id"},
        childColumns = {"crypto_currency_id"})})
public class FavoriteCryptoCurrency {

    @PrimaryKey(autoGenerate = true)
    public Long id;

    @ColumnInfo(name = "crypto_currency_id")
    public String cryptoCurrencyId;
}