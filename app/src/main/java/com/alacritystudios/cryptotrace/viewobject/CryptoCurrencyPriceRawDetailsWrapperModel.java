package com.alacritystudios.cryptotrace.viewobject;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author Anuj Dutt
 *         A wrapper model containing crypto-currency details returned by the API.
 */

public class CryptoCurrencyPriceRawDetailsWrapperModel {

    @SerializedName("RAW")
    public LinkedHashMap<String, HashMap<String, CryptoCurrencyPriceCompleteDetailsWrapperModel.CurrencyDetailsRawModel>> rawCryptoCurrencyDetails;
}
