package com.alacritystudios.cryptotrace.viewobject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

/**
 * @author Anuj Dutt
 * Wrapper model to store currency historical price data.
 */

public class CryptoCurrencyHistogramWrapperModel {

    @SerializedName("Response")
    @Expose
    public String response;
    @SerializedName("Type")
    @Expose
    public Integer type;
    @SerializedName("Aggregated")
    @Expose
    public Boolean aggregated;
    @SerializedName("Data")
    @Expose
    public List<CryptoCurrencyStats> data = null;
    @SerializedName("TimeTo")
    @Expose
    public Integer timeTo;
    @SerializedName("TimeFrom")
    @Expose
    public Integer timeFrom;
    @SerializedName("FirstValueInArray")
    @Expose
    public Boolean firstValueInArray;

    public static class CryptoCurrencyStats {

        @SerializedName("time")
        @Expose
        public Integer time;
        @SerializedName("close")
        @Expose
        public Double close;
        @SerializedName("high")
        @Expose
        public Double high;
        @SerializedName("low")
        @Expose
        public Double low;
        @SerializedName("open")
        @Expose
        public Double open;

        @Override
        public boolean equals(Object other) {
            if (other == null) {
                return false;
            }
            if (this == other) {
                return true;
            }
            if (this.getClass() != other.getClass()) {
                return false;
            }
            com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyFormattedStats rhs = (com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyFormattedStats) other;
            return Objects.equals(time, rhs.time)
                    && Objects.equals(close, rhs.close)
                    && Objects.equals(high, rhs.high)
                    && Objects.equals(low, rhs.low)
                    && Objects.equals(open, rhs.open);
        }
    }
}
