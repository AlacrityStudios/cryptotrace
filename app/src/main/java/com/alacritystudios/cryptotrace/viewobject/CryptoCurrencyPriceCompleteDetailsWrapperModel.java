package com.alacritystudios.cryptotrace.viewobject;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author Anuj Dutt
 *         A wrapper model containing crypto-currency details returned by the API.
 */

public class CryptoCurrencyPriceCompleteDetailsWrapperModel {


    @SerializedName("RAW")
    public LinkedHashMap<String, HashMap<String, CurrencyDetailsRawModel>> rawCryptoCurrencyDetails;

    @SerializedName("DISPLAY")
    public LinkedHashMap<String, HashMap<String, CurrencyDetailsDisplayModel>> displayCryptoCurrencyDetails;

//
//
//
//    public CurrencyDetailsRawModel currencyDetailsRawModel;
//
//    public CurrencyDetailsDisplayModel currencyDetailsDisplayModel;
//
//    public CryptoCurrencyPriceCompleteDetailsWrapperModel(CurrencyDetailsRawModel currencyDetailsRawModel, CurrencyDetailsDisplayModel currencyDetailsDisplayModel) {
//        this.currencyDetailsRawModel = currencyDetailsRawModel;
//        this.currencyDetailsDisplayModel = currencyDetailsDisplayModel;
//    }

    public static class CurrencyDetailsRawModel {

        @SerializedName("TYPE")
        public String type;

        @SerializedName("MARKET")
        public String market;

        @SerializedName("FROMSYMBOL")
        public String fromSymbol;

        @SerializedName("TOSYMBOL")
        public String toSymbol;

        @SerializedName("FLAGS")
        public String flags;

        @SerializedName("PRICE")
        public Double price;

        @SerializedName("LASTUPDATE")
        public Integer lastUpdate;

        @SerializedName("LASTVOLUME")
        public Double lastVolume;

        @SerializedName("LASTVOLUMETO")
        public Double lastVolumeTo;

        @SerializedName("LASTTRADEID")
        public Double lastTradeId;

        @SerializedName("VOLUME24HOUR")
        public Double volume24Hour;

        @SerializedName("VOLUME24HOURTO")
        public Double volume24HourTo;

        @SerializedName("OPEN24HOUR")
        public Double open24Hour;

        @SerializedName("HIGH24HOUR")
        public Double high24Hour;

        @SerializedName("LOW24HOUR")
        public Double low24Hour;

        @SerializedName("LASTMARKET")
        public String lastMarket;

        @SerializedName("CHANGE24HOUR")
        public Double change24Hour;

        @SerializedName("CHANGEPCT24HOUR")
        public Double changePct24Hour;

        @SerializedName("SUPPLY")
        public Double supply;

        @SerializedName("MKTCAP")
        public Double mktCap;
    }

    public static class CurrencyDetailsDisplayModel {

        @SerializedName("TYPE")
        public String type;
        @SerializedName("MARKET")
        public String market;
        @SerializedName("FROMSYMBOL")
        public String fromSymbol;
        @SerializedName("TOSYMBOL")
        public String toSymbol;
        @SerializedName("FLAGS")
        public String flags;
        @SerializedName("PRICE")
        public String price;
        @SerializedName("LASTUPDATE")
        public String lastUpdate;
        @SerializedName("LASTVOLUME")
        public String lastVolume;
        @SerializedName("LASTVOLUMETO")
        public String lastVolumeTo;
        @SerializedName("LASTTRADEID")
        public Double lastTradeId;
        @SerializedName("VOLUME24HOUR")
        public String volume24Hour;
        @SerializedName("VOLUME24HOURTO")
        public String volume24HourTo;
        @SerializedName("OPEN24HOUR")
        public String open24Hour;
        @SerializedName("HIGH24HOUR")
        public String high24Hour;
        @SerializedName("LOW24HOUR")
        public String low24Hour;
        @SerializedName("LASTMARKET")
        public String lastMarket;
        @SerializedName("CHANGE24HOUR")
        public String change24Hour;
        @SerializedName("CHANGEPCT24HOUR")
        public String changePct24Hour;
        @SerializedName("SUPPLY")
        public String supply;
        @SerializedName("MKTCAP")
        public String mktCap;
    }
}

