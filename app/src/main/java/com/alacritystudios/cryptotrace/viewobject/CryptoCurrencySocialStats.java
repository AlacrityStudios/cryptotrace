package com.alacritystudios.cryptotrace.viewobject;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Anuj Dutt
 */

public class CryptoCurrencySocialStats {

    @SerializedName("Response")
    public String response;
    @SerializedName("Message")
    public String message;
    @SerializedName("Type")
    public Long type;
    @SerializedName("Data")
    public Data data;

    public static class Data {

        @SerializedName("CryptoCompare")
        public CryptoCompare cryptoCompare;
        @SerializedName("Twitter")
        public Twitter twitter;
        @SerializedName("Reddit")
        public Reddit reddit;
        @SerializedName("Facebook")
        public Facebook facebook;
    }

    public static class Reddit {

        @SerializedName("comments_per_hour")
        public String commentsPerHour;

        @SerializedName("comments_per_day")
        public Double commentsPerDay;

        @SerializedName("link")
        public String link;

        @SerializedName("posts_per_day")
        public String postsPerDay;

        @SerializedName("active_users")
        public Long activeUsers;

        @SerializedName("subscribers")
        public Long subscribers;

        @SerializedName("posts_per_hour")
        public String postsPerHour;

        @SerializedName("community_creation")
        public String communityCreation;

        @SerializedName("name")
        public String name;

        @SerializedName("Points")
        public Long points;
    }

    public static class Twitter {

        @SerializedName("favourites")
        public String favourites;

        @SerializedName("statuses")
        public Long statuses;

        @SerializedName("following")
        public String following;

        @SerializedName("account_creation")
        public String accountCreation;

        @SerializedName("name")
        public String name;

        @SerializedName("link")
        public String link;

        @SerializedName("lists")
        public Long lists;

        @SerializedName("followers")
        public Long followers;

        @SerializedName("Points")
        public Long points;
    }

    public static class Facebook {

        @SerializedName("likes")
        public Long likes;
        @SerializedName("name")
        public String name;
        @SerializedName("link")
        public String link;
        @SerializedName("talking_about")
        public Long talkingAbout;
        @SerializedName("is_closed")
        public String isClosed;
        @SerializedName("Points")
        public Long points;
    }

    public static class SimilarItem {

        @SerializedName("Id")
        public Long id;
        @SerializedName("Name")
        public String name;
        @SerializedName("FullName")
        public String fullName;
        @SerializedName("ImageUrl")
        public String imageUrl;
        @SerializedName("Url")
        public String url;
    }

    public static class CryptoCompare {

        @SerializedName("SimilarItems")
        public List<SimilarItem> similarItems = null;
        @SerializedName("Followers")
        public Long followers;
        @SerializedName("Posts")
        public String posts;
        @SerializedName("Comments")
        public String comments;
        @SerializedName("Points")
        public Long points;
        @SerializedName("PageViews")
        public Long pageViews;
    }
}
