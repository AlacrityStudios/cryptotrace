package com.alacritystudios.cryptotrace.viewobject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * @author Anuj Dutt
 *         Model for exchange
 */

@Entity(tableName = "exchange")
public class Exchange {

    @PrimaryKey
    @NonNull
    @SerializedName("Id")
    public String id;

    @SerializedName("Name")
    public String name;

    @SerializedName("LogoUrl")
    public String logoUrl;

    @SerializedName("InternalName")
    public String internalName;

    @SerializedName("Country")
    public String country;

    @SerializedName("Trades")
    public Boolean trades;

    public Exchange(@NonNull String id, String name, String logoUrl, String internalName, String country, Boolean trades) {
        this.id = id;
        this.name = name;
        this.logoUrl = logoUrl;
        this.internalName = internalName;
        this.country = country;
        this.trades = trades;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        Exchange rhs = (Exchange) other;
        return Objects.equals(id, rhs.id)
                && Objects.equals(name, rhs.name)
                && Objects.equals(logoUrl, rhs.logoUrl)
                && Objects.equals(internalName, rhs.internalName)
                && Objects.equals(country, rhs.country)
                && Objects.equals(trades, rhs.trades);
    }
}
