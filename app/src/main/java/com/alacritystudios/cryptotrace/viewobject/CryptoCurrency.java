package com.alacritystudios.cryptotrace.viewobject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;


/**
 * @author Anuj Dutt
 *         Model for cyrpto-currency
 */

@Entity(tableName = "cryptocurrency")
public class CryptoCurrency {

    @PrimaryKey
    @SerializedName("Id")
    @NonNull
    public String id;

    @SerializedName("ImageUrl")
    public String imageUrl;

    @SerializedName("Symbol")
    public String symbol;

    @SerializedName("CoinName")
    public String coinName;

    @SerializedName("Algorithm")
    public String algorithm;

    @SerializedName("ProofType")
    public String proofType;

    @SerializedName("SortOrder")
    public String sortOrder;

    public Boolean isFavorite;

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        CryptoCurrency rhs = (CryptoCurrency) other;
        return Objects.equals(coinName, rhs.coinName)
                && Objects.equals(symbol, rhs.symbol)
                && Objects.equals(imageUrl, rhs.imageUrl)
                && Objects.equals(sortOrder, rhs.sortOrder)
                && Objects.equals(algorithm, rhs.algorithm)
                && Objects.equals(id, rhs.id)
                && Objects.equals(isFavorite, rhs.isFavorite)
                && Objects.equals(proofType, rhs.proofType);
    }
}
