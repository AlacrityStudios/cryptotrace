package com.alacritystudios.cryptotrace.viewobject;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Objects;

/**
 * @author Anuj Dutt
 *         A model for fiat currency
 */

@Entity(tableName = "fiatcurrency")
public class FiatCurrency {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public Long id;

    public String name;

    public String coinName;

    public String fullName;

    public String symbol;

    public String imageUrl;

    public FiatCurrency(String name, String coinName, String fullName, String symbol, String imageUrl) {
        this.name = name;
        this.coinName = coinName;
        this.fullName = fullName;
        this.symbol = symbol;
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        FiatCurrency rhs = (FiatCurrency) other;
        return Objects.equals(coinName, rhs.coinName)
                && Objects.equals(imageUrl, rhs.imageUrl)
                && Objects.equals(id, rhs.id)
                && Objects.equals(name, rhs.name)
                && Objects.equals(symbol, rhs.symbol)
                && Objects.equals(fullName, rhs.fullName);
    }
}
