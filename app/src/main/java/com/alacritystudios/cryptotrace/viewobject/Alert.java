package com.alacritystudios.cryptotrace.viewobject;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * @author Anuj Dutt
 *         An entity class to hold Alerts set by the user.
 */



@Entity(tableName = "alert", foreignKeys = {
        @ForeignKey(entity = CryptoCurrency.class, parentColumns = {"id"}, childColumns = {"crypto_currency_id"}),
        @ForeignKey(entity = FiatCurrency.class, parentColumns = {"id"}, childColumns = {"fiat_currency_id"}),
        @ForeignKey(entity = Exchange.class, parentColumns = {"id"}, childColumns = {"exchange_id"})
        })
public class Alert {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public Long id;

    @ColumnInfo(name = "crypto_currency_id")
    public String cryptoCurrencyId;

    @ColumnInfo(name = "fiat_currency_id")
    public Long fiatCurrencyId;

    @ColumnInfo(name = "exchange_id")
    public String exchangeId;

    @ColumnInfo(name = "fetched_price")
    public Double price;

    @ColumnInfo(name = "alert_price")
    public Double alertPrice;

    public Alert(String cryptoCurrencyId, Long fiatCurrencyId, String exchangeId, Double price, Double alertPrice) {
        this.cryptoCurrencyId = cryptoCurrencyId;
        this.fiatCurrencyId = fiatCurrencyId;
        this.exchangeId = exchangeId;
        this.price = price;
        this.alertPrice = alertPrice;
    }
}
