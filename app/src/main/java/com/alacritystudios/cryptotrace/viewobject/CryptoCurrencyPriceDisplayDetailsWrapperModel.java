package com.alacritystudios.cryptotrace.viewobject;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author Anuj Dutt
 *         A wrapper model containing crypto-currency details returned by the API.
 */

public class CryptoCurrencyPriceDisplayDetailsWrapperModel {

    @SerializedName("DISPLAY")
    public LinkedHashMap<String, HashMap<String, CryptoCurrencyPriceCompleteDetailsWrapperModel.CurrencyDetailsDisplayModel>> displayCryptoCurrencyDetails;
}
