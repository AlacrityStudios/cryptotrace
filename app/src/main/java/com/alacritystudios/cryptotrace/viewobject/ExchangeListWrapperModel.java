package com.alacritystudios.cryptotrace.viewobject;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Anuj Dutt
 *         Model to receive from CryptoCompare Min Api while fetching exchange list.
 */

public class ExchangeListWrapperModel {

    @SerializedName("Response")
    public String response;

    @SerializedName("Message")
    public String message;

    @SerializedName("Data")
    public HashMap<String, Exchange> data;
}
