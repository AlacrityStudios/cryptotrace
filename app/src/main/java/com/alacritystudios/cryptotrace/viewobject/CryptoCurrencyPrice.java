package com.alacritystudios.cryptotrace.viewobject;

/**
 * @author Anuj Dutt
 *         A view object for storing cryptocurrency pricing.
 */

public class CryptoCurrencyPrice {

    public String fromCurrencyId;

    public String fromCurrencyName;

    public String fromCurrencySymbol;

    public String toCurrencySymbol;

    public String fromCurrencySymbolImageUrl;

    public String price;

    public String change;

    public String changePercent;
}
