package com.alacritystudios.cryptotrace.ui.searchcrypto.search;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.CryptoCurrencyListAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentSearchCryptoCurrencyBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.CryptoTraceViewModelFactory;
import com.alacritystudios.cryptotrace.viewmodel.SearchCryptoCurrencyViewModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * @author Anuj Dutt
 *  Fragment to search for crypto-currencies.
 */
public class SearchCryptoCurrencyFragment extends Fragment {

    @Inject
    CryptoTraceViewModelFactory cryptoTraceViewModelFactory;

    SearchCryptoCurrencyViewModel searchCryptoCurrencyViewModel;

    FragmentBindingComponent fragmentBindingComponent;

    FragmentSearchCryptoCurrencyBinding fragmentSearchCryptoCurrencyBinding;

    AutoClearedValue<CryptoCurrencyListAdapter> cryptoCurrencyListAdapterAutoClearedValue;

    public SearchCryptoCurrencyFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent();
        fragmentSearchCryptoCurrencyBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_crypto_currency, container, false, fragmentBindingComponent);
        setupRecyclerView();
        setupToolbar();
        return fragmentSearchCryptoCurrencyBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupViewModel();
    }


    private void setupViewModel() {
        searchCryptoCurrencyViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(SearchCryptoCurrencyViewModel.class);
        searchCryptoCurrencyViewModel.getListLiveData().observe(this, new Observer<Resource<List<CryptoCurrency>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<CryptoCurrency>> listResource) {
                if (cryptoCurrencyListAdapterAutoClearedValue.get() != null && listResource != null) {
                    switch (listResource.status) {
                        case LOADING:
                            cryptoCurrencyListAdapterAutoClearedValue.get().setCryptoCurrencyList(listResource.data);
                            break;
                        case SUCCESS:
                            cryptoCurrencyListAdapterAutoClearedValue.get().setCryptoCurrencyList(listResource.data);
                            break;
                        case ERROR:
                            break;
                    }
                }
            }
        });

        fragmentSearchCryptoCurrencyBinding.svSearchCc.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                searchCryptoCurrencyViewModel.getQuery().setValue(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchCryptoCurrencyViewModel.getQuery().setValue(newText);
                return true;
            }
        });
    }

    private void setupRecyclerView() {
        cryptoCurrencyListAdapterAutoClearedValue = new AutoClearedValue<>(new CryptoCurrencyListAdapter(fragmentBindingComponent, new CryptoCurrencyListAdapter.CryptoCurrencyClickCallback() {

            @Override
            public void onItemClick(String id, ImageView sharedImageView, TextView sharedTextView) {

            }

            @Override
            public void onFavoritesClick(CryptoCurrency cryptoCurrency, boolean shouldAdd) {

            }
        }), this);
        fragmentSearchCryptoCurrencyBinding.rvCryptoCurrencyList.setHasFixedSize(true);
        fragmentSearchCryptoCurrencyBinding.rvCryptoCurrencyList.setAdapter(cryptoCurrencyListAdapterAutoClearedValue.get());
    }

    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(fragmentSearchCryptoCurrencyBinding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
