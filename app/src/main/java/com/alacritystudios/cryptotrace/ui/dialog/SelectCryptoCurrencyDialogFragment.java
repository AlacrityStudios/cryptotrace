package com.alacritystudios.cryptotrace.ui.dialog;


import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.CryptoCurrencyListSelectionAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.DialogFragmentSelectCryptoCurrencyBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.addnewalert.AddNewAlertActivity;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivity;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivityViewModel;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.AddNewAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectCryptoCurrencyFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * @author Anuj Dutt
 *         Fragment to display the list of crypto-currencies.
 */
public class SelectCryptoCurrencyDialogFragment extends DialogFragment {

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    SelectCryptoCurrencyFragmentViewModel selectCryptoCurrencyFragmentViewModel;

    SelectParamsViewModelListener parentViewModel;

    FragmentBindingComponent fragmentBindingComponent;

    DialogFragmentSelectCryptoCurrencyBinding dialogFragmentSelectCryptoCurrencyBinding;

    AutoClearedValue<CryptoCurrencyListSelectionAdapter> cryptoCurrencyListSelectionAdapterAutoClearedValue;

    public SelectCryptoCurrencyDialogFragment() {
        // Required empty public constructor
    }

    public static SelectCryptoCurrencyDialogFragment getInstance() {
        return new SelectCryptoCurrencyDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        dialogFragmentSelectCryptoCurrencyBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_select_crypto_currency, container, false, fragmentBindingComponent);
        setupRecyclerViews();
        return dialogFragmentSelectCryptoCurrencyBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        selectCryptoCurrencyFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(SelectCryptoCurrencyFragmentViewModel.class);
        if (getActivity() != null && getActivity() instanceof CryptoDetailsActivity) {
            parentViewModel = ViewModelProviders.of((CryptoDetailsActivity) getActivity(), cryptoTraceViewModelFactory).get(CryptoDetailsActivityViewModel.class);
        } else if (getActivity() != null && getActivity() instanceof AddNewAlertActivity) {
            parentViewModel = ViewModelProviders.of((AddNewAlertActivity) getActivity(), cryptoTraceViewModelFactory).get(AddNewAlertActivityViewModel.class);
        }
        initialiseTextWatcher();
        setupLiveDataListener();
        setupClickListeners();
        selectCryptoCurrencyFragmentViewModel.setFromCurrencyNameLiveData("");
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void setupClickListeners() {
        dialogFragmentSelectCryptoCurrencyBinding.tvSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    private void initialiseTextWatcher() {
        dialogFragmentSelectCryptoCurrencyBinding.etFromCurrencyName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectCryptoCurrencyFragmentViewModel.setFromCurrencyNameLiveData(s.toString());
            }
        });
    }

    private void setupLiveDataListener() {
        selectCryptoCurrencyFragmentViewModel.getCryptoCurrencyListLiveData().observe(this, new Observer<Resource<List<CryptoCurrency>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<CryptoCurrency>> listResource) {
                if (listResource != null && cryptoCurrencyListSelectionAdapterAutoClearedValue.get() != null) {
                    if (listResource.status == Resource.Status.LOADING) {
                        dialogFragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setRefreshing(true);
                        cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else if (listResource.status == Resource.Status.SUCCESS) {
                        dialogFragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setRefreshing(false);
                        cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else {
                        dialogFragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setRefreshing(false);
                        cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showErrorLayout();
                    }
                } else {
                    dialogFragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setRefreshing(false);
                    cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(new ArrayList<CryptoCurrency>());
                    showErrorLayout();
                }
            }
        });
    }


    private void setupRecyclerViews() {
        cryptoCurrencyListSelectionAdapterAutoClearedValue = new AutoClearedValue<>(new CryptoCurrencyListSelectionAdapter(fragmentBindingComponent, new CryptoCurrencyListSelectionAdapter.CryptoCurrencyClickCallback() {

            @Override
            public void onItemClick(CryptoCurrency cryptoCurrency) {
                parentViewModel.setFromCurrency(cryptoCurrency);
                getDialog().dismiss();
            }
        }), this);
        dialogFragmentSelectCryptoCurrencyBinding.rvFromCurrencyList.setHasFixedSize(true);
        dialogFragmentSelectCryptoCurrencyBinding.rvFromCurrencyList.setAdapter(cryptoCurrencyListSelectionAdapterAutoClearedValue.get());
        dialogFragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setEnabled(false);
    }

    private void showErrorLayout() {
        dialogFragmentSelectCryptoCurrencyBinding.llToCurrencyPlaceholder.setVisibility(View.VISIBLE);
        dialogFragmentSelectCryptoCurrencyBinding.rvFromCurrencyList.setVisibility(View.GONE);
    }

    private void showContentLayout() {
        dialogFragmentSelectCryptoCurrencyBinding.llToCurrencyPlaceholder.setVisibility(View.GONE);
        dialogFragmentSelectCryptoCurrencyBinding.rvFromCurrencyList.setVisibility(View.VISIBLE);
    }
}
