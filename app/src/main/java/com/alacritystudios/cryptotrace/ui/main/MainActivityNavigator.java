package com.alacritystudios.cryptotrace.ui.main;

import android.support.v4.app.FragmentTransaction;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.ui.main.alertslist.AlertListFragment;
import com.alacritystudios.cryptotrace.ui.main.cryptocurrencylist.CryptoCurrencyListFragment;
import com.alacritystudios.cryptotrace.ui.main.home.HomeFragment;
import com.alacritystudios.cryptotrace.ui.main.newslist.NewsListFragment;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         A navigator to navigate through all the fragments.
 */

public class MainActivityNavigator {

    MainActivity mainActivity;

    @Inject
    public MainActivityNavigator(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void navigateToHomeFragment() {
        FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, new HomeFragment(), "HomeFragment");
        fragmentTransaction.commit();
    }

    public void navigateToCryptoCurrencyListFragment() {
        FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, new CryptoCurrencyListFragment(), "CryptoCurrencyListFragment");
        fragmentTransaction.commit();
    }

    public void navigateToAlertListFragment() {
        FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, new AlertListFragment(), "AlertListFragment");
        fragmentTransaction.commit();
    }

    public void navigateToNewsListFragment() {
        FragmentTransaction fragmentTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, new NewsListFragment(), "NewsListFragment");
        fragmentTransaction.commit();
    }
}
