package com.alacritystudios.cryptotrace.ui.main.cryptocurrencylist;


import android.app.ActivityOptions;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.CryptoCurrencyListAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentCryptoCurrencyListBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivity;
import com.alacritystudios.cryptotrace.ui.searchcrypto.SearchCryptoCurrencyActivity;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyListViewModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class CryptoCurrencyListFragment extends Fragment {

    private static final String CRYPTO_CURRENCY_ID = "cryptoCurrencyId";

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    CryptoCurrencyListViewModel cryptoCurrencyListViewModel;

    FragmentCryptoCurrencyListBinding fragmentCryptoCurrencyListBinding;

    AutoClearedValue<CryptoCurrencyListAdapter> cryptoCurrencyListAdapterAutoClearedValue;

    FragmentBindingComponent fragmentBindingComponent;

    public CryptoCurrencyListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.crypto_currency_list_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_crypto_currency_search:
                Intent intent = new Intent(getActivity(), SearchCryptoCurrencyActivity.class);
                startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentCryptoCurrencyListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_crypto_currency_list, container, false, fragmentBindingComponent);
        setupToolbar();
        setupRecyclerView();
        return fragmentCryptoCurrencyListBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cryptoCurrencyListViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(CryptoCurrencyListViewModel.class);
        cryptoCurrencyListViewModel.getResults().observe(this, new Observer<Resource<List<CryptoCurrency>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<CryptoCurrency>> listResource) {
                if (listResource != null && cryptoCurrencyListAdapterAutoClearedValue.get() != null) {
                    cryptoCurrencyListAdapterAutoClearedValue.get().setCryptoCurrencyList(listResource.data);
                    if (listResource.status == Resource.Status.LOADING) {
                        if (! fragmentCryptoCurrencyListBinding.srlCryptoCurrencyList.isRefreshing()) {
                            fragmentCryptoCurrencyListBinding.srlCryptoCurrencyList.setRefreshing(true);
                        }
                        showContentLayout();
                    } else if (listResource.status == Resource.Status.SUCCESS) {
                        fragmentCryptoCurrencyListBinding.srlCryptoCurrencyList.setRefreshing(false);
                        if (listResource.data == null || listResource.data.size() == 0) {
                            showErrorLayout();
                        } else {
                            showContentLayout();
                        }
                    } else {
                        fragmentCryptoCurrencyListBinding.srlCryptoCurrencyList.setRefreshing(false);
                        if (listResource.data == null || listResource.data.size() == 0) {
                            showErrorLayout();
                        } else {
                            showContentLayout();
                        }
                    }
                } else {
                    fragmentCryptoCurrencyListBinding.srlCryptoCurrencyList.setRefreshing(false);
                    cryptoCurrencyListAdapterAutoClearedValue.get().setCryptoCurrencyList(new ArrayList<CryptoCurrency>());
                    showErrorLayout();
                }
            }
        });
        fragmentCryptoCurrencyListBinding.srlCryptoCurrencyList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                cryptoCurrencyListViewModel.incrementReloadCount();
            }
        });
    }

    private void setupRecyclerView() {
        cryptoCurrencyListAdapterAutoClearedValue = new AutoClearedValue<>(new CryptoCurrencyListAdapter(fragmentBindingComponent, new CryptoCurrencyListAdapter.CryptoCurrencyClickCallback() {

            @Override
            public void onItemClick(String id, ImageView sharedImageView, TextView sharedTextView) {
                Intent intent = new Intent(getActivity(), CryptoDetailsActivity.class);
                intent.putExtra(CRYPTO_CURRENCY_ID, id);
                Pair<View, String> pair1 = Pair.create((View) sharedImageView, ViewCompat.getTransitionName(sharedImageView));
                Pair<View, String> pair2 = Pair.create((View) sharedTextView, ViewCompat.getTransitionName(sharedTextView));
                ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(getActivity(), pair1, pair2);
                startActivity(intent, activityOptions.toBundle());
            }

            @Override
            public void onFavoritesClick(CryptoCurrency cryptoCurrency, boolean shouldAdd) {
                cryptoCurrencyListViewModel.invertCryptoCurrencyStatusInFavorites(cryptoCurrency, shouldAdd);
            }

        }), this);
        fragmentCryptoCurrencyListBinding.rvCryptoCurrencyList.setHasFixedSize(true);
        fragmentCryptoCurrencyListBinding.rvCryptoCurrencyList.setAdapter(cryptoCurrencyListAdapterAutoClearedValue.get());
    }

    private void showErrorLayout() {
        fragmentCryptoCurrencyListBinding.tvError.setVisibility(View.VISIBLE);
        fragmentCryptoCurrencyListBinding.rvCryptoCurrencyList.setVisibility(View.GONE);
    }

    private void showContentLayout() {
        fragmentCryptoCurrencyListBinding.tvError.setVisibility(View.GONE);
        fragmentCryptoCurrencyListBinding.rvCryptoCurrencyList.setVisibility(View.VISIBLE);
    }

    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(fragmentCryptoCurrencyListBinding.toolbar);
    }
}
