package com.alacritystudios.cryptotrace.ui.main.newslist;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.NewsListAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentNewsListBinding;
import com.alacritystudios.cryptotrace.resource.Listing;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.CryptoTraceViewModelFactory;
import com.alacritystudios.cryptotrace.viewmodel.NewsListFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.News;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsListFragment extends Fragment {

    @Inject
    CryptoTraceViewModelFactory cryptoTraceViewModelFactory;

    FragmentNewsListBinding fragmentNewsListBinding;

    NewsListFragmentViewModel newsListFragmentViewModel;

    AutoClearedValue<NewsListAdapter> newsListAdapterAutoClearedValue;

    FragmentBindingComponent fragmentBindingComponent;

    public NewsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentNewsListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_list, container, false, fragmentBindingComponent);
        setupToolbar();
        setupRecyclerView();
        return fragmentNewsListBinding.getRoot();
    }

    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(fragmentNewsListBinding.toolbar);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        newsListFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(NewsListFragmentViewModel.class);
        fragmentNewsListBinding.srlNewsList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                newsListFragmentViewModel.refreshNewsData();
            }
        });
        newsListFragmentViewModel.getResults().pagedListLiveData.observe(this, new Observer<PagedList<News>>() {

            @Override
            public void onChanged(@Nullable PagedList<News> news) {
                newsListAdapterAutoClearedValue.get().setList(news);
            }
        });

        newsListFragmentViewModel.getResults().networkStateLiveData.observe(this, new Observer<Listing.NetworkState>() {

            @Override
            public void onChanged(Listing.NetworkState networkState) {
                fragmentNewsListBinding.srlNewsList.setRefreshing(false);
                PagedList<News> news = newsListFragmentViewModel.getResults().pagedListLiveData.getValue();
                newsListAdapterAutoClearedValue.get().setList(news);
                if (networkState.status.equals(Listing.NetworkState.Status.FAILED)) {
                    Snackbar.make(fragmentNewsListBinding.getRoot(), networkState.error, 2000).show();
                    showErrorLayout();
                } else if (networkState.status.equals(Listing.NetworkState.Status.RUNNING)) {
                    showContentLayout();
                } else {
                    showContentLayout();
                }
            }
        });
    }

    private void setupRecyclerView() {
        newsListAdapterAutoClearedValue = new AutoClearedValue<>(new NewsListAdapter(new NewsListAdapter.NewsListDiffCallback(), fragmentBindingComponent, new NewsListAdapter.NewsClickCallback() {

            @Override
            public void onItemClick(News news) {

            }
        }), this);
        fragmentNewsListBinding.rvNewsList.setHasFixedSize(true);
        fragmentNewsListBinding.rvNewsList.setAdapter(newsListAdapterAutoClearedValue.get());
    }

    private void showErrorLayout() {
        fragmentNewsListBinding.llError.setVisibility(View.VISIBLE);
        fragmentNewsListBinding.llContent.setVisibility(View.GONE);
    }

    private void showContentLayout() {
        fragmentNewsListBinding.llError.setVisibility(View.GONE);
        fragmentNewsListBinding.llContent.setVisibility(View.VISIBLE);
    }
}
