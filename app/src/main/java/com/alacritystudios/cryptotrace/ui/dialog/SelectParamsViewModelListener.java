package com.alacritystudios.cryptotrace.ui.dialog;

import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

/**
 * @author Anuj Dutt
 *         A mandatory interface to be implemented by view models in case they allow for
 *         altering param values through dialog fragments.
 */

public interface SelectParamsViewModelListener {

    void setFromCurrency(CryptoCurrency cryptoCurrency);

    void setToCurrency(FiatCurrency fiatCurrency);

    void setExchange(Exchange exchange);
}
