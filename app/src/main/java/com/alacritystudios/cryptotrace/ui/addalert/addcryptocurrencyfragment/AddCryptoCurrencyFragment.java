package com.alacritystudios.cryptotrace.ui.addalert.addcryptocurrencyfragment;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.CryptoCurrencyListSelectionAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentAddAlertBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddCryptoCurrencyFragment extends Fragment {

    private static final String FROM_CURRENCY_SYMBOL = "FROM_CURRENCY_SYMBOL";
    private static final String TO_CURRENCY_SYMBOL = "TO_CURRENCY_SYMBOL";
    private static final String EXCHANGE_NAME = "EXCHANGE_NAME";

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    AddAlertFragmentViewModel addAlertFragmentViewModel;

    AutoClearedValue<CryptoCurrencyListSelectionAdapter> cryptoCurrencyListSelectionAdapterAutoClearedValue;

    FragmentAddAlertBinding fragmentAddAlertBinding;
    FragmentBindingComponent fragmentBindingComponent;

    public AddCryptoCurrencyFragment() {
        // Required empty public constructor
    }

    public static AddCryptoCurrencyFragment getInstance(String fromCurrencySymbol, String toCurrencySymbol, String exchangeName) {
        AddCryptoCurrencyFragment addCryptoCurrencyFragment = new AddCryptoCurrencyFragment();
        Bundle bundle = new Bundle();
        bundle.putString(FROM_CURRENCY_SYMBOL, fromCurrencySymbol);
        bundle.putString(TO_CURRENCY_SYMBOL, toCurrencySymbol);
        bundle.putString(EXCHANGE_NAME, exchangeName);
        addCryptoCurrencyFragment.setArguments(bundle);
        return addCryptoCurrencyFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //setHasOptionsMenu(true);
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentAddAlertBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_alert, container, false, fragmentBindingComponent);
        setupRecyclerViews();
        setupEditTextRelationships();
        return fragmentAddAlertBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        addAlertFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(AddAlertFragmentViewModel.class);
        initialiseTextWatchers();
        setupLiveDataListeners();
        addAlertFragmentViewModel.getParams().observe(this, new Observer<AddAlertFragmentViewModel.AddAlertFragmentQueryParams>() {

            @Override
            public void onChanged(@Nullable AddAlertFragmentViewModel.AddAlertFragmentQueryParams addAlertFragmentQueryParams) {
                if (addAlertFragmentQueryParams != null && addAlertFragmentQueryParams.getQueryParamsStatus() == AddAlertFragmentViewModel.AddAlertFragmentQueryParams.AddAlertFragmentQueryParamsStatus.READY) {
                    fragmentAddAlertBinding.setCryptoCurrency(addAlertFragmentQueryParams.cryptoCurrency);
                    fragmentAddAlertBinding.setFiatCurrency(addAlertFragmentQueryParams.fiatCurrency);
                    fragmentAddAlertBinding.setExchange(addAlertFragmentQueryParams.exchange);
                } else if (addAlertFragmentQueryParams != null && addAlertFragmentQueryParams.cryptoCurrency != null) {
                    fragmentAddAlertBinding.etFromCurrencyName.setText(addAlertFragmentQueryParams.cryptoCurrency.coinName);
                }
            }
        });

    }

    private void initialiseTextWatchers() {
        fragmentAddAlertBinding.etFromCurrencyName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (fragmentAddAlertBinding.etFromCurrencyName.hasFocus())
                    addAlertFragmentViewModel.setFromCurrencyNameLiveData(s.toString());
            }
        });
        fragmentAddAlertBinding.etToCurrencyName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addAlertFragmentViewModel.setToCurrencyNameLiveData(s.toString());
            }
        });
        fragmentAddAlertBinding.etExchangeName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addAlertFragmentViewModel.setExchangeNameLiveData(s.toString());
            }
        });
    }

    private void setupLiveDataListeners() {
        addAlertFragmentViewModel.getCryptoCurrencyListLiveData().observe(this, new Observer<Resource<List<CryptoCurrency>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<CryptoCurrency>> listResource) {
                if (fragmentAddAlertBinding.etFromCurrencyName.hasFocus()) {
                    fragmentAddAlertBinding.llFromCurrency.setVisibility(View.VISIBLE);
                } else {
                    fragmentAddAlertBinding.llFromCurrency.setVisibility(View.GONE);
                }
                if (listResource != null && cryptoCurrencyListSelectionAdapterAutoClearedValue.get() != null) {

                    if (listResource.status == Resource.Status.LOADING) {
                        cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);

                    } else if (listResource.status == Resource.Status.SUCCESS) {
                        cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);

                    } else {
                        cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                    }
                } else {
                    cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(new ArrayList<CryptoCurrency>());
                }
            }
        });
    }

    private void setupRecyclerViews() {
        cryptoCurrencyListSelectionAdapterAutoClearedValue = new AutoClearedValue<>(new CryptoCurrencyListSelectionAdapter(fragmentBindingComponent, new CryptoCurrencyListSelectionAdapter.CryptoCurrencyClickCallback() {

            @Override
            public void onItemClick(CryptoCurrency cryptoCurrency) {
                addAlertFragmentViewModel.setFromCurrency(cryptoCurrency);
                fragmentAddAlertBinding.etToCurrencyName.requestFocus();
                fragmentAddAlertBinding.etFromCurrencyName.setText(cryptoCurrency.coinName);
            }
        }), this);
        fragmentAddAlertBinding.rvFromCurrencyList.setAdapter(cryptoCurrencyListSelectionAdapterAutoClearedValue.get());
    }

    private void setupEditTextRelationships() {
        fragmentAddAlertBinding.etFromCurrencyName.requestFocus();
        fragmentAddAlertBinding.llToCurrency.setVisibility(View.GONE);
        fragmentAddAlertBinding.llExchange.setVisibility(View.GONE);
        fragmentAddAlertBinding.llSummary.setVisibility(View.GONE);
        fragmentAddAlertBinding.etExchangeName.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    fragmentAddAlertBinding.llExchange.setVisibility(View.GONE);
                    fragmentAddAlertBinding.llToCurrency.setVisibility(View.GONE);
                    fragmentAddAlertBinding.llFromCurrency.setVisibility(View.GONE);
                }
            }
        });
        fragmentAddAlertBinding.etToCurrencyName.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    fragmentAddAlertBinding.llExchange.setVisibility(View.GONE);
                    fragmentAddAlertBinding.llToCurrency.setVisibility(View.GONE);
                    fragmentAddAlertBinding.llFromCurrency.setVisibility(View.GONE);
                }
            }
        });
        fragmentAddAlertBinding.etFromCurrencyName.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    fragmentAddAlertBinding.llExchange.setVisibility(View.GONE);
                    fragmentAddAlertBinding.llToCurrency.setVisibility(View.GONE);
                    fragmentAddAlertBinding.llFromCurrency.setVisibility(View.GONE);
                }
            }
        });
    }
}
