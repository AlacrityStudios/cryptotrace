package com.alacritystudios.cryptotrace.ui.addalert.confirmalert;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.DialogConfirmAlertBinding;
import com.alacritystudios.cryptotrace.ui.addalert.selectprice.SelectPriceFragment;

/**
 * @author Anuj Dutt
 *  A dialog fragment to show alert details before confirming it.
 */

public class ConfirmAlertFragment extends DialogFragment {


    DialogConfirmAlertBinding dialogConfirmAlertBinding;

    FragmentBindingComponent fragmentBindingComponent;


    public ConfirmAlertFragment() {
        // Required empty public constructor
    }

    public static ConfirmAlertFragment getInstance() {
        ConfirmAlertFragment confirmAlertFragment = new ConfirmAlertFragment();
        Bundle bundle = new Bundle();
        confirmAlertFragment.setArguments(bundle);
        return confirmAlertFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        dialogConfirmAlertBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_confirm_alert, container, false, fragmentBindingComponent);
        return dialogConfirmAlertBinding.getRoot();
    }
}
