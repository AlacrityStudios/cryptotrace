package com.alacritystudios.cryptotrace.ui.dialog;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.FiatCurrencyListSelectionAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.DialogFragmentSelectFiatCurrencyBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.addnewalert.AddNewAlertActivity;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivity;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivityViewModel;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.AddNewAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectFiatCurrencyFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * @author Anuj Dutt
 *         Fragment to display the list of fiat-currencies.
 */

public class SelectFiatCurrencyDialogFragment extends DialogFragment {

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    SelectFiatCurrencyFragmentViewModel selectFiatCurrencyFragmentViewModel;

    SelectParamsViewModelListener parentViewModel;

    FragmentBindingComponent fragmentBindingComponent;

    DialogFragmentSelectFiatCurrencyBinding dialogFragmentSelectFiatCurrencyBinding;

    AutoClearedValue<FiatCurrencyListSelectionAdapter> fiatCurrencyListSelectionAdapterAutoClearedValue;

    public SelectFiatCurrencyDialogFragment() {
        // Required empty public constructor
    }

    public static SelectFiatCurrencyDialogFragment getInstance() {
        return new SelectFiatCurrencyDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        dialogFragmentSelectFiatCurrencyBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_select_fiat_currency, container, false, fragmentBindingComponent);
        setupRecyclerViews();
        return dialogFragmentSelectFiatCurrencyBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        selectFiatCurrencyFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(SelectFiatCurrencyFragmentViewModel.class);
        if (getActivity() != null && getActivity() instanceof CryptoDetailsActivity) {
            parentViewModel = ViewModelProviders.of((CryptoDetailsActivity) getActivity(), cryptoTraceViewModelFactory).get(CryptoDetailsActivityViewModel.class);
        } else if (getActivity() != null && getActivity() instanceof AddNewAlertActivity) {
            parentViewModel = ViewModelProviders.of((AddNewAlertActivity) getActivity(), cryptoTraceViewModelFactory).get(AddNewAlertActivityViewModel.class);
        }
        initialiseTextWatcher();
        setupLiveDataListener();
        setupClickListeners();
        selectFiatCurrencyFragmentViewModel.setToCurrencyNameLiveData("");
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void setupClickListeners() {
        dialogFragmentSelectFiatCurrencyBinding.tvSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    private void initialiseTextWatcher() {
        dialogFragmentSelectFiatCurrencyBinding.etToCurrencyName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectFiatCurrencyFragmentViewModel.setToCurrencyNameLiveData(s.toString());
            }
        });
    }

    private void setupLiveDataListener() {
        selectFiatCurrencyFragmentViewModel.getFiatCurrencyListLiveData().observe(this, new Observer<Resource<List<FiatCurrency>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<FiatCurrency>> listResource) {
                if (listResource != null && fiatCurrencyListSelectionAdapterAutoClearedValue.get() != null) {
                    if (listResource.status == Resource.Status.LOADING) {
                        dialogFragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setRefreshing(true);
                        fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else if (listResource.status == Resource.Status.SUCCESS) {
                        dialogFragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setRefreshing(false);
                        fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else {
                        dialogFragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setRefreshing(false);
                        fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showErrorLayout();
                    }
                } else {
                    dialogFragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setRefreshing(false);
                    fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(new ArrayList<FiatCurrency>());
                    showErrorLayout();
                }
            }
        });
    }


    private void setupRecyclerViews() {
        fiatCurrencyListSelectionAdapterAutoClearedValue = new AutoClearedValue<>(new FiatCurrencyListSelectionAdapter(fragmentBindingComponent, new FiatCurrencyListSelectionAdapter.FiatCurrencyClickCallback() {

            @Override
            public void onItemClick(FiatCurrency fiatCurrency) {
                parentViewModel.setToCurrency(fiatCurrency);
                getDialog().dismiss();
            }
        }), this);
        dialogFragmentSelectFiatCurrencyBinding.rvToCurrencyList.setHasFixedSize(true);
        dialogFragmentSelectFiatCurrencyBinding.rvToCurrencyList.setAdapter(fiatCurrencyListSelectionAdapterAutoClearedValue.get());
        dialogFragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setEnabled(false);
    }

    private void showErrorLayout() {
        dialogFragmentSelectFiatCurrencyBinding.llToCurrencyPlaceholder.setVisibility(View.VISIBLE);
        dialogFragmentSelectFiatCurrencyBinding.rvToCurrencyList.setVisibility(View.GONE);
    }

    private void showContentLayout() {
        dialogFragmentSelectFiatCurrencyBinding.llToCurrencyPlaceholder.setVisibility(View.GONE);
        dialogFragmentSelectFiatCurrencyBinding.rvToCurrencyList.setVisibility(View.VISIBLE);
    }
}
