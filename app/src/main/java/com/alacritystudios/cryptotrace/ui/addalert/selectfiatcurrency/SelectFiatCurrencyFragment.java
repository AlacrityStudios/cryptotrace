package com.alacritystudios.cryptotrace.ui.addalert.selectfiatcurrency;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.CryptoCurrencyListSelectionAdapter;
import com.alacritystudios.cryptotrace.adapter.FiatCurrencyListSelectionAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentSelectCryptoCurrencyBinding;
import com.alacritystudios.cryptotrace.databinding.FragmentSelectFiatCurrencyBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.addalert.AddAlertActivity;
import com.alacritystudios.cryptotrace.ui.addalert.selectcryptocurrency.SelectCryptoCurrencyFragment;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectCryptoCurrencyFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectFiatCurrencyFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectFiatCurrencyFragment extends Fragment {

    private static final String TO_CURRENCY_NAME = "TO_CURRENCY_NAME";

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    SelectFiatCurrencyFragmentViewModel selectFiatCurrencyFragmentViewModel;

    AddAlertActivityViewModel addAlertActivityViewModel;

    FragmentBindingComponent fragmentBindingComponent;

    FragmentSelectFiatCurrencyBinding fragmentSelectFiatCurrencyBinding;

    AutoClearedValue<FiatCurrencyListSelectionAdapter> fiatCurrencyListSelectionAdapterAutoClearedValue;

    public SelectFiatCurrencyFragment() {
        // Required empty public constructor
    }

    public static SelectFiatCurrencyFragment getInstance(String toCurrencyName) {
        SelectFiatCurrencyFragment selectFiatCurrencyFragment = new SelectFiatCurrencyFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TO_CURRENCY_NAME, toCurrencyName);
        selectFiatCurrencyFragment.setArguments(bundle);
        return selectFiatCurrencyFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentSelectFiatCurrencyBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_fiat_currency, container, false, fragmentBindingComponent);
        setupRecyclerViews();
        return fragmentSelectFiatCurrencyBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        selectFiatCurrencyFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(SelectFiatCurrencyFragmentViewModel.class);
        addAlertActivityViewModel = ViewModelProviders.of((AddAlertActivity)getActivity(), cryptoTraceViewModelFactory).get(AddAlertActivityViewModel.class);
        initialiseTextWatcher();
        setupLiveDataListener();
        setupClickListeners();
        selectFiatCurrencyFragmentViewModel.setToCurrencyNameLiveData("");
    }

    private void setupClickListeners() {
        fragmentSelectFiatCurrencyBinding.llNextFragment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkValidity()) {
                    addAlertActivityViewModel.setBreadCrumbLiveData(AddAlertActivityViewModel.AddAlertActivityBreadCrumb.SELECT_EXCHANGE_CURRENCY_FRAGMENT);
                } else {
                    Snackbar.make(fragmentSelectFiatCurrencyBinding.getRoot(), getString(R.string.select_fiat_currency_validity_error_message), 2000).show();
                }
            }
        });

        fragmentSelectFiatCurrencyBinding.llPreviousFragment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                addAlertActivityViewModel.setBreadCrumbLiveData(AddAlertActivityViewModel.AddAlertActivityBreadCrumb.SELECT_CRYPTO_CURRENCY_FRAGMENT);
            }
        });
    }

    private void initialiseTextWatcher() {
        fragmentSelectFiatCurrencyBinding.etToCurrencyName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectFiatCurrencyFragmentViewModel.setToCurrencyNameLiveData(s.toString());
            }
        });
    }

    private void setupLiveDataListener() {
        selectFiatCurrencyFragmentViewModel.getFiatCurrencyListLiveData().observe(this, new Observer<Resource<List<FiatCurrency>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<FiatCurrency>> listResource) {
                if (listResource != null && fiatCurrencyListSelectionAdapterAutoClearedValue.get() != null) {
                    if (listResource.status == Resource.Status.LOADING) {
                        fragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setRefreshing(true);
                        fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else if (listResource.status == Resource.Status.SUCCESS) {
                        fragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setRefreshing(false);
                        fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else {
                        fragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setRefreshing(false);
                        fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        if(listResource.data == null || listResource.data.size() == 0) {
                            showErrorLayout();
                        }
                    }
                } else {
                    fragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setRefreshing(false);
                    fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(new ArrayList<FiatCurrency>());
                    showErrorLayout();
                }
            }
        });
    }


    private void setupRecyclerViews() {
        fiatCurrencyListSelectionAdapterAutoClearedValue = new AutoClearedValue<>(new FiatCurrencyListSelectionAdapter(fragmentBindingComponent, new FiatCurrencyListSelectionAdapter.FiatCurrencyClickCallback() {

            @Override
            public void onItemClick(FiatCurrency fiatCurrency) {
                fragmentSelectFiatCurrencyBinding.etToCurrencyName.setText(fiatCurrency.coinName);
                addAlertActivityViewModel.setToCurrency(fiatCurrency);
            }
        }), this);
        fragmentSelectFiatCurrencyBinding.rvToCurrencyList.setHasFixedSize(true);
        fragmentSelectFiatCurrencyBinding.rvToCurrencyList.setAdapter(fiatCurrencyListSelectionAdapterAutoClearedValue.get());
        fragmentSelectFiatCurrencyBinding.srlSelectFiatCurrency.setEnabled(false);
    }

    private void showErrorLayout() {
        fragmentSelectFiatCurrencyBinding.llToCurrencyPlaceholder.setVisibility(View.VISIBLE);
        fragmentSelectFiatCurrencyBinding.rvToCurrencyList.setVisibility(View.GONE);
    }

    private void showContentLayout() {
        fragmentSelectFiatCurrencyBinding.llToCurrencyPlaceholder.setVisibility(View.GONE);
        fragmentSelectFiatCurrencyBinding.rvToCurrencyList.setVisibility(View.VISIBLE);
    }

    private boolean checkValidity() {
        return addAlertActivityViewModel.getParams().getValue() != null && addAlertActivityViewModel.getParams().getValue().fiatCurrency != null;
    }

}



