package com.alacritystudios.cryptotrace.ui.main.alertslist;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.AlertListAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentAlertListBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.addnewalert.AddNewAlertActivity;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.AlertListFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.AlertDisplayDetails;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlertListFragment extends Fragment {

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    AlertListFragmentViewModel alertListViewModel;

    FragmentAlertListBinding fragmentAlertListBinding;

    AutoClearedValue<AlertListAdapter> alertListAdapterAutoClearedValue;

    FragmentBindingComponent fragmentBindingComponent;

    public AlertListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentAlertListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_alert_list, container, false, fragmentBindingComponent);
        setupToolbar();
        setupRecyclerView();
        setupClickListeners();
        return fragmentAlertListBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        alertListViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(AlertListFragmentViewModel.class);
        alertListViewModel.getResults().observe(this, new Observer<Resource<List<AlertDisplayDetails>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<AlertDisplayDetails>> listResource) {
                if (listResource != null && alertListAdapterAutoClearedValue.get() != null) {
                    alertListAdapterAutoClearedValue.get().setDataList(listResource.data);
                    if (listResource.status == Resource.Status.LOADING) {
                        fragmentAlertListBinding.srlAlertList.setRefreshing(true);
                        showContentLayout();
                    } else if (listResource.status == Resource.Status.SUCCESS) {
                        fragmentAlertListBinding.srlAlertList.setRefreshing(false);
                        if (listResource.data == null || listResource.data.size() == 0) {
                            showErrorLayout();
                        } else {
                            showContentLayout();
                        }
                    } else {
                        fragmentAlertListBinding.srlAlertList.setRefreshing(false);
                        showErrorLayout();
                    }
                } else {
                    fragmentAlertListBinding.srlAlertList.setRefreshing(false);
                    alertListAdapterAutoClearedValue.get().setDataList(new ArrayList<AlertDisplayDetails>());
                    showErrorLayout();
                }
            }
        });
        fragmentAlertListBinding.srlAlertList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                alertListViewModel.incrementReloadCount();
            }
        });
    }

    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(fragmentAlertListBinding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.alert_list_fragment_heading);
    }

    private void setupRecyclerView() {
        alertListAdapterAutoClearedValue = new AutoClearedValue<>(new AlertListAdapter(fragmentBindingComponent), this);
        fragmentAlertListBinding.rvAlertList.setHasFixedSize(true);
        fragmentAlertListBinding.rvAlertList.setAdapter(alertListAdapterAutoClearedValue.get());
    }

    private void setupClickListeners() {
        fragmentAlertListBinding.fabAddNewAlert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddNewAlertActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showErrorLayout() {
        fragmentAlertListBinding.tvError.setVisibility(View.VISIBLE);
        fragmentAlertListBinding.rvAlertList.setVisibility(View.GONE);
    }

    private void showContentLayout() {
        fragmentAlertListBinding.tvError.setVisibility(View.GONE);
        fragmentAlertListBinding.rvAlertList.setVisibility(View.VISIBLE);
    }
}
