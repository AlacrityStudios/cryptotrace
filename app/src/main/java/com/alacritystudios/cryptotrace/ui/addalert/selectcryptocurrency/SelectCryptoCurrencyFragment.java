package com.alacritystudios.cryptotrace.ui.addalert.selectcryptocurrency;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.CryptoCurrencyListSelectionAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentSelectCryptoCurrencyBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.addalert.AddAlertActivity;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectCryptoCurrencyFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectCryptoCurrencyFragment extends Fragment {

    private static final String FROM_CURRENCY_NAME = "FROM_CURRENCY_NAME";

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    SelectCryptoCurrencyFragmentViewModel selectCryptoCurrencyFragmentViewModel;

    AddAlertActivityViewModel addAlertActivityViewModel;

    FragmentBindingComponent fragmentBindingComponent;

    FragmentSelectCryptoCurrencyBinding fragmentSelectCryptoCurrencyBinding;

    AutoClearedValue<CryptoCurrencyListSelectionAdapter> cryptoCurrencyListSelectionAdapterAutoClearedValue;

    public SelectCryptoCurrencyFragment() {
        // Required empty public constructor
    }

    public static SelectCryptoCurrencyFragment getInstance(String fromCurrencyName) {
        SelectCryptoCurrencyFragment selectCryptoCurrencyFragment = new SelectCryptoCurrencyFragment();
        Bundle bundle = new Bundle();
        bundle.putString(FROM_CURRENCY_NAME, fromCurrencyName);
        selectCryptoCurrencyFragment.setArguments(bundle);
        return selectCryptoCurrencyFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentSelectCryptoCurrencyBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_crypto_currency, container, false, fragmentBindingComponent);
        setupRecyclerViews();
        return fragmentSelectCryptoCurrencyBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        selectCryptoCurrencyFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(SelectCryptoCurrencyFragmentViewModel.class);
        addAlertActivityViewModel = ViewModelProviders.of((AddAlertActivity) getActivity(), cryptoTraceViewModelFactory).get(AddAlertActivityViewModel.class);
        initialiseTextWatcher();
        setupLiveDataListeners();
        setupClickListeners();
        selectCryptoCurrencyFragmentViewModel.setFromCurrencyNameLiveData("");
    }

    private void setupClickListeners() {
        fragmentSelectCryptoCurrencyBinding.llNextFragment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkValidity()) {
                    addAlertActivityViewModel.setBreadCrumbLiveData(AddAlertActivityViewModel.AddAlertActivityBreadCrumb.SELECT_FIAT_CURRENCY_FRAGMENT);
                } else {
                    Snackbar.make(fragmentSelectCryptoCurrencyBinding.getRoot(), getString(R.string.select_crypto_currency_validity_error_message), 2000).show();
                }
            }
        });
    }

    private void initialiseTextWatcher() {
        fragmentSelectCryptoCurrencyBinding.etFromCurrencyName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectCryptoCurrencyFragmentViewModel.setFromCurrencyNameLiveData(s.toString());
            }
        });
    }

    private void setupLiveDataListeners() {
        selectCryptoCurrencyFragmentViewModel.getCryptoCurrencyListLiveData().observe(this, new Observer<Resource<List<CryptoCurrency>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<CryptoCurrency>> listResource) {
                if (listResource != null && cryptoCurrencyListSelectionAdapterAutoClearedValue.get() != null) {
                    if (listResource.status == Resource.Status.LOADING) {
                        fragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setRefreshing(true);
                        cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else if (listResource.status == Resource.Status.SUCCESS) {
                        fragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setRefreshing(false);
                        cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else {
                        fragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setRefreshing(false);
                        cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        if(listResource.data == null || listResource.data.size() == 0) {
                            showErrorLayout();
                        }
                    }
                } else {
                    fragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setRefreshing(false);
                    cryptoCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(new ArrayList<CryptoCurrency>());
                    showErrorLayout();
                }
            }
        });
    }

    private void setupRecyclerViews() {
        cryptoCurrencyListSelectionAdapterAutoClearedValue = new AutoClearedValue<>(new CryptoCurrencyListSelectionAdapter(fragmentBindingComponent, new CryptoCurrencyListSelectionAdapter.CryptoCurrencyClickCallback() {

            @Override
            public void onItemClick(CryptoCurrency cryptoCurrency) {
                fragmentSelectCryptoCurrencyBinding.etFromCurrencyName.setText(cryptoCurrency.coinName);
                addAlertActivityViewModel.setFromCurrency(cryptoCurrency);
            }
        }), this);
        fragmentSelectCryptoCurrencyBinding.rvFromCurrencyList.setHasFixedSize(true);
        fragmentSelectCryptoCurrencyBinding.rvFromCurrencyList.setAdapter(cryptoCurrencyListSelectionAdapterAutoClearedValue.get());
        fragmentSelectCryptoCurrencyBinding.srlSelectCryptoCurrency.setEnabled(false);
    }

    private void showErrorLayout() {
        fragmentSelectCryptoCurrencyBinding.llFromCurrencyPlaceholder.setVisibility(View.VISIBLE);
        fragmentSelectCryptoCurrencyBinding.rvFromCurrencyList.setVisibility(View.GONE);
    }

    private void showContentLayout() {
        fragmentSelectCryptoCurrencyBinding.llFromCurrencyPlaceholder.setVisibility(View.GONE);
        fragmentSelectCryptoCurrencyBinding.rvFromCurrencyList.setVisibility(View.VISIBLE);
    }

    private boolean checkValidity() {
        return addAlertActivityViewModel.getParams().getValue() != null && addAlertActivityViewModel.getParams().getValue().cryptoCurrency != null;
    }
}
