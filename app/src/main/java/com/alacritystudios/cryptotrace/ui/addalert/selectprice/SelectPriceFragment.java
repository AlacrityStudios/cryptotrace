package com.alacritystudios.cryptotrace.ui.addalert.selectprice;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentSelectPriceBinding;
import com.alacritystudios.cryptotrace.ui.addalert.AddAlertActivity;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectPriceFragmentViewModel;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectPriceFragment extends Fragment {

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    SelectPriceFragmentViewModel selectPriceFragmentViewModel;

    AddAlertActivityViewModel addAlertActivityViewModel;

    FragmentBindingComponent fragmentBindingComponent;

    FragmentSelectPriceBinding fragmentSelectPriceBinding;

    public SelectPriceFragment() {
        // Required empty public constructor
    }

    public static SelectPriceFragment getInstance() {
        SelectPriceFragment selectPriceFragment = new SelectPriceFragment();
        Bundle bundle = new Bundle();
        selectPriceFragment.setArguments(bundle);
        return selectPriceFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentSelectPriceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_price, container, false, fragmentBindingComponent);
        return fragmentSelectPriceBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        selectPriceFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(SelectPriceFragmentViewModel.class);
        addAlertActivityViewModel = ViewModelProviders.of((AddAlertActivity)getActivity(), cryptoTraceViewModelFactory).get(AddAlertActivityViewModel.class);
        initialiseTextWatcher();
        setupLiveDataListener();
        setupClickListeners();
    }

    private void setupClickListeners() {
        fragmentSelectPriceBinding.llNextFragment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkValidity()) {
                    addAlertActivityViewModel.setBreadCrumbLiveData(AddAlertActivityViewModel.AddAlertActivityBreadCrumb.SELECT_EXCHANGE_CURRENCY_FRAGMENT);
                } else {
                    Snackbar.make(fragmentSelectPriceBinding.getRoot(), getString(R.string.select_price_validity_error_message), 2000).show();
                }
            }
        });

        fragmentSelectPriceBinding.llPreviousFragment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                addAlertActivityViewModel.setBreadCrumbLiveData(AddAlertActivityViewModel.AddAlertActivityBreadCrumb.SELECT_EXCHANGE_CURRENCY_FRAGMENT);
            }
        });
    }

    private void initialiseTextWatcher() {
        fragmentSelectPriceBinding.etCryptoCurrencyPrice.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s != null && count > 0) {
                    addAlertActivityViewModel.setPrice(Double.parseDouble(s.toString()));
                } else {
                    addAlertActivityViewModel.setPrice(null);
                }
            }
        });
    }

    private void setupLiveDataListener() {
//        selectPriceFragmentViewModel.getFiatCurrencyListLiveData().observe(this, new Observer<Resource<List<FiatCurrency>>>() {
//
//            @Override
//            public void onChanged(@Nullable Resource<List<FiatCurrency>> listResource) {
//                if (listResource != null && fiatCurrencyListSelectionAdapterAutoClearedValue.get() != null) {
//                    if (listResource.status == Resource.Status.LOADING) {
//                        fragmentSelectPriceBinding.srlSelectPrice.setRefreshing(true);
//                        fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
//                        showContentLayout();
//                    } else if (listResource.status == Resource.Status.SUCCESS) {
//                        fragmentSelectPriceBinding.srlSelectPrice.setRefreshing(false);
//                        fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
//                        showContentLayout();
//                    } else {
//                        fragmentSelectPriceBinding.srlSelectPrice.setRefreshing(false);
//                        fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
//                        showErrorLayout();
//                    }
//                } else {
//                    fragmentSelectPriceBinding.srlSelectPrice.setRefreshing(false);
//                    fiatCurrencyListSelectionAdapterAutoClearedValue.get().setDataList(new ArrayList<FiatCurrency>());
//                    showErrorLayout();
//                }
//            }
//        });
    }

    private boolean checkValidity() {
        return addAlertActivityViewModel.getParams().getValue() != null && addAlertActivityViewModel.getParams().getValue().price != null;
    }

}