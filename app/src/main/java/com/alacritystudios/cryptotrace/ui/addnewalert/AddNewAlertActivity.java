package com.alacritystudios.cryptotrace.ui.addnewalert;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.ActivityAddNewAlertBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.dialog.SelectCryptoCurrencyDialogFragment;
import com.alacritystudios.cryptotrace.ui.dialog.SelectExchangeDialogFragment;
import com.alacritystudios.cryptotrace.ui.dialog.SelectFiatCurrencyDialogFragment;
import com.alacritystudios.cryptotrace.viewmodel.AddNewAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoTraceViewModelFactory;

import java.text.NumberFormat;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class AddNewAlertActivity extends AppCompatActivity implements HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    CryptoTraceViewModelFactory cryptoTraceViewModelFactory;

    ActivityAddNewAlertBinding activityAddNewAlertBinding;

    FragmentBindingComponent fragmentBindingComponent;

    AddNewAlertActivityViewModel addNewAlertActivityViewModel;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        fragmentBindingComponent = new FragmentBindingComponent();
        activityAddNewAlertBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_new_alert, fragmentBindingComponent);
        initialiseViewModel();
        setLiveDataListeners();
        setupToolbar();
        setupFromCurrencyTextView();
        setupToCurrencyTextView();
        setupExchangeTextView();
        setFabClickListener();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    private void initialiseViewModel() {
        addNewAlertActivityViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(AddNewAlertActivityViewModel.class);

    }

    private void setLiveDataListeners() {
        addNewAlertActivityViewModel.getParamsLiveData().observe(this, new Observer<AddNewAlertActivityViewModel.AddNewAlertActivityQueryParams>() {

            @Override
            public void onChanged(AddNewAlertActivityViewModel.AddNewAlertActivityQueryParams addNewAlertActivityQueryParams) {
                if (addNewAlertActivityQueryParams.cryptoCurrency != null) {
                    activityAddNewAlertBinding.setCryptoCurrencyFullName(addNewAlertActivityQueryParams.cryptoCurrency.coinName);
                    activityAddNewAlertBinding.tvFromCurrency.setActivated(false);
                }
                if (addNewAlertActivityQueryParams.fiatCurrency != null) {
                    activityAddNewAlertBinding.setFiatCurrencyFullName(addNewAlertActivityQueryParams.fiatCurrency.coinName);
                    activityAddNewAlertBinding.setFiatCurrencyImageUrl(addNewAlertActivityQueryParams.fiatCurrency.imageUrl);
                    activityAddNewAlertBinding.tvToCurrency.setActivated(false);
                }
                if (addNewAlertActivityQueryParams.exchange != null) {
                    activityAddNewAlertBinding.setExchangeName(addNewAlertActivityQueryParams.exchange.name);
                    activityAddNewAlertBinding.tvExchange.setActivated(false);
                }
            }
        });

        addNewAlertActivityViewModel.getPriceLiveData().observe(this, new Observer<Resource<Double>>() {

            @Override
            public void onChanged(Resource<Double> doubleResource) {
                if (doubleResource.status == Resource.Status.LOADING) {
                    activityAddNewAlertBinding.setPrice("...");
                } else if (doubleResource.status == Resource.Status.ERROR) {
                    activityAddNewAlertBinding.setPrice("-");
                    Snackbar.make(activityAddNewAlertBinding.getRoot(), R.string.basic_params_error_message, 2000).show();
                } else {
                    activityAddNewAlertBinding.setPrice(NumberFormat.getNumberInstance().format(doubleResource.data));
                }
            }
        });
    }

    private void setupToolbar() {
        setSupportActionBar(activityAddNewAlertBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.add_new_alert);
    }

    private void setupFromCurrencyTextView() {
        activityAddNewAlertBinding.tvFromCurrency.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SelectCryptoCurrencyDialogFragment selectCryptoCurrencyDialogFragment = SelectCryptoCurrencyDialogFragment.getInstance();
                selectCryptoCurrencyDialogFragment.show(getSupportFragmentManager(), "SelectCryptoCurrencyDialogFragment");
            }
        });
    }

    private void setupToCurrencyTextView() {
        activityAddNewAlertBinding.tvToCurrency.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SelectFiatCurrencyDialogFragment selectFiatCurrencyDialogFragment = SelectFiatCurrencyDialogFragment.getInstance();
                selectFiatCurrencyDialogFragment.show(getSupportFragmentManager(), "SelectFiatCurrencyDialogFragment");
            }
        });
    }

    private void setupExchangeTextView() {
        activityAddNewAlertBinding.tvExchange.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SelectExchangeDialogFragment selectExchangeDialogFragment = SelectExchangeDialogFragment.getInstance();
                selectExchangeDialogFragment.show(getSupportFragmentManager(), "SelectExchangeDialogFragment");
            }
        });
    }

    private void setFabClickListener() {
        activityAddNewAlertBinding.acbConfirmAlert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkValidityOfData()) {
                    addNewAlertActivityViewModel.saveAlert(Double.parseDouble(activityAddNewAlertBinding.etPrice.getText().toString()));
                }
            }
        });
    }

    public boolean checkValidityOfData() {
        boolean valid = true;
        if (addNewAlertActivityViewModel.getParamsLiveData().getValue() == null || addNewAlertActivityViewModel.getParamsLiveData().getValue().cryptoCurrency == null) {
            activityAddNewAlertBinding.tvFromCurrency.setActivated(true);
            valid = false;
        } else {
            activityAddNewAlertBinding.tvFromCurrency.setActivated(false);
        }
        if (addNewAlertActivityViewModel.getParamsLiveData().getValue() == null || addNewAlertActivityViewModel.getParamsLiveData().getValue().fiatCurrency == null) {
            activityAddNewAlertBinding.tvToCurrency.setActivated(true);
            valid = false;
        } else {
            activityAddNewAlertBinding.tvToCurrency.setActivated(false);
        }
        if (addNewAlertActivityViewModel.getParamsLiveData().getValue() == null || addNewAlertActivityViewModel.getParamsLiveData().getValue().exchange == null) {
            activityAddNewAlertBinding.tvExchange.setActivated(true);
            valid = false;
        } else {
            activityAddNewAlertBinding.tvExchange.setActivated(false);
        }
        if (activityAddNewAlertBinding.etPrice.getText() == null || activityAddNewAlertBinding.etPrice.getText().toString().length() == 0) {
            activityAddNewAlertBinding.etPrice.setError(getString(R.string.enter_price));
            valid = false;
        } else {
            activityAddNewAlertBinding.etPrice.setError(null);
        }

        if (addNewAlertActivityViewModel.getPriceLiveData().getValue() == null || addNewAlertActivityViewModel.getPriceLiveData().getValue().data == null) {
            showPriceErrorDialog();
            valid = false;
        }
        return valid;
    }

    public void showPriceErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.price_error_message)
                .setTitle(R.string.action_required);
        builder.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                addNewAlertActivityViewModel.retryPriceFetch();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
