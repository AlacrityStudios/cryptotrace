package com.alacritystudios.cryptotrace.ui.addalert;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.ActivityAddAlertBinding;
import com.alacritystudios.cryptotrace.ui.addalert.addcryptocurrencyfragment.AddCryptoCurrencyFragment;
import com.alacritystudios.cryptotrace.ui.addalert.confirmalert.ConfirmAlertFragment;
import com.alacritystudios.cryptotrace.ui.addalert.selectcryptocurrency.SelectCryptoCurrencyFragment;
import com.alacritystudios.cryptotrace.ui.addalert.selectexchange.SelectExchangeFragment;
import com.alacritystudios.cryptotrace.ui.addalert.selectfiatcurrency.SelectFiatCurrencyFragment;
import com.alacritystudios.cryptotrace.ui.addalert.selectprice.SelectPriceFragment;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoTraceViewModelFactory;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class AddAlertActivity extends AppCompatActivity implements HasSupportFragmentInjector{

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    CryptoTraceViewModelFactory cryptoTraceViewModelFactory;

    FragmentBindingComponent fragmentBindingComponent;

    ActivityAddAlertBinding activityAddAlertBinding;

    AddAlertActivityViewModel addAlertActivityViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        fragmentBindingComponent = new FragmentBindingComponent();
        activityAddAlertBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_alert, fragmentBindingComponent);
        setupToolbar();
        initialiseViewModel();
    }

    public void loadAddAlertFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, AddCryptoCurrencyFragment.getInstance("", "", ""), "AddCryptoCurrencyFragment");
        fragmentTransaction.commit();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    private void initialiseViewModel() {
        addAlertActivityViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(AddAlertActivityViewModel.class);
        setLiveDataListeners();
        addAlertActivityViewModel.setBreadCrumbLiveData(AddAlertActivityViewModel.AddAlertActivityBreadCrumb.SELECT_CRYPTO_CURRENCY_FRAGMENT);
        loadConfirmAlertFragment();
    }

    private void setLiveDataListeners() {
        addAlertActivityViewModel.getParams().observe(this, new Observer<AddAlertActivityViewModel.AddAlertActivityQueryParams>() {

            @Override
            public void onChanged(@Nullable AddAlertActivityViewModel.AddAlertActivityQueryParams addAlertActivityQueryParams) {
                activityAddAlertBinding.setSelectedFromCurrency(addAlertActivityQueryParams.cryptoCurrency);
                activityAddAlertBinding.setSelectedToCurrency(addAlertActivityQueryParams.fiatCurrency);
                activityAddAlertBinding.setSelectedExchange(addAlertActivityQueryParams.exchange);
            }
        });
        addAlertActivityViewModel.getBreadCrumbLiveData().observe(this, new Observer<AddAlertActivityViewModel.AddAlertActivityBreadCrumb>() {

            @Override
            public void onChanged(AddAlertActivityViewModel.AddAlertActivityBreadCrumb addAlertActivityBreadCrumb) {
                switch (addAlertActivityBreadCrumb) {
                    case SELECT_CRYPTO_CURRENCY_FRAGMENT:
                        loadSelectCryptoCurrencyFragment();
                        break;
                    case SELECT_FIAT_CURRENCY_FRAGMENT:
                        loadSelectFiatCurrencyFragment();
                        break;
                    case SELECT_EXCHANGE_CURRENCY_FRAGMENT:
                        loadSelectExchangeFragment();
                        break;
                    case SELECT_PRICE_FRAGMENT:
                        loadSelectPriceFragment();
                        break;
                }
            }
        });
    }

    private void setupToolbar() {
        setSupportActionBar(activityAddAlertBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
    }

    private void setFromCurrencyCrumbAsActive() {
        activityAddAlertBinding.tvSelectedFromCurrency.setTextAppearance(this, R.style.grey8014sp_semibold);
        activityAddAlertBinding.tvSelectedToCurrency.setTextAppearance(this, R.style.grey6012sp);
        activityAddAlertBinding.tvSelectedExchange.setTextAppearance(this, R.style.grey6012sp);
        activityAddAlertBinding.tvSelectedPrice.setTextAppearance(this, R.style.grey6012sp);
    }

    private void setToCurrencyCrumbAsActive() {
        activityAddAlertBinding.tvSelectedToCurrency.setTextAppearance(this, R.style.grey8014sp_semibold);
        activityAddAlertBinding.tvSelectedFromCurrency.setTextAppearance(this, R.style.grey6012sp);
        activityAddAlertBinding.tvSelectedExchange.setTextAppearance(this, R.style.grey6012sp);
        activityAddAlertBinding.tvSelectedPrice.setTextAppearance(this, R.style.grey6012sp);
    }

    private void setExchangeCrumbAsActive() {
        activityAddAlertBinding.tvSelectedExchange.setTextAppearance(this, R.style.grey8014sp_semibold);
        activityAddAlertBinding.tvSelectedFromCurrency.setTextAppearance(this, R.style.grey6012sp);
        activityAddAlertBinding.tvSelectedToCurrency.setTextAppearance(this, R.style.grey6012sp);
        activityAddAlertBinding.tvSelectedPrice.setTextAppearance(this, R.style.grey6012sp);
    }

    private void setPriceCrumbAsActive() {
        activityAddAlertBinding.tvSelectedPrice.setTextAppearance(this, R.style.grey8014sp_semibold);
        activityAddAlertBinding.tvSelectedFromCurrency.setTextAppearance(this, R.style.grey6012sp);
        activityAddAlertBinding.tvSelectedToCurrency.setTextAppearance(this, R.style.grey6012sp);
        activityAddAlertBinding.tvSelectedExchange.setTextAppearance(this, R.style.grey6012sp);
    }

    private void loadSelectCryptoCurrencyFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, SelectCryptoCurrencyFragment.getInstance(""), "SelectCryptoCurrencyFragment");
        fragmentTransaction.commit();
        setFromCurrencyCrumbAsActive();
    }

    private void loadSelectFiatCurrencyFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, SelectFiatCurrencyFragment.getInstance(""), "SelectFiatCurrencyFragment");
        fragmentTransaction.commit();
        setToCurrencyCrumbAsActive();
    }

    private void loadSelectExchangeFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, SelectExchangeFragment.getInstance(""), "SelectExchangeFragment");
        fragmentTransaction.commit();
        setExchangeCrumbAsActive();
    }

    private void loadSelectPriceFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, SelectPriceFragment.getInstance(), "SelectPriceFragment");
        fragmentTransaction.commit();
        setPriceCrumbAsActive();
    }

    private void loadConfirmAlertFragment() {
//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.fl_container, ConfirmAlertFragment.getInstance(), "ConfirmAlertFragment");
//        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
//        fragmentTransaction.commit();
        ConfirmAlertFragment.getInstance().show(getSupportFragmentManager(), "ConfirmAlertFragment");
    }
}
