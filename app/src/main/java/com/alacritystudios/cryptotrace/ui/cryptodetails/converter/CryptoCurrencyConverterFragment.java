package com.alacritystudios.cryptotrace.ui.cryptodetails.converter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;

import dagger.android.support.AndroidSupportInjection;

/**
 *
 */
public class CryptoCurrencyConverterFragment extends Fragment {
    private static final String CRYPTO_CURRENCY_ID = "cryptoCurrencyId";
    private String cryptoCurrencyId;

    public CryptoCurrencyConverterFragment() {
        // Required empty public constructor
    }

    public static CryptoCurrencyConverterFragment newInstance(String cryptoCurrencyId) {
        CryptoCurrencyConverterFragment fragment = new CryptoCurrencyConverterFragment();
        Bundle args = new Bundle();
        args.putString(CRYPTO_CURRENCY_ID, cryptoCurrencyId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_crypto_currency_converter, container, false);
    }
}
