package com.alacritystudios.cryptotrace.ui.cryptodetails;

import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.alacritystudios.cryptotrace.application.CryptoTraceApplication;
import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.repository.ExchangeRepository;
import com.alacritystudios.cryptotrace.repository.FiatCurrencyRepository;
import com.alacritystudios.cryptotrace.ui.dialog.SelectParamsViewModelListener;
import com.alacritystudios.cryptotrace.util.PreferenceUtil;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         A ViewModel to help fetch data about crypto-currency details.
 */

public class CryptoDetailsActivityViewModel extends AndroidViewModel implements SelectParamsViewModelListener {

    private final MediatorLiveData<CryptoCurrencyDetailsActivityParams> currencyDetailsActivityParamsMutableLiveData;

    private LiveData<CryptoCurrency> selectedCryptoCurrency;

    private MutableLiveData<String> selectedCryptoCurrencyId;

    private CryptoCurrencyRepository cryptoCurrencyRepository;

    private FiatCurrencyRepository fiatCurrencyRepository;

    private ExchangeRepository exchangeRepository;

    @Inject
    public CryptoDetailsActivityViewModel(@NonNull CryptoTraceApplication application, CryptoCurrencyRepository cryptoCurrencyRepository,
                                          FiatCurrencyRepository fiatCurrencyRepository, ExchangeRepository exchangeRepository) {
        super(application);
        this.cryptoCurrencyRepository = cryptoCurrencyRepository;
        this.fiatCurrencyRepository = fiatCurrencyRepository;
        this.exchangeRepository = exchangeRepository;
        currencyDetailsActivityParamsMutableLiveData = new MediatorLiveData<>();
        currencyDetailsActivityParamsMutableLiveData.setValue(new CryptoCurrencyDetailsActivityParams());
    }

    public void initialiseAllParams(String cryptoCurrencyId) {
        fetchAndSetCryptoCurrency(cryptoCurrencyId);
        fetchAndSetDefaultExchange();
        fetchAndSetDefaultFiatCurrency();
    }

    private void fetchAndSetCryptoCurrency(String cryptoCurrencyId) {
        currencyDetailsActivityParamsMutableLiveData.addSource(cryptoCurrencyRepository.fetchFavoriteCryptoCurrencyDetailsById(cryptoCurrencyId), new Observer<CryptoCurrency>() {

            @Override
            public void onChanged(@Nullable CryptoCurrency cryptoCurrency) {
                setFromCurrency(cryptoCurrency);
            }
        });
    }

    private void fetchAndSetDefaultFiatCurrency() {
        currencyDetailsActivityParamsMutableLiveData.addSource(fiatCurrencyRepository.fetchFiatCurrencyBySymbol(PreferenceUtil.getConversionCurrencyChoice(this.getApplication())), new Observer<FiatCurrency>() {

            @Override
            public void onChanged(@Nullable FiatCurrency fiatCurrency) {
                setToCurrency(fiatCurrency);
            }
        });
    }

    private void fetchAndSetDefaultExchange() {
        currencyDetailsActivityParamsMutableLiveData.addSource(exchangeRepository.getExchangeDetailsByExchangeSymbol(PreferenceUtil.getExchangeChoice(this.getApplication())), new Observer<Exchange>() {

            @Override
            public void onChanged(@Nullable Exchange exchange) {
                setExchange(exchange);
            }
        });
    }

    @Override
    public void setFromCurrency(CryptoCurrency cryptoCurrency) {
        CryptoCurrencyDetailsActivityParams cryptoCurrencyDetailsActivityParams = currencyDetailsActivityParamsMutableLiveData.getValue();
        if (cryptoCurrencyDetailsActivityParams != null) {
            cryptoCurrencyDetailsActivityParams.cryptoCurrency = cryptoCurrency;
        }
        currencyDetailsActivityParamsMutableLiveData.setValue(cryptoCurrencyDetailsActivityParams);
    }

    @Override
    public void setToCurrency(FiatCurrency fiatCurrency) {
        CryptoCurrencyDetailsActivityParams cryptoCurrencyDetailsActivityParams = currencyDetailsActivityParamsMutableLiveData.getValue();
        if (cryptoCurrencyDetailsActivityParams != null) {
            cryptoCurrencyDetailsActivityParams.fiatCurrency = fiatCurrency;
        }
        currencyDetailsActivityParamsMutableLiveData.setValue(cryptoCurrencyDetailsActivityParams);
    }

    @Override
    public void setExchange(Exchange exchange) {
        CryptoCurrencyDetailsActivityParams cryptoCurrencyDetailsActivityParams = currencyDetailsActivityParamsMutableLiveData.getValue();
        if (cryptoCurrencyDetailsActivityParams != null) {
            cryptoCurrencyDetailsActivityParams.exchange = exchange;
        }
        currencyDetailsActivityParamsMutableLiveData.setValue(cryptoCurrencyDetailsActivityParams);
    }

    public MediatorLiveData<CryptoCurrencyDetailsActivityParams> getParamsLiveData() {
        return currencyDetailsActivityParamsMutableLiveData;
    }

    public static class CryptoCurrencyDetailsActivityParams {

        public CryptoCurrency cryptoCurrency;
        public FiatCurrency fiatCurrency;
        public Exchange exchange;

        public CryptoCurrencyDetailsActivityParamsStatus getStatusOfParams() {
            if (cryptoCurrency != null && fiatCurrency != null && exchange != null) {
                return CryptoCurrencyDetailsActivityParamsStatus.READY;
            } else {
                return CryptoCurrencyDetailsActivityParamsStatus.NOT_READY;
            }
        }

        public enum CryptoCurrencyDetailsActivityParamsStatus {

            READY,
            NOT_READY
        }
    }
}
