package com.alacritystudios.cryptotrace.ui.cryptodetails;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.ActivityCryptoDetailsBinding;
import com.alacritystudios.cryptotrace.ui.cryptodetails.price.CryptoCurrencyPriceInformationFragment;
import com.alacritystudios.cryptotrace.ui.cryptodetails.social.CryptoCurrencySocialStatsFragment;
import com.alacritystudios.cryptotrace.ui.cryptodetails.stats.CryptoCurrencyStatsFragment;
import com.alacritystudios.cryptotrace.viewmodel.CryptoTraceViewModelFactory;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class CryptoDetailsActivity extends AppCompatActivity implements HasSupportFragmentInjector {

    private static final String CRYPTO_CURRENCY_ID = "cryptoCurrencyId";

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    CryptoTraceViewModelFactory cryptoTraceViewModelFactory;

    ActivityCryptoDetailsBinding activityCryptoDetailsBinding;

    FragmentBindingComponent fragmentBindingComponent;

    CryptoDetailsActivityViewModel cryptoDetailsActivityViewModel;

    String cryptoCurrencyId;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_crypto_details_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        postponeEnterTransition();
        cryptoCurrencyId = getIntent().getStringExtra(CRYPTO_CURRENCY_ID);
        fragmentBindingComponent = new FragmentBindingComponent();
        activityCryptoDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_crypto_details, fragmentBindingComponent);
        activityCryptoDetailsBinding.setCryptoCurrencyId(cryptoCurrencyId);
        setupToolbar();
        setupViewModel();
        setupViewPager();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    private void setupViewPager() {
        activityCryptoDetailsBinding.vpCryptoDetails.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return CryptoCurrencyPriceInformationFragment.newInstance();
                    case 1:
                        return CryptoCurrencyStatsFragment.newInstance();
                    case 2:
                        return CryptoCurrencySocialStatsFragment.newInstance();
                    default:
                        return null;
                }
            }

            @Override
            public int getCount() {
                return 3;
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getString(R.string.crypto_details_tab_heading_price);
                    case 1:
                        return getString(R.string.crypto_details_tab_heading_stats);
                    case 2:
                        return getString(R.string.crypto_details_tab_heading_social);
                    default:
                        return super.getPageTitle(position);
                }
            }
        });
        activityCryptoDetailsBinding.stlCryptoDetails.setViewPager(activityCryptoDetailsBinding.vpCryptoDetails);
        activityCryptoDetailsBinding.vpCryptoDetails.setOffscreenPageLimit(2);
    }

    private void setupToolbar() {
        setSupportActionBar(activityCryptoDetailsBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupViewModel() {
        cryptoDetailsActivityViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(CryptoDetailsActivityViewModel.class);
        cryptoDetailsActivityViewModel.initialiseAllParams(cryptoCurrencyId);
        cryptoDetailsActivityViewModel.getParamsLiveData().observe(this, new Observer<CryptoDetailsActivityViewModel.CryptoCurrencyDetailsActivityParams>() {

            @Override
            public void onChanged(CryptoDetailsActivityViewModel.CryptoCurrencyDetailsActivityParams cryptoCurrencyDetailsActivityParams) {
                if (cryptoCurrencyDetailsActivityParams.cryptoCurrency != null) {
                    activityCryptoDetailsBinding.setCryptoCurrency(cryptoCurrencyDetailsActivityParams.cryptoCurrency);
                    startPostponedEnterTransition();
                }
            }
        });
    }
}
