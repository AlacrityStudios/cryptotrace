package com.alacritystudios.cryptotrace.ui.addalert.selectexchange;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.ExchangeListSelectionAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentSelectExchangeBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.addalert.AddAlertActivity;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectExchangeFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.Exchange;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectExchangeFragment extends Fragment {

    private static final String EXCHANGE_NAME = "EXCHANGE_NAME";

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    SelectExchangeFragmentViewModel selectExchangeFragmentViewModel;

    AddAlertActivityViewModel addAlertActivityViewModel;

    FragmentBindingComponent fragmentBindingComponent;

    FragmentSelectExchangeBinding fragmentSelectExchangeBinding;

    AutoClearedValue<ExchangeListSelectionAdapter> exchangeListSelectionAdapterAutoClearedValue;

    public SelectExchangeFragment() {
        // Required empty public constructor
    }

    public static SelectExchangeFragment getInstance(String exchangeName) {
        SelectExchangeFragment selectExchangeFragment = new SelectExchangeFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXCHANGE_NAME, exchangeName);
        selectExchangeFragment.setArguments(bundle);
        return selectExchangeFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentSelectExchangeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_exchange, container, false, fragmentBindingComponent);
        setupRecyclerViews();
        return fragmentSelectExchangeBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        selectExchangeFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(SelectExchangeFragmentViewModel.class);
        addAlertActivityViewModel = ViewModelProviders.of((AddAlertActivity) getActivity(), cryptoTraceViewModelFactory).get(AddAlertActivityViewModel.class);
        initialiseTextWatcher();
        setupLiveDataListeners();
        setupClickListeners();
        selectExchangeFragmentViewModel.setExchangeNameLiveData("");
    }

    private void setupClickListeners() {
        fragmentSelectExchangeBinding.llNextFragment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkValidity()) {
                    addAlertActivityViewModel.setBreadCrumbLiveData(AddAlertActivityViewModel.AddAlertActivityBreadCrumb.SELECT_PRICE_FRAGMENT);
                } else {
                    Snackbar.make(fragmentSelectExchangeBinding.getRoot(), getString(R.string.select_exchange_validity_error_message), 2000).show();
                }
            }
        });

        fragmentSelectExchangeBinding.llPreviousFragment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                addAlertActivityViewModel.setBreadCrumbLiveData(AddAlertActivityViewModel.AddAlertActivityBreadCrumb.SELECT_FIAT_CURRENCY_FRAGMENT);
            }
        });
    }

    private void initialiseTextWatcher() {
        fragmentSelectExchangeBinding.etExchangeName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectExchangeFragmentViewModel.setExchangeNameLiveData(s.toString());
            }
        });
    }

    private void setupLiveDataListeners() {
        selectExchangeFragmentViewModel.getExchangeListLiveData().observe(this, new Observer<Resource<List<Exchange>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<Exchange>> listResource) {
                if (listResource != null && exchangeListSelectionAdapterAutoClearedValue.get() != null) {
                    if (listResource.status == Resource.Status.LOADING) {
                        fragmentSelectExchangeBinding.srlSelectExchange.setRefreshing(true);
                        exchangeListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else if (listResource.status == Resource.Status.SUCCESS) {
                        fragmentSelectExchangeBinding.srlSelectExchange.setRefreshing(false);
                        exchangeListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else {
                        fragmentSelectExchangeBinding.srlSelectExchange.setRefreshing(false);
                        exchangeListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        if(listResource.data == null || listResource.data.size() == 0) {
                            showErrorLayout();
                        }
                    }
                } else {
                    fragmentSelectExchangeBinding.srlSelectExchange.setRefreshing(false);
                    exchangeListSelectionAdapterAutoClearedValue.get().setDataList(new ArrayList<Exchange>());
                    showErrorLayout();
                }
            }
        });
    }

    private void setupRecyclerViews() {
        exchangeListSelectionAdapterAutoClearedValue = new AutoClearedValue<>(new ExchangeListSelectionAdapter(fragmentBindingComponent, new ExchangeListSelectionAdapter.ExchangeClickCallback() {

            @Override
            public void onItemClick(Exchange exchange) {
                fragmentSelectExchangeBinding.etExchangeName.setText(exchange.name);
                addAlertActivityViewModel.setExchange(exchange);
            }
        }), this);
        fragmentSelectExchangeBinding.rvExchangeList.setHasFixedSize(true);
        fragmentSelectExchangeBinding.rvExchangeList.setAdapter(exchangeListSelectionAdapterAutoClearedValue.get());
        fragmentSelectExchangeBinding.srlSelectExchange.setEnabled(false);
    }

    private void showErrorLayout() {
        fragmentSelectExchangeBinding.llExchangePlaceholder.setVisibility(View.VISIBLE);
        fragmentSelectExchangeBinding.rvExchangeList.setVisibility(View.GONE);
    }

    private void showContentLayout() {
        fragmentSelectExchangeBinding.llExchangePlaceholder.setVisibility(View.GONE);
        fragmentSelectExchangeBinding.rvExchangeList.setVisibility(View.VISIBLE);
    }

    private boolean checkValidity() {
        return addAlertActivityViewModel.getParams().getValue() != null && addAlertActivityViewModel.getParams().getValue().exchange != null;
    }
}
