package com.alacritystudios.cryptotrace.ui.main.home;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.CryptoCurrencyPriceListAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentHomeBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.dialog.SelectExchangeDialogFragment;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.util.BundleAndRequestConstants;
import com.alacritystudios.cryptotrace.viewmodel.HomeFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPrice;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    HomeFragmentViewModel homeFragmentViewModel;

    FragmentHomeBinding fragmentHomeBinding;

    AutoClearedValue<CryptoCurrencyPriceListAdapter> cryptoCurrencyPriceListAdapterAutoClearedValue;

    FragmentBindingComponent fragmentBindingComponent;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home,
                container, false, fragmentBindingComponent);
        setupToolbar();
        setupRecyclerView();
        setupOptionsLayout();
        return fragmentHomeBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        homeFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(HomeFragmentViewModel.class);
        homeFragmentViewModel.getResults().observe(this, new Observer<Resource<List<CryptoCurrencyPrice>>>() {

            @Override
            public void onChanged(Resource<List<CryptoCurrencyPrice>> cryptoCurrencyPrices) {
                if (cryptoCurrencyPrices != null) {
                    if (cryptoCurrencyPrices.status == Resource.Status.LOADING) {
                        fragmentHomeBinding.srlCryptoCurrencyList.setRefreshing(true);
                        cryptoCurrencyPriceListAdapterAutoClearedValue.get().setCryptoCurrencyList(cryptoCurrencyPrices.data);
                    }

                    if (cryptoCurrencyPrices.status == Resource.Status.ERROR) {
                        Snackbar.make(fragmentHomeBinding.getRoot(), cryptoCurrencyPrices.errorMessage, 2000).show();
                        cryptoCurrencyPriceListAdapterAutoClearedValue.get().setCryptoCurrencyList(cryptoCurrencyPrices.data);
                        fragmentHomeBinding.srlCryptoCurrencyList.setRefreshing(false);
//                    fragmentHomeBinding.llError.setVisibility(View.VISIBLE);
//                    fragmentHomeBinding.llContent.setVisibility(View.GONE);
                    }

                    if (cryptoCurrencyPrices.status == Resource.Status.SUCCESS) {
                        cryptoCurrencyPriceListAdapterAutoClearedValue.get().setCryptoCurrencyList(cryptoCurrencyPrices.data);
                        fragmentHomeBinding.srlCryptoCurrencyList.setRefreshing(false);
                        fragmentHomeBinding.llError.setVisibility(View.GONE);
                        fragmentHomeBinding.llContent.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        homeFragmentViewModel.getToCurrencyDetailsLiveData().observe(this, new Observer<FiatCurrency>() {

            @Override
            public void onChanged(@Nullable FiatCurrency fiatCurrency) {
                fragmentHomeBinding.setToCurrencyDetails(fiatCurrency);
            }
        });

        homeFragmentViewModel.getFromExchangeDetailsLiveData().observe(this, new Observer<Exchange>() {

            @Override
            public void onChanged(@Nullable Exchange exchange) {
                fragmentHomeBinding.setExchangeDetails(exchange);
            }
        });
    }

    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(fragmentHomeBinding.toolbar);
        FloatingActionButton btn = fragmentHomeBinding.optionLabels.findViewById(com.getbase.floatingactionbutton.R.id.fab_expand_menu_button);
        fragmentHomeBinding.fabEditOptions.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (fragmentHomeBinding.optionLabels.isExpanded()) {
                    fragmentHomeBinding.optionLabels.collapse();
                } else {
                    fragmentHomeBinding.optionLabels.expand();
                }
            }
        });
    }

    private void setupRecyclerView() {
        cryptoCurrencyPriceListAdapterAutoClearedValue = new AutoClearedValue<>(new CryptoCurrencyPriceListAdapter(fragmentBindingComponent, new CryptoCurrencyPriceListAdapter.CryptoCurrencyClickCallback() {

            @Override
            public void onItemClick(String id) {
//                Intent intent = new Intent(getActivity(), CryptoDetailsActivity.class);
//                intent.putExtra(CRYPTO_CURRENCY_ID, id);
//                startActivity(intent);
            }


        }), this);
        fragmentHomeBinding.rvFavoriteCryptoCurrencyList.setHasFixedSize(true);
        fragmentHomeBinding.rvFavoriteCryptoCurrencyList.setAdapter(cryptoCurrencyPriceListAdapterAutoClearedValue.get());
    }

    private void setupOptionsLayout() {
        fragmentHomeBinding.fabEditExchange.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showExchangeListDialogFragment();
                fragmentHomeBinding.optionLabels.collapse();
            }
        });
    }

    public void showExchangeListDialogFragment() {
        SelectExchangeDialogFragment selectExchangeDialogFragment = SelectExchangeDialogFragment.getInstance();
        selectExchangeDialogFragment.setTargetFragment(this, BundleAndRequestConstants.EXCHANGE_LIST_DIALOG_FRAGMENT_REQUEST_CODE);
        if (getFragmentManager() != null) {
            selectExchangeDialogFragment.show(getFragmentManager(), "SelectExchangeDialogFragment");
        }
    }
}
