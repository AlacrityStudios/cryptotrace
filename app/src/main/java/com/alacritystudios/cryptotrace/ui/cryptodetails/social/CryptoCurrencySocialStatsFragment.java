package com.alacritystudios.cryptotrace.ui.cryptodetails.social;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.CryptoCurrencySimilarItemsAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentCryptoCurrencySocialStatsBinding;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedSocialStatsDetails;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivityViewModel;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencySocialStatsFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoTraceViewModelFactory;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class CryptoCurrencySocialStatsFragment extends Fragment {

    private String cryptoCurrencyId;

    @Inject
    CryptoTraceViewModelFactory cryptoTraceViewModelFactory;

    FragmentBindingComponent fragmentBindingComponent;

    FragmentCryptoCurrencySocialStatsBinding fragmentCryptoCurrencySocialStatsBinding;

    CryptoCurrencySocialStatsFragmentViewModel cryptoCurrencySocialStatsFragmentViewModel;

    CryptoDetailsActivityViewModel cryptoDetailsActivityViewModel;

    AutoClearedValue<CryptoCurrencySimilarItemsAdapter> cryptoCurrencySimilarItemsAdapterAutoClearedValue;

    public CryptoCurrencySocialStatsFragment() {
        // Required empty public constructor
    }

    public static CryptoCurrencySocialStatsFragment newInstance() {
        CryptoCurrencySocialStatsFragment fragment = new CryptoCurrencySocialStatsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentCryptoCurrencySocialStatsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_crypto_currency_social_stats, container, false, fragmentBindingComponent);
        setupRecyclerView();
        return fragmentCryptoCurrencySocialStatsBinding.getRoot();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cryptoCurrencySocialStatsFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(CryptoCurrencySocialStatsFragmentViewModel.class);
        cryptoDetailsActivityViewModel = ViewModelProviders.of(getActivity(), cryptoTraceViewModelFactory).get(CryptoDetailsActivityViewModel.class);
        setupLiveDataListeners();
        setupViewModelInitialValues();
        setupSwipeRefreshLayout();
    }

    private void setupSwipeRefreshLayout() {
        fragmentCryptoCurrencySocialStatsBinding.srlCryptoCurrencySocialStats.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                cryptoCurrencySocialStatsFragmentViewModel.reloadData();
            }
        });
    }

    public void setupViewModelInitialValues() {
        if (cryptoDetailsActivityViewModel.getParamsLiveData().getValue() != null) {
            cryptoCurrencySocialStatsFragmentViewModel.setFromCurrencyLiveData(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().cryptoCurrency != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().cryptoCurrency.id : null);
        }
    }

    private void setupLiveDataListeners() {
        cryptoDetailsActivityViewModel.getParamsLiveData().observe(this, new Observer<CryptoDetailsActivityViewModel.CryptoCurrencyDetailsActivityParams>() {

            @Override
            public void onChanged(CryptoDetailsActivityViewModel.CryptoCurrencyDetailsActivityParams cryptoCurrencyDetailsActivityParams) {
                cryptoCurrencySocialStatsFragmentViewModel.setFromCurrencyLiveData(cryptoCurrencyDetailsActivityParams.cryptoCurrency != null ? cryptoCurrencyDetailsActivityParams.cryptoCurrency.id : null);
            }
        });
        cryptoCurrencySocialStatsFragmentViewModel.getResultLiveData().observe(this, new Observer<Resource<CryptoCurrencyFormattedSocialStatsDetails>>() {

            @Override
            public void onChanged(Resource<CryptoCurrencyFormattedSocialStatsDetails> cryptoCurrencyFormattedSocialStatsDetailsResource) {
                fragmentCryptoCurrencySocialStatsBinding.setCryptoCurrencySocialStats(cryptoCurrencyFormattedSocialStatsDetailsResource.data);
                cryptoCurrencySimilarItemsAdapterAutoClearedValue.get().setDataList(cryptoCurrencyFormattedSocialStatsDetailsResource.data != null ? cryptoCurrencyFormattedSocialStatsDetailsResource.data.similarItems : new ArrayList<CryptoCurrencyFormattedSocialStatsDetails.SimilarCryptoCurrency>());
                if (cryptoCurrencyFormattedSocialStatsDetailsResource.status == Resource.Status.LOADING) {
                    fragmentCryptoCurrencySocialStatsBinding.srlCryptoCurrencySocialStats.setRefreshing(true);
                } else if (cryptoCurrencyFormattedSocialStatsDetailsResource.status == Resource.Status.SUCCESS) {
                    fragmentCryptoCurrencySocialStatsBinding.srlCryptoCurrencySocialStats.setRefreshing(false);
                } else {
                    fragmentCryptoCurrencySocialStatsBinding.srlCryptoCurrencySocialStats.setRefreshing(false);
                    Snackbar.make(fragmentCryptoCurrencySocialStatsBinding.getRoot(), cryptoCurrencyFormattedSocialStatsDetailsResource.errorMessage != null ? cryptoCurrencyFormattedSocialStatsDetailsResource.errorMessage : "Unknown error. Please try again in some time.", 2000).show();
                }
            }
        });
    }

    private void setupRecyclerView() {
        cryptoCurrencySimilarItemsAdapterAutoClearedValue = new AutoClearedValue<>(new CryptoCurrencySimilarItemsAdapter(fragmentBindingComponent, new CryptoCurrencySimilarItemsAdapter.CryptoCurrencyClickCallback() {

            @Override
            public void onItemClick(String id) {

            }
        }), this);
        fragmentCryptoCurrencySocialStatsBinding.rvSimilarCryptoCurrencies.setHasFixedSize(true);
        fragmentCryptoCurrencySocialStatsBinding.rvSimilarCryptoCurrencies.setAdapter(cryptoCurrencySimilarItemsAdapterAutoClearedValue.get());
    }
}