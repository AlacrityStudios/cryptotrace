package com.alacritystudios.cryptotrace.ui.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.databinding.ActivityMainBinding;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabSelectListener;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    MainActivityNavigator mainActivityNavigator;

    ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setupBottomBar();
    }

    private void setupBottomBar() {
        for (int i = 0; i < activityMainBinding.bottomBar.getTabCount(); i++) {
            BottomBarTab tab = activityMainBinding.bottomBar.getTabAtPosition(i);
            View icon = tab.findViewById(com.roughike.bottombar.R.id.bb_bottom_bar_icon);
            ViewGroup.LayoutParams layoutParams = icon.getLayoutParams();
            layoutParams.height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());
            layoutParams.width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());
            icon.setLayoutParams(layoutParams);
        }

        activityMainBinding.bottomBar.setOnTabSelectListener(new OnTabSelectListener() {

            @Override
            public void onTabSelected(int tabId) {
                switch (tabId) {
                    case R.id.tab_home:
                        mainActivityNavigator.navigateToHomeFragment();
                        break;
                    case R.id.tab_crypto_currencies:
                        mainActivityNavigator.navigateToCryptoCurrencyListFragment();
                        break;
                    case R.id.tab_alerts:
                        mainActivityNavigator.navigateToAlertListFragment();
                        break;
                    case R.id.tab_news:
                        mainActivityNavigator.navigateToNewsListFragment();
                        break;
                }
            }
        });
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }
}