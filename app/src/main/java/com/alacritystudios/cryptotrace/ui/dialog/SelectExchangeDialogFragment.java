package com.alacritystudios.cryptotrace.ui.dialog;


import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.ExchangeListSelectionAdapter;
import com.alacritystudios.cryptotrace.adapter.ExchangeListAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.DialogFragmentSelectExchangeBinding;
import com.alacritystudios.cryptotrace.databinding.FragmentExchangeListBinding;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.addnewalert.AddNewAlertActivity;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivity;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivityViewModel;
import com.alacritystudios.cryptotrace.ui.main.home.HomeFragment;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.AddAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.AddNewAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.ExchangeListViewModel;
import com.alacritystudios.cryptotrace.viewmodel.HomeFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.SelectExchangeFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.Exchange;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * @author Anuj Dutt
 *         Fragment to display the list of exchanges trading crypto-currencies.
 */
public class SelectExchangeDialogFragment extends DialogFragment {

    @Inject
    ViewModelProvider.Factory cryptoTraceViewModelFactory;

    SelectExchangeFragmentViewModel selectExchangeFragmentViewModel;

    SelectParamsViewModelListener parentViewModel;

    FragmentBindingComponent fragmentBindingComponent;

    DialogFragmentSelectExchangeBinding dialogFragmentSelectExchangeBinding;

    AutoClearedValue<ExchangeListSelectionAdapter> exchangeListSelectionAdapterAutoClearedValue;

    public SelectExchangeDialogFragment() {
        // Required empty public constructor
    }

    public static SelectExchangeDialogFragment getInstance() {
        return new SelectExchangeDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        dialogFragmentSelectExchangeBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_select_exchange, container, false, fragmentBindingComponent);
        setupRecyclerViews();
        return dialogFragmentSelectExchangeBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        selectExchangeFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(SelectExchangeFragmentViewModel.class);
        if (getActivity() != null && getActivity() instanceof CryptoDetailsActivity) {
            parentViewModel = ViewModelProviders.of((CryptoDetailsActivity) getActivity(), cryptoTraceViewModelFactory).get(CryptoDetailsActivityViewModel.class);
        } else if (getActivity() != null && getActivity() instanceof AddNewAlertActivity) {
            parentViewModel = ViewModelProviders.of((AddNewAlertActivity) getActivity(), cryptoTraceViewModelFactory).get(AddNewAlertActivityViewModel.class);
        }
        initialiseTextWatcher();
        setupLiveDataListener();
        setupClickListeners();
        selectExchangeFragmentViewModel.setExchangeNameLiveData("");
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void setupClickListeners() {
        dialogFragmentSelectExchangeBinding.tvSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    private void initialiseTextWatcher() {
        dialogFragmentSelectExchangeBinding.etExchangeName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectExchangeFragmentViewModel.setExchangeNameLiveData(s.toString());
            }
        });
    }

    private void setupLiveDataListener() {
        selectExchangeFragmentViewModel.getExchangeListLiveData().observe(this, new Observer<Resource<List<Exchange>>>() {

            @Override
            public void onChanged(@Nullable Resource<List<Exchange>> listResource) {
                if (listResource != null && exchangeListSelectionAdapterAutoClearedValue.get() != null) {
                    if (listResource.status == Resource.Status.LOADING) {
                        dialogFragmentSelectExchangeBinding.srlSelectExchange.setRefreshing(true);
                        exchangeListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else if (listResource.status == Resource.Status.SUCCESS) {
                        dialogFragmentSelectExchangeBinding.srlSelectExchange.setRefreshing(false);
                        exchangeListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showContentLayout();
                    } else {
                        dialogFragmentSelectExchangeBinding.srlSelectExchange.setRefreshing(false);
                        exchangeListSelectionAdapterAutoClearedValue.get().setDataList(listResource.data);
                        showErrorLayout();
                    }
                } else {
                    dialogFragmentSelectExchangeBinding.srlSelectExchange.setRefreshing(false);
                    exchangeListSelectionAdapterAutoClearedValue.get().setDataList(new ArrayList<Exchange>());
                    showErrorLayout();
                }
            }
        });
    }


    private void setupRecyclerViews() {
        exchangeListSelectionAdapterAutoClearedValue = new AutoClearedValue<>(new ExchangeListSelectionAdapter(fragmentBindingComponent, new ExchangeListSelectionAdapter.ExchangeClickCallback() {

            @Override
            public void onItemClick(Exchange exchange) {
                parentViewModel.setExchange(exchange);
                getDialog().dismiss();
            }
        }), this);
        dialogFragmentSelectExchangeBinding.rvExchangeList.setHasFixedSize(true);
        dialogFragmentSelectExchangeBinding.rvExchangeList.setAdapter(exchangeListSelectionAdapterAutoClearedValue.get());
        dialogFragmentSelectExchangeBinding.srlSelectExchange.setEnabled(false);
    }

    private void showErrorLayout() {
        dialogFragmentSelectExchangeBinding.llToCurrencyPlaceholder.setVisibility(View.VISIBLE);
        dialogFragmentSelectExchangeBinding.rvExchangeList.setVisibility(View.GONE);
    }

    private void showContentLayout() {
        dialogFragmentSelectExchangeBinding.llToCurrencyPlaceholder.setVisibility(View.GONE);
        dialogFragmentSelectExchangeBinding.rvExchangeList.setVisibility(View.VISIBLE);
    }
}
