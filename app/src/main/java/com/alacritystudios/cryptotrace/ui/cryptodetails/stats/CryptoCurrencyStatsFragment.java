package com.alacritystudios.cryptotrace.ui.cryptodetails.stats;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapter.CryptoCurrencyStatsAdapter;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentCryptoCurrencyStatsBinding;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedStatsWrapper;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivityViewModel;
import com.alacritystudios.cryptotrace.ui.dialog.SelectExchangeDialogFragment;
import com.alacritystudios.cryptotrace.ui.dialog.SelectFiatCurrencyDialogFragment;
import com.alacritystudios.cryptotrace.util.AutoClearedValue;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyStatsFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoTraceViewModelFactory;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class CryptoCurrencyStatsFragment extends Fragment {

    @Inject
    CryptoTraceViewModelFactory cryptoTraceViewModelFactory;

    CryptoCurrencyStatsFragmentViewModel cryptoCurrencyStatsFragmentViewModel;

    CryptoDetailsActivityViewModel cryptoDetailsActivityViewModel;

    FragmentCryptoCurrencyStatsBinding fragmentCryptoCurrencyStatsBinding;

    FragmentBindingComponent fragmentBindingComponent;

    AutoClearedValue<CryptoCurrencyStatsAdapter> cryptoCurrencyStatsAdapterAutoClearedValue;

    public CryptoCurrencyStatsFragment() {
        // Required empty public constructor
    }

    public static CryptoCurrencyStatsFragment newInstance() {
        CryptoCurrencyStatsFragment fragment = new CryptoCurrencyStatsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidSupportInjection.inject(this);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentCryptoCurrencyStatsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_crypto_currency_stats, container, false, fragmentBindingComponent);
        setupRecyclerView();
        setupChartLayout();
        setupClickListeners();
        return fragmentCryptoCurrencyStatsBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cryptoCurrencyStatsFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(CryptoCurrencyStatsFragmentViewModel.class);
        cryptoDetailsActivityViewModel = ViewModelProviders.of(getActivity(), cryptoTraceViewModelFactory).get(CryptoDetailsActivityViewModel.class);
        setupLiveDataListeners();
        setupViewModelInitialValues();
        setupSwipeRefreshLayout();
    }

    private void setupClickListeners() {
        fragmentCryptoCurrencyStatsBinding.tv1h.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.setSelected(true);
                fragmentCryptoCurrencyStatsBinding.tv1d.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1m.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1y.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tvT.setSelected(false);
                cryptoCurrencyStatsFragmentViewModel.setHistogramChoice(CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice._1H);
            }
        });
        fragmentCryptoCurrencyStatsBinding.tv1d.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.setSelected(true);
                fragmentCryptoCurrencyStatsBinding.tv1h.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1m.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1y.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tvT.setSelected(false);
                cryptoCurrencyStatsFragmentViewModel.setHistogramChoice(CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice._1D);
            }
        });
        fragmentCryptoCurrencyStatsBinding.tv1m.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.setSelected(true);
                fragmentCryptoCurrencyStatsBinding.tv1d.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1h.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1y.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tvT.setSelected(false);
                cryptoCurrencyStatsFragmentViewModel.setHistogramChoice(CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice._1M);
            }
        });
        fragmentCryptoCurrencyStatsBinding.tv1y.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.setSelected(true);
                fragmentCryptoCurrencyStatsBinding.tv1d.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1m.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1h.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tvT.setSelected(false);
                cryptoCurrencyStatsFragmentViewModel.setHistogramChoice(CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice._1Y);
            }
        });
        fragmentCryptoCurrencyStatsBinding.tvT.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.setSelected(true);
                fragmentCryptoCurrencyStatsBinding.tv1d.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1m.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1y.setSelected(false);
                fragmentCryptoCurrencyStatsBinding.tv1h.setSelected(false);
                cryptoCurrencyStatsFragmentViewModel.setHistogramChoice(CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice._T);
            }
        });
        fragmentCryptoCurrencyStatsBinding.tvToCurrency.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SelectFiatCurrencyDialogFragment selectFiatCurrencyDialogFragment = SelectFiatCurrencyDialogFragment.getInstance();
                selectFiatCurrencyDialogFragment.show(getChildFragmentManager(), "SelectFiatCurrencyDialogFragment");
            }
        });
        fragmentCryptoCurrencyStatsBinding.tvExchange.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SelectExchangeDialogFragment selectExchangeDialogFragment = SelectExchangeDialogFragment.getInstance();
                selectExchangeDialogFragment.show(getChildFragmentManager(), "SelectExchangeDialogFragment");
            }
        });
    }

    /**
     * Sets up the chart layout.
     */
    private void setupChartLayout() {
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisLeft().setEnabled(false);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisRight().setDrawLabels(true);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisRight().setLabelCount(6, true);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisRight().setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisRight().setDrawGridLines(true);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisRight().setGridColor(ActivityCompat.getColor(getActivity(), R.color.colorGrey40));
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisRight().enableGridDashedLine(4, 2, 2);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisRight().setDrawAxisLine(false);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisRight().setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorGrey80));
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getAxisRight().setTextSize(8f);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getXAxis().setDrawGridLines(false);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getXAxis().setDrawAxisLine(false);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getXAxis().setDrawLabels(true);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getXAxis().setLabelCount(6, true);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getXAxis().setAvoidFirstLastClipping(false);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getXAxis().setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorGrey80));
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getXAxis().setTextSize(8f);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.setDescription(null);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getLegend().setEnabled(false);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.setTouchEnabled(false);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.setPinchZoom(true);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.setNoDataText("");
    }

    private void setupSwipeRefreshLayout() {
        fragmentCryptoCurrencyStatsBinding.srlCryptoCurrencyStats.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                cryptoCurrencyStatsFragmentViewModel.reloadData();
            }
        });
    }

    private void setupRecyclerView() {
        cryptoCurrencyStatsAdapterAutoClearedValue = new AutoClearedValue<>(new CryptoCurrencyStatsAdapter(), this);
        fragmentCryptoCurrencyStatsBinding.rvCryptoCurrencyStats.setHasFixedSize(true);
        fragmentCryptoCurrencyStatsBinding.rvCryptoCurrencyStats.setAdapter(cryptoCurrencyStatsAdapterAutoClearedValue.get());
    }

    public void setupViewModelInitialValues() {
        fragmentCryptoCurrencyStatsBinding.tv1h.performClick();
        if (cryptoDetailsActivityViewModel.getParamsLiveData().getValue() != null) {
            cryptoCurrencyStatsFragmentViewModel.setExchangeLiveData(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().exchange != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().exchange.internalName : null);
            cryptoCurrencyStatsFragmentViewModel.setToCurrencyLiveData(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().fiatCurrency != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().fiatCurrency.name : null);
            cryptoCurrencyStatsFragmentViewModel.setFromCurrencyLiveData(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().cryptoCurrency != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().cryptoCurrency.symbol : null);
            fragmentCryptoCurrencyStatsBinding.tvExchange.setText(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().exchange != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().exchange.name : null);
            fragmentCryptoCurrencyStatsBinding.tvToCurrency.setText(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().fiatCurrency != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().fiatCurrency.coinName : null);
        }
    }

    private void setupLiveDataListeners() {
        cryptoDetailsActivityViewModel.getParamsLiveData().observe(this, new Observer<CryptoDetailsActivityViewModel.CryptoCurrencyDetailsActivityParams>() {

            @Override
            public void onChanged(CryptoDetailsActivityViewModel.CryptoCurrencyDetailsActivityParams cryptoCurrencyDetailsActivityParams) {
                cryptoCurrencyStatsFragmentViewModel.setExchangeLiveData(cryptoCurrencyDetailsActivityParams.exchange != null ? cryptoCurrencyDetailsActivityParams.exchange.internalName : null);
                cryptoCurrencyStatsFragmentViewModel.setToCurrencyLiveData(cryptoCurrencyDetailsActivityParams.fiatCurrency != null ? cryptoCurrencyDetailsActivityParams.fiatCurrency.name : null);
                cryptoCurrencyStatsFragmentViewModel.setFromCurrencyLiveData(cryptoCurrencyDetailsActivityParams.cryptoCurrency != null ? cryptoCurrencyDetailsActivityParams.cryptoCurrency.symbol : null);
                fragmentCryptoCurrencyStatsBinding.tvExchange.setText(cryptoCurrencyDetailsActivityParams.exchange != null ? cryptoCurrencyDetailsActivityParams.exchange.name : null);
                fragmentCryptoCurrencyStatsBinding.tvToCurrency.setText(cryptoCurrencyDetailsActivityParams.fiatCurrency != null ? cryptoCurrencyDetailsActivityParams.fiatCurrency.coinName : null);
            }
        });

        cryptoCurrencyStatsFragmentViewModel.getResourceLiveData().observe(this, new Observer<Resource<CryptoCurrencyFormattedStatsWrapper>>() {

            @Override
            public void onChanged(Resource<CryptoCurrencyFormattedStatsWrapper> cryptoCurrencyFormattedStatsWrapperResource) {
                if (cryptoCurrencyFormattedStatsWrapperResource.status == Resource.Status.LOADING) {
                    fragmentCryptoCurrencyStatsBinding.srlCryptoCurrencyStats.setRefreshing(true);
                    cryptoCurrencyStatsAdapterAutoClearedValue.get().setCryptoCurrencyFormattedStats(null);
                    fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.clear();
                } else if (cryptoCurrencyFormattedStatsWrapperResource.status == Resource.Status.SUCCESS) {
                    fragmentCryptoCurrencyStatsBinding.srlCryptoCurrencyStats.setRefreshing(false);
                    cryptoCurrencyStatsAdapterAutoClearedValue.get().setCryptoCurrencyFormattedStats(cryptoCurrencyFormattedStatsWrapperResource.data != null ? cryptoCurrencyFormattedStatsWrapperResource.data.cryptoCurrencyFormattedStatsList : new ArrayList<CryptoCurrencyFormattedStatsWrapper.CryptoCurrencyFormattedStats>());
                    receiveCurrencyHistogram(cryptoCurrencyFormattedStatsWrapperResource.data != null ? cryptoCurrencyFormattedStatsWrapperResource.data.cryptoCurrencyFormattedStatsEntryList : new ArrayList<Entry>());
                } else {
                    fragmentCryptoCurrencyStatsBinding.srlCryptoCurrencyStats.setRefreshing(false);
                    cryptoCurrencyStatsAdapterAutoClearedValue.get().setCryptoCurrencyFormattedStats(null);
                    fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.clear();
                    Snackbar.make(fragmentCryptoCurrencyStatsBinding.getRoot(), cryptoCurrencyFormattedStatsWrapperResource.errorMessage != null ? cryptoCurrencyFormattedStatsWrapperResource.errorMessage : "Unknown error. Please try again in some time.", 2000).show();
                }
            }
        });
    }

    public void receiveCurrencyHistogram(List<Entry> entryList) {
        LineDataSet lineDataSet = new LineDataSet(entryList, "");
        lineDataSet.setColors(new int[]{R.color.colorAccent}, getActivity());
        lineDataSet.setDrawFilled(true);
        lineDataSet.setDrawCircles(false);
        lineDataSet.setLineWidth(1.5F);
        lineDataSet.setValueTypeface(ResourcesCompat.getFont(getActivity(), R.font.open_sans));
        LineData lineData = new LineData(lineDataSet);
        lineData.setDrawValues(false);

        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.getXAxis().setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                switch (cryptoCurrencyStatsFragmentViewModel.getHistogramChoice()) {
                    case _1H:
                        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date((long) value * 1000));
                    case _1D:
                        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date((long) value * 1000));
                    case _1M:
                        return new SimpleDateFormat("dd MMM", Locale.getDefault()).format(new Date((long) value * 1000));
                    case _1Y:
                        return new SimpleDateFormat("dd MMM yy", Locale.getDefault()).format(new Date((long) value * 1000));
                    case _T:
                        return new SimpleDateFormat("dd MMM yy", Locale.getDefault()).format(new Date((long) value * 1000));
                    default:
                        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date((long) value * 1000));
                }
            }
        });
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.setData(lineData);
        fragmentCryptoCurrencyStatsBinding.lcCurrencyStats.animateX(1000);
    }
}
