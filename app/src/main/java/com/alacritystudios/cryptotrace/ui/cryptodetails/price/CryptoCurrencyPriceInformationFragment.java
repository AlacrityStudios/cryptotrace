package com.alacritystudios.cryptotrace.ui.cryptodetails.price;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.binding.FragmentBindingComponent;
import com.alacritystudios.cryptotrace.databinding.FragmentCryptoCurrencyPriceInformationBinding;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedPriceDetails;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.cryptodetails.CryptoDetailsActivityViewModel;
import com.alacritystudios.cryptotrace.ui.dialog.SelectExchangeDialogFragment;
import com.alacritystudios.cryptotrace.ui.dialog.SelectFiatCurrencyDialogFragment;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyPriceFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyStatsFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoTraceViewModelFactory;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 *
 */
public class CryptoCurrencyPriceInformationFragment extends Fragment {

    private String cryptoCurrencyId;

    @Inject
    CryptoTraceViewModelFactory cryptoTraceViewModelFactory;

    CryptoCurrencyPriceFragmentViewModel cryptoCurrencyPriceFragmentViewModel;

    CryptoDetailsActivityViewModel cryptoDetailsActivityViewModel;

    FragmentBindingComponent fragmentBindingComponent;

    FragmentCryptoCurrencyPriceInformationBinding fragmentCryptoCurrencyPriceBinding;

    public CryptoCurrencyPriceInformationFragment() {
        // Required empty public constructor
    }

    public static CryptoCurrencyPriceInformationFragment newInstance() {
        return new CryptoCurrencyPriceInformationFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBindingComponent = new FragmentBindingComponent(this);
        fragmentCryptoCurrencyPriceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_crypto_currency_price_information, container, false, fragmentBindingComponent);
        setupClickListeners();
        return fragmentCryptoCurrencyPriceBinding.getRoot();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cryptoCurrencyPriceFragmentViewModel = ViewModelProviders.of(this, cryptoTraceViewModelFactory).get(CryptoCurrencyPriceFragmentViewModel.class);
        cryptoDetailsActivityViewModel = ViewModelProviders.of(getActivity(), cryptoTraceViewModelFactory).get(CryptoDetailsActivityViewModel.class);
        setupLiveDataListeners();
        setupViewModelInitialValues();
        setupSwipeRefreshLayout();
    }

    public void setupViewModelInitialValues() {
        if(cryptoDetailsActivityViewModel.getParamsLiveData().getValue() != null) {
            cryptoCurrencyPriceFragmentViewModel.setExchangeLiveData(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().exchange != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().exchange.internalName : null);
            cryptoCurrencyPriceFragmentViewModel.setToCurrencyLiveData(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().fiatCurrency != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().fiatCurrency.name : null);
            cryptoCurrencyPriceFragmentViewModel.setFromCurrencyLiveData(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().cryptoCurrency != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().cryptoCurrency.symbol : null);
            fragmentCryptoCurrencyPriceBinding.tvExchange.setText(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().exchange != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().exchange.name : "");
            fragmentCryptoCurrencyPriceBinding.tvToCurrency.setText(cryptoDetailsActivityViewModel.getParamsLiveData().getValue().fiatCurrency != null ? cryptoDetailsActivityViewModel.getParamsLiveData().getValue().fiatCurrency.coinName : "");
        }
    }

    private void setupLiveDataListeners() {
        cryptoDetailsActivityViewModel.getParamsLiveData().observe(this, new Observer<CryptoDetailsActivityViewModel.CryptoCurrencyDetailsActivityParams>() {

            @Override
            public void onChanged(CryptoDetailsActivityViewModel.CryptoCurrencyDetailsActivityParams cryptoCurrencyDetailsActivityParams) {
                cryptoCurrencyPriceFragmentViewModel.setExchangeLiveData(cryptoCurrencyDetailsActivityParams.exchange != null ? cryptoCurrencyDetailsActivityParams.exchange.internalName : null);
                cryptoCurrencyPriceFragmentViewModel.setToCurrencyLiveData(cryptoCurrencyDetailsActivityParams.fiatCurrency != null ? cryptoCurrencyDetailsActivityParams.fiatCurrency.name : null);
                cryptoCurrencyPriceFragmentViewModel.setFromCurrencyLiveData(cryptoCurrencyDetailsActivityParams.cryptoCurrency != null ? cryptoCurrencyDetailsActivityParams.cryptoCurrency.symbol : null);
                fragmentCryptoCurrencyPriceBinding.tvExchange.setText(cryptoCurrencyDetailsActivityParams.exchange != null ? cryptoCurrencyDetailsActivityParams.exchange.name : "");
                fragmentCryptoCurrencyPriceBinding.tvToCurrency.setText(cryptoCurrencyDetailsActivityParams.fiatCurrency != null ? cryptoCurrencyDetailsActivityParams.fiatCurrency.coinName : "");
            }
        });
        cryptoCurrencyPriceFragmentViewModel.getResourceLiveData().observe(this, new Observer<Resource<CryptoCurrencyFormattedPriceDetails>>() {

            @Override
            public void onChanged(Resource<CryptoCurrencyFormattedPriceDetails> cryptoCurrencyFormattedPriceResource) {
                if (cryptoCurrencyFormattedPriceResource.status == Resource.Status.LOADING) {
                    fragmentCryptoCurrencyPriceBinding.srlCryptoCurrencyConverter.setRefreshing(true);
                    fragmentCryptoCurrencyPriceBinding.setCryptoCurrencyPrice(cryptoCurrencyFormattedPriceResource.data);
                } else if (cryptoCurrencyFormattedPriceResource.status == Resource.Status.SUCCESS) {
                    fragmentCryptoCurrencyPriceBinding.srlCryptoCurrencyConverter.setRefreshing(false);
                    fragmentCryptoCurrencyPriceBinding.setCryptoCurrencyPrice(cryptoCurrencyFormattedPriceResource.data);
                } else {
                    fragmentCryptoCurrencyPriceBinding.srlCryptoCurrencyConverter.setRefreshing(false);
                    fragmentCryptoCurrencyPriceBinding.setCryptoCurrencyPrice(cryptoCurrencyFormattedPriceResource.data);
                    Snackbar.make(fragmentCryptoCurrencyPriceBinding.getRoot(), cryptoCurrencyFormattedPriceResource.errorMessage != null? cryptoCurrencyFormattedPriceResource.errorMessage : "Unknown error. Please try again in some time.", 2000).show();
                }
            }
        });
    }

    private void setupClickListeners() {
        fragmentCryptoCurrencyPriceBinding.tvToCurrency.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SelectFiatCurrencyDialogFragment selectFiatCurrencyDialogFragment = SelectFiatCurrencyDialogFragment.getInstance();
                selectFiatCurrencyDialogFragment.show(getChildFragmentManager(), "SelectFiatCurrencyDialogFragment");
            }
        });
        fragmentCryptoCurrencyPriceBinding.tvExchange.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SelectExchangeDialogFragment selectExchangeDialogFragment = SelectExchangeDialogFragment.getInstance();
                selectExchangeDialogFragment.show(getChildFragmentManager(), "SelectExchangeDialogFragment");
            }
        });
    }

    private void setupSwipeRefreshLayout() {
        fragmentCryptoCurrencyPriceBinding.srlCryptoCurrencyConverter.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                cryptoCurrencyPriceFragmentViewModel.reloadData();
            }
        });
    }
}
