package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.repository.NewsRepository;
import com.alacritystudios.cryptotrace.resource.Listing;
import com.alacritystudios.cryptotrace.viewobject.News;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         ViewModel to display data on NewsList Fragment.
 */

public class NewsListFragmentViewModel extends ViewModel {

    private Listing<News> results;
    NewsRepository newsRepository;

    @Inject
    public NewsListFragmentViewModel(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
        fetchNewsData();
    }

    public Listing<News> getResults() {
        return results;
    }

    public void fetchNewsData() {
        results = newsRepository.fetchNewsList(0L);
    }

    public void refreshNewsData() {
        results.refresh();
    }
}
