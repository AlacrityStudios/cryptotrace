package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.FavoriteCryptoCurrency;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         ViewModel object to display the list of crypto-currencies in CryptoCurrencyListFragment.
 */

public class CryptoCurrencyListViewModel extends ViewModel {

    public CryptoCurrencyRepository cryptoCurrencyRepository;

    private LiveData<Resource<List<CryptoCurrency>>> results;

    private MutableLiveData<Integer> reloadCount = new MutableLiveData<>();

    private LiveData<List<FavoriteCryptoCurrency>> favoriteCryptoCurrencyList;

    @Inject
    public CryptoCurrencyListViewModel(CryptoCurrencyRepository repository) {
        cryptoCurrencyRepository = repository;
        reloadCount.setValue(0);
        favoriteCryptoCurrencyList = cryptoCurrencyRepository.fetchFavoriteCryptoCurrencies();
        results = Transformations.switchMap(favoriteCryptoCurrencyList, new Function<List<FavoriteCryptoCurrency>, LiveData<Resource<List<CryptoCurrency>>>>() {

            @Override
            public LiveData<Resource<List<CryptoCurrency>>> apply(List<FavoriteCryptoCurrency> input) {
                return cryptoCurrencyRepository.updateAndFetchCryptoCurrencyList();
            }
        });

        results = Transformations.switchMap(reloadCount, new Function<Integer, LiveData<Resource<List<CryptoCurrency>>>>() {

            @Override
            public LiveData<Resource<List<CryptoCurrency>>> apply(Integer input) {
                return cryptoCurrencyRepository.updateAndFetchCryptoCurrencyList();
            }
        });
    }

    public LiveData<Resource<List<CryptoCurrency>>> getResults() {
        return results;
    }

    public void invertCryptoCurrencyStatusInFavorites(CryptoCurrency cryptoCurrency, boolean shouldAdd) {

        FavoriteCryptoCurrency favoriteCryptoCurrency = new FavoriteCryptoCurrency();
        favoriteCryptoCurrency.cryptoCurrencyId = cryptoCurrency.id;
        if (shouldAdd) {
            cryptoCurrencyRepository.addCryptoCurrencyToFavoritesList(favoriteCryptoCurrency);
        } else {
            cryptoCurrencyRepository.deleteCryptoCurrencyFromFavoritesList(favoriteCryptoCurrency);
        }
    }

    public void incrementReloadCount() {
        reloadCount.setValue((reloadCount.getValue() != null ? reloadCount.getValue(): 0) + 1);
    }
}
