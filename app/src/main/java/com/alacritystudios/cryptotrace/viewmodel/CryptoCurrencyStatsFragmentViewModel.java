package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedStatsWrapper;
import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         ViewModel for fragment showing stats of crypto-currrency.
 */

public class CryptoCurrencyStatsFragmentViewModel extends ViewModel {

    private LiveData<Resource<CryptoCurrencyFormattedStatsWrapper>> resourceLiveData;

    private final MutableLiveData<CryptoCurrencyStatsFragmentParams> cryptoCurrencyStatsFragmentParamsMutableLiveData;


    @Inject
    public CryptoCurrencyStatsFragmentViewModel(CryptoCurrencyRepository cryptoCurrencyRepository) {
        cryptoCurrencyStatsFragmentParamsMutableLiveData = new MutableLiveData<>();
        cryptoCurrencyStatsFragmentParamsMutableLiveData.setValue(CryptoCurrencyStatsFragmentParams.getInstance());
        setupDataRelationships(cryptoCurrencyRepository);
    }

    public void setupDataRelationships(final CryptoCurrencyRepository cryptoCurrencyRepository) {
        resourceLiveData = Transformations.switchMap(cryptoCurrencyStatsFragmentParamsMutableLiveData, new Function<CryptoCurrencyStatsFragmentParams, LiveData<Resource<CryptoCurrencyFormattedStatsWrapper>>>() {

            @Override
            public LiveData<Resource<CryptoCurrencyFormattedStatsWrapper>> apply(CryptoCurrencyStatsFragmentParams input) {
                switch (input.choice) {
                    case _1H:
                        return cryptoCurrencyRepository.fetchCryptoCurrencyStatsByMinute(input, 60);
                    case _1D:
                        return cryptoCurrencyRepository.fetchCryptoCurrencyStatsByHour(input, 24);
                    case _1M:
                        return cryptoCurrencyRepository.fetchCryptoCurrencyStatsByDay(input, 30);
                    case _1Y:
                        return cryptoCurrencyRepository.fetchCryptoCurrencyStatsByDay(input, 365);
                    case _T:
                        return cryptoCurrencyRepository.fetchCryptoCurrencyStatsByDay(input, 2000);
                    default:
                        return cryptoCurrencyRepository.fetchCryptoCurrencyStatsByMinute(input, 60);
                }
            }
        });
    }

    public void setToCurrencyLiveData(String toCurrency) {
        CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams = cryptoCurrencyStatsFragmentParamsMutableLiveData.getValue();
        if (cryptoCurrencyStatsFragmentParams != null) {
            cryptoCurrencyStatsFragmentParams.toCurrency = toCurrency;
        }
        cryptoCurrencyStatsFragmentParamsMutableLiveData.setValue(cryptoCurrencyStatsFragmentParams);
    }

    public void setFromCurrencyLiveData(String fromCurrency) {
        CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams = cryptoCurrencyStatsFragmentParamsMutableLiveData.getValue();
        if (cryptoCurrencyStatsFragmentParams != null) {
            cryptoCurrencyStatsFragmentParams.fromCurrency = fromCurrency;
        }
        cryptoCurrencyStatsFragmentParamsMutableLiveData.setValue(cryptoCurrencyStatsFragmentParams);
    }

    public void setExchangeLiveData(String exchange) {
        CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams = cryptoCurrencyStatsFragmentParamsMutableLiveData.getValue();
        if (cryptoCurrencyStatsFragmentParams != null) {
            cryptoCurrencyStatsFragmentParams.exchange = exchange;
        }
        cryptoCurrencyStatsFragmentParamsMutableLiveData.setValue(cryptoCurrencyStatsFragmentParams);
    }

    public void setHistogramChoice(CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice choice) {
        CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams = cryptoCurrencyStatsFragmentParamsMutableLiveData.getValue();
        if (cryptoCurrencyStatsFragmentParams != null) {
            cryptoCurrencyStatsFragmentParams.choice = choice;
        }
        cryptoCurrencyStatsFragmentParamsMutableLiveData.setValue(cryptoCurrencyStatsFragmentParams);
    }

    public LiveData<Resource<CryptoCurrencyFormattedStatsWrapper>> getResourceLiveData() {
        return resourceLiveData;
    }

    public CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice getHistogramChoice() {
        CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams = cryptoCurrencyStatsFragmentParamsMutableLiveData.getValue();
        if (cryptoCurrencyStatsFragmentParams != null) {
            return cryptoCurrencyStatsFragmentParams.choice;
        } else {
            return CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice._1H;
        }
    }

    public void reloadData() {
        CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams = cryptoCurrencyStatsFragmentParamsMutableLiveData.getValue();
        cryptoCurrencyStatsFragmentParamsMutableLiveData.setValue(cryptoCurrencyStatsFragmentParams);
    }

    public static class CryptoCurrencyStatsFragmentParams {

        public String fromCurrency;
        public String toCurrency;
        public String exchange;
        public CryptoCurrencyHistogramChoice choice;

        public static CryptoCurrencyStatsFragmentParams getInstance() {
            CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams = new CryptoCurrencyStatsFragmentParams();
            cryptoCurrencyStatsFragmentParams.choice = CryptoCurrencyHistogramChoice._1H;
            return cryptoCurrencyStatsFragmentParams;
        }

        public CryptoCurrencyStatsFragmentParamsStatus getStatusOfParams() {
            if (fromCurrency != null && toCurrency != null && exchange != null && fromCurrency.length() > 0 && toCurrency.length() > 0 && exchange.length() > 0 && choice != null) {
                return CryptoCurrencyStatsFragmentParamsStatus.READY;
            } else {
                return CryptoCurrencyStatsFragmentParamsStatus.NOT_READY;
            }
        }

        public enum CryptoCurrencyStatsFragmentParamsStatus {

            READY,
            NOT_READY
        }

        public enum CryptoCurrencyHistogramChoice {

            _1H,
            _1D,
            _1M,
            _1Y,
            _T
        }
    }
}
