package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.repository.ExchangeRepository;
import com.alacritystudios.cryptotrace.repository.FiatCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         Viewmodel to process data for adding new alert.
 */

public class AddAlertFragmentViewModel extends ViewModel {

    private MutableLiveData<String> fromCurrencyNameLiveData;
    private MutableLiveData<String> toCurrencyNameLiveData;
    private MutableLiveData<String> exchangeNameLiveData;
    private MutableLiveData<AddAlertFragmentQueryParams> params;

    private LiveData<Resource<List<CryptoCurrency>>> cryptoCurrencyListLiveData;
    private LiveData<Resource<List<FiatCurrency>>> toCurrencyListLiveData;
    private LiveData<Resource<List<Exchange>>> exchangeListLiveData;

    @Inject
    public AddAlertFragmentViewModel(CryptoCurrencyRepository cryptoCurrencyRepository, FiatCurrencyRepository fiatCurrencyRepository, ExchangeRepository exchangeRepository) {
        params = new MutableLiveData<>();
        params.setValue(new AddAlertFragmentQueryParams());
        fromCurrencyNameLiveData = new MutableLiveData<>();
        toCurrencyNameLiveData = new MutableLiveData<>();
        exchangeNameLiveData = new MutableLiveData<>();
        setOptionsDependencies(cryptoCurrencyRepository, fiatCurrencyRepository, exchangeRepository);
    }

    private void setOptionsDependencies(final CryptoCurrencyRepository cryptoCurrencyRepository, final FiatCurrencyRepository fiatCurrencyRepository, final ExchangeRepository exchangeRepository) {
        cryptoCurrencyListLiveData = Transformations.switchMap(fromCurrencyNameLiveData, new Function<String, LiveData<Resource<List<CryptoCurrency>>>>() {

            @Override
            public LiveData<Resource<List<CryptoCurrency>>> apply(String input) {
                return cryptoCurrencyRepository.fetchSimpleCryptoCurrencyListByFullName(input);
            }
        });

        toCurrencyListLiveData = Transformations.switchMap(toCurrencyNameLiveData, new Function<String, LiveData<Resource<List<FiatCurrency>>>>() {

            @Override
            public LiveData<Resource<List<FiatCurrency>>> apply(String input) {
                return fiatCurrencyRepository.fetchSimpleFiatCurrencyListByFiatCurrencyName(input);
            }
        });

        exchangeListLiveData = Transformations.switchMap(exchangeNameLiveData, new Function<String, LiveData<Resource<List<Exchange>>>>() {

            @Override
            public LiveData<Resource<List<Exchange>>> apply(String input) {
                return exchangeRepository.getExchangeListByExchangeName(input);
            }
        });
    }

    public static class AddAlertFragmentQueryParams {

        public CryptoCurrency cryptoCurrency;
        public FiatCurrency fiatCurrency;
        public Exchange exchange;

        public enum AddAlertFragmentQueryParamsStatus {
            READY,
            NOT_READY
        }

        public AddAlertFragmentQueryParamsStatus getQueryParamsStatus() {
            if (cryptoCurrency == null || fiatCurrency == null || exchange == null) {
                return AddAlertFragmentQueryParamsStatus.NOT_READY;
            } else {
                return AddAlertFragmentQueryParamsStatus.READY;
            }
        }
    }

    public void setToCurrencyNameLiveData(String toCurrencyName) {
        toCurrencyNameLiveData.setValue(toCurrencyName);
    }

    public void setFromCurrencyNameLiveData(String fromCurrencyName) {
        fromCurrencyNameLiveData.setValue(fromCurrencyName);
    }

    public void setExchangeNameLiveData(String exchangeName) {
        exchangeNameLiveData.setValue(exchangeName);
    }

    public void setFromCurrency(CryptoCurrency fromCurrency) {
        AddAlertFragmentQueryParams addAlertFragmentQueryParams = params.getValue();
        if (addAlertFragmentQueryParams != null) {
            addAlertFragmentQueryParams.cryptoCurrency = fromCurrency;
            params.setValue(addAlertFragmentQueryParams);
        }
    }

    public void setToCurrency(FiatCurrency toCurrency) {
        AddAlertFragmentQueryParams addAlertFragmentQueryParams = params.getValue();
        if (addAlertFragmentQueryParams != null) {
            addAlertFragmentQueryParams.fiatCurrency = toCurrency;
            params.setValue(addAlertFragmentQueryParams);
        }
    }

    public void setExchange(Exchange exchange) {
        AddAlertFragmentQueryParams addAlertFragmentQueryParams = params.getValue();
        if (addAlertFragmentQueryParams != null) {
            addAlertFragmentQueryParams.exchange = exchange;
            params.setValue(addAlertFragmentQueryParams);
        }
    }

    public MutableLiveData<AddAlertFragmentQueryParams> getParams() {
        return params;
    }

    public LiveData<Resource<List<CryptoCurrency>>> getCryptoCurrencyListLiveData() {
        return cryptoCurrencyListLiveData;
    }

    public LiveData<Resource<List<FiatCurrency>>> getToCurrencyListLiveData() {
        return toCurrencyListLiveData;
    }

    public LiveData<Resource<List<Exchange>>> getExchangeListLiveData() {
        return exchangeListLiveData;
    }

    public MutableLiveData<String> getFromCurrencyNameLiveData() {
        return fromCurrencyNameLiveData;
    }

    public MutableLiveData<String> getToCurrencyNameLiveData() {
        return toCurrencyNameLiveData;
    }

    public MutableLiveData<String> getExchangeNameLiveData() {
        return exchangeNameLiveData;
    }
}
