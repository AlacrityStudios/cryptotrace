package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.repository.AlertRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewobject.Alert;
import com.alacritystudios.cryptotrace.viewobject.AlertDisplayDetails;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         A viewmodel to display the list of the cryptocurrency coins.
 */

public class AlertListFragmentViewModel extends ViewModel {

    private LiveData<Resource<List<AlertDisplayDetails>>> results;

    private MutableLiveData<Integer> reloadCount = new MutableLiveData<>();

    @Inject
    public AlertListFragmentViewModel(final AlertRepository repository) {
        results = Transformations.switchMap(reloadCount, new Function<Integer, LiveData<Resource<List<AlertDisplayDetails>>>>() {

            @Override
            public LiveData<Resource<List<AlertDisplayDetails>>> apply(Integer input) {
                return repository.fetchAlertDetailsList();
            }
        });
        reloadCount.setValue(0);
    }

    public LiveData<Resource<List<AlertDisplayDetails>>> getResults() {
        return results;
    }

    public void incrementReloadCount() {
        reloadCount.setValue((reloadCount.getValue() != null ? reloadCount.getValue() : 0) + 1);
    }
}
