package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.repository.AlertRepository;
import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.ui.dialog.SelectParamsViewModelListener;
import com.alacritystudios.cryptotrace.viewobject.Alert;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import javax.inject.Inject;

/**
 * A view model to create new alert.
 */

public class AddNewAlertActivityViewModel extends ViewModel implements SelectParamsViewModelListener {

    private AlertRepository alertRepository;
    private MutableLiveData<AddNewAlertActivityQueryParams> paramsLiveData;
    private LiveData<Resource<Double>> priceLiveData;

    @Inject
    public AddNewAlertActivityViewModel(CryptoCurrencyRepository cryptoCurrencyRepository, AlertRepository alertRepository) {
        this.alertRepository = alertRepository;
        paramsLiveData = new MutableLiveData<>();
        paramsLiveData.setValue(new AddNewAlertActivityQueryParams());
        setupParamsDependencies(cryptoCurrencyRepository);
    }

    private void setupParamsDependencies(final CryptoCurrencyRepository cryptoCurrencyRepository) {
        priceLiveData = Transformations.switchMap(paramsLiveData, new Function<AddNewAlertActivityQueryParams, LiveData<Resource<Double>>>() {

            @Override
            public LiveData<Resource<Double>> apply(AddNewAlertActivityQueryParams input) {
                return cryptoCurrencyRepository.fetchCryptoCurrencyPriceForAlert(input);
            }
        });
    }

    @Override
    public void setFromCurrency(CryptoCurrency cryptoCurrency) {
        AddNewAlertActivityQueryParams addNewAlertActivityQueryParams = paramsLiveData.getValue();
        if (addNewAlertActivityQueryParams != null) {
            addNewAlertActivityQueryParams.cryptoCurrency = cryptoCurrency;
            paramsLiveData.setValue(addNewAlertActivityQueryParams);
        }
    }

    @Override
    public void setToCurrency(FiatCurrency fiatCurrency) {
        AddNewAlertActivityQueryParams addNewAlertActivityQueryParams = paramsLiveData.getValue();
        if (addNewAlertActivityQueryParams != null) {
            addNewAlertActivityQueryParams.fiatCurrency = fiatCurrency;
            paramsLiveData.setValue(addNewAlertActivityQueryParams);
        }
    }

    @Override
    public void setExchange(Exchange exchange) {
        AddNewAlertActivityQueryParams addNewAlertActivityQueryParams = paramsLiveData.getValue();
        if (addNewAlertActivityQueryParams != null) {
            addNewAlertActivityQueryParams.exchange = exchange;
            paramsLiveData.setValue(addNewAlertActivityQueryParams);
        }
    }

    public void retryPriceFetch() {
        paramsLiveData.setValue(paramsLiveData.getValue());
    }

    public MutableLiveData<AddNewAlertActivityQueryParams> getParamsLiveData() {
        return paramsLiveData;
    }

    public LiveData<Resource<Double>> getPriceLiveData() {
        return priceLiveData;
    }

    public void saveAlert(Double alertPrice) {
        if (paramsLiveData.getValue() != null && priceLiveData.getValue() != null) {
            alertRepository.saveNewAlert(new Alert(paramsLiveData.getValue().cryptoCurrency.id,
                    paramsLiveData.getValue().fiatCurrency.id, paramsLiveData.getValue().exchange.id,
                    priceLiveData.getValue().data, alertPrice));
        }
    }

    public static class AddNewAlertActivityQueryParams {

        public CryptoCurrency cryptoCurrency;
        public FiatCurrency fiatCurrency;
        public Exchange exchange;

        public enum AddNewAlertActivityQueryParamsStatus {
            READY,
            NOT_READY
        }

        public AddNewAlertActivityViewModel.AddNewAlertActivityQueryParams.AddNewAlertActivityQueryParamsStatus getQueryParamsStatus() {
            if (cryptoCurrency != null && fiatCurrency != null && exchange != null) {
                return AddNewAlertActivityQueryParamsStatus.READY;
            } else {
                return AddNewAlertActivityQueryParamsStatus.NOT_READY;
            }
        }
    }
}
