package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.lifecycle.ViewModel;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         A view model to assist in selecting the price at which to create alerts for.
 */

public class SelectPriceFragmentViewModel extends ViewModel {


    @Inject
    public SelectPriceFragmentViewModel() {
    }
}
