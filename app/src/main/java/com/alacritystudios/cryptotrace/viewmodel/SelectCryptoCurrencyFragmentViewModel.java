package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *      A viewmodel class to display a list of cryptocurrencies and select from them.
 */

public class SelectCryptoCurrencyFragmentViewModel extends ViewModel {

    private MutableLiveData<String> fromCurrencyNameLiveData;
    private LiveData<Resource<List<CryptoCurrency>>> cryptoCurrencyListLiveData;

    @Inject
    public SelectCryptoCurrencyFragmentViewModel(CryptoCurrencyRepository cryptoCurrencyRepository) {
        fromCurrencyNameLiveData = new MutableLiveData<>();
        setOptionsDependencies(cryptoCurrencyRepository);
    }

    private void setOptionsDependencies(final CryptoCurrencyRepository cryptoCurrencyRepository) {
        cryptoCurrencyListLiveData = Transformations.switchMap(fromCurrencyNameLiveData, new Function<String, LiveData<Resource<List<CryptoCurrency>>>>() {

            @Override
            public LiveData<Resource<List<CryptoCurrency>>> apply(String input) {
                return cryptoCurrencyRepository.fetchSimpleCryptoCurrencyListByFullName(input);
            }
        });
    }

    public void setFromCurrencyNameLiveData(String fromCurrencyName) {
        fromCurrencyNameLiveData.setValue(fromCurrencyName);
    }

    public LiveData<Resource<List<CryptoCurrency>>> getCryptoCurrencyListLiveData() {
        return cryptoCurrencyListLiveData;
    }
}
