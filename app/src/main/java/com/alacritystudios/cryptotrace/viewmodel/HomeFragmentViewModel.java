package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.alacritystudios.cryptotrace.application.CryptoTraceApplication;
import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.repository.ExchangeRepository;
import com.alacritystudios.cryptotrace.repository.FiatCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.util.PreferenceUtil;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPrice;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         View model to manipulate the Home Fragment.
 */

public class HomeFragmentViewModel extends AndroidViewModel {

    private MediatorLiveData<HomeFragmentQueryParams> homeFragmentQueryParamsMediatorLiveData;
    private MutableLiveData<String> toCurrencyNameLiveData;
    private MutableLiveData<String> fromExchangeNameLiveData;
    private LiveData<Resource<List<CryptoCurrencyPrice>>> results;
    private LiveData<List<CryptoCurrency>> favoriteCryptoCurrencyLiveData;
    private LiveData<List<String>> fromCurrencySymbolsLiveData;
    private LiveData<FiatCurrency> toCurrencyDetailsLiveData;
    private LiveData<Exchange> fromExchangeDetailsLiveData;
    List<String> fromSymbolsList;

    @Inject
    public HomeFragmentViewModel(CryptoTraceApplication cryptoTraceApplication, final CryptoCurrencyRepository cryptoCurrencyRepository,
                                 final FiatCurrencyRepository fiatCurrencyRepository, final ExchangeRepository exchangeRepository) {
        super(cryptoTraceApplication);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getApplication());
        homeFragmentQueryParamsMediatorLiveData = new MediatorLiveData<>();
        fromSymbolsList = new ArrayList<String>();
        results = new MediatorLiveData<>();
        fromCurrencySymbolsLiveData = new MediatorLiveData<>();
        toCurrencyNameLiveData = new MutableLiveData<>();
        toCurrencyDetailsLiveData = new MutableLiveData<>();
        fromExchangeNameLiveData = new MutableLiveData<>();
        fromExchangeDetailsLiveData = new MutableLiveData<>();
        setupDisplayDependencies(sharedPreferences, fiatCurrencyRepository, exchangeRepository);
        toCurrencyNameLiveData.setValue(sharedPreferences.getString(PreferenceUtil.PREF_CONVERSION_CURRENCY_CHOICE, "USD"));
        fromExchangeNameLiveData.setValue(sharedPreferences.getString(PreferenceUtil.PREF_EXCHANGE_CHOICE, "CCCAGG"));
        setupPreferenceDependencies(sharedPreferences, fiatCurrencyRepository, exchangeRepository);
        favoriteCryptoCurrencyLiveData = cryptoCurrencyRepository.fetchFavoriteCryptoCurrencyDetails();
        setupQueryDependencies(cryptoCurrencyRepository);
    }

    private void setupPreferenceDependencies(final SharedPreferences sharedPreferences, final FiatCurrencyRepository fiatCurrencyRepository, final ExchangeRepository exchangeRepository) {
        SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {

            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                if (key.equals(PreferenceUtil.PREF_CONVERSION_CURRENCY_CHOICE)) {
                    toCurrencyNameLiveData.setValue(prefs.getString(PreferenceUtil.PREF_CONVERSION_CURRENCY_CHOICE, "USD"));
                } else if (key.equals(PreferenceUtil.PREF_EXCHANGE_CHOICE)) {
                    fromExchangeNameLiveData.setValue(prefs.getString(PreferenceUtil.PREF_EXCHANGE_CHOICE, "CCCAGG"));
                }
            }
        };
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    private void setupDisplayDependencies(final SharedPreferences sharedPreferences, final FiatCurrencyRepository fiatCurrencyRepository, final ExchangeRepository exchangeRepository) {
        toCurrencyDetailsLiveData = Transformations.switchMap(toCurrencyNameLiveData, new Function<String, LiveData<FiatCurrency>>() {

            @Override
            public LiveData<FiatCurrency> apply(String input) {
                return fiatCurrencyRepository.fetchFiatCurrencyBySymbol(sharedPreferences.getString(PreferenceUtil.PREF_CONVERSION_CURRENCY_CHOICE, "USD"));
            }
        });

        fromExchangeDetailsLiveData = Transformations.switchMap(fromExchangeNameLiveData, new Function<String, LiveData<Exchange>>() {

            @Override
            public LiveData<Exchange> apply(String input) {
                return exchangeRepository.getExchangeDetailsByExchangeSymbol(sharedPreferences.getString(PreferenceUtil.PREF_EXCHANGE_CHOICE, "CCCAGG"));
            }
        });
    }

    private void setupQueryDependencies(final CryptoCurrencyRepository cryptoCurrencyRepository) {
        HomeFragmentQueryParams homeFragmentQueryParams = new HomeFragmentQueryParams();
        homeFragmentQueryParams.exchange = fromExchangeNameLiveData.getValue();
        homeFragmentQueryParams.toSymbol = toCurrencyNameLiveData.getValue();
        homeFragmentQueryParamsMediatorLiveData.setValue(homeFragmentQueryParams);
        homeFragmentQueryParamsMediatorLiveData.addSource(fromExchangeNameLiveData, new Observer<String>() {

            @Override
            public void onChanged(@Nullable String exchange) {
                if (homeFragmentQueryParamsMediatorLiveData.getValue() != null) {
                    HomeFragmentQueryParams homeFragmentQueryParams = homeFragmentQueryParamsMediatorLiveData.getValue();
                    homeFragmentQueryParams.exchange = exchange;
                    homeFragmentQueryParamsMediatorLiveData.setValue(homeFragmentQueryParams);
                }
            }
        });

        homeFragmentQueryParamsMediatorLiveData.addSource(toCurrencyNameLiveData, new Observer<String>() {

            @Override
            public void onChanged(@Nullable String toCurrency) {
                if (homeFragmentQueryParamsMediatorLiveData.getValue() != null) {
                    HomeFragmentQueryParams homeFragmentQueryParams = homeFragmentQueryParamsMediatorLiveData.getValue();
                    homeFragmentQueryParams.toSymbol = toCurrency;
                    homeFragmentQueryParamsMediatorLiveData.setValue(homeFragmentQueryParams);
                }
            }
        });

        homeFragmentQueryParamsMediatorLiveData.addSource(favoriteCryptoCurrencyLiveData, new Observer<List<CryptoCurrency>>() {

            @Override
            public void onChanged(@Nullable List<CryptoCurrency> cryptoCurrencies) {
                if (homeFragmentQueryParamsMediatorLiveData.getValue() != null && cryptoCurrencies != null) {
                    HomeFragmentQueryParams homeFragmentQueryParams = homeFragmentQueryParamsMediatorLiveData.getValue();
                    homeFragmentQueryParams.favoriteCryptoCurrencies = cryptoCurrencies;
                    List<String> fromSymbols = new ArrayList<>();
                    for (CryptoCurrency cryptoCurrency : cryptoCurrencies) {
                        fromSymbols.add(cryptoCurrency.symbol);
                    }
                    homeFragmentQueryParams.fromSymbols = fromSymbols.size() > 0 ? TextUtils.join(",", fromSymbols) : "";
                    homeFragmentQueryParamsMediatorLiveData.setValue(homeFragmentQueryParams);
                }
            }
        });

        favoriteCryptoCurrencyLiveData = cryptoCurrencyRepository.fetchFavoriteCryptoCurrencyDetails();

        results = Transformations.switchMap(homeFragmentQueryParamsMediatorLiveData, new Function<HomeFragmentQueryParams, LiveData<Resource<List<CryptoCurrencyPrice>>>>() {

            @Override
            public LiveData<Resource<List<CryptoCurrencyPrice>>> apply(HomeFragmentQueryParams input) {
                return cryptoCurrencyRepository.fetchFavoriteCryptoCurrencyPrices(input);
            }
        });
    }


    public void setToCurrencyNameLiveData(String toCurrency) {
        toCurrencyNameLiveData.setValue(toCurrency);
    }

    public void setFromExchangeNameLiveData(String fromExchange) {
        fromExchangeNameLiveData.setValue(fromExchange);
    }

    public LiveData<Resource<List<CryptoCurrencyPrice>>> getResults() {
        return results;
    }

    public LiveData<FiatCurrency> getToCurrencyDetailsLiveData() {
        return toCurrencyDetailsLiveData;
    }

    public LiveData<Exchange> getFromExchangeDetailsLiveData() {
        return fromExchangeDetailsLiveData;
    }

    public static class HomeFragmentQueryParams {

        public String exchange;
        public String fromSymbols;
        public String toSymbol;
        public List<CryptoCurrency> favoriteCryptoCurrencies;


        public HomeFragmentQueryStatus getQueryStatus() {
            if (exchange != null && fromSymbols != null && toSymbol != null && favoriteCryptoCurrencies != null) {
                if (favoriteCryptoCurrencies.size() == 0) {
                    return HomeFragmentQueryStatus.NO_FAVORITES;
                } else {
                    return HomeFragmentQueryStatus.READY;
                }
            } else {
                return HomeFragmentQueryStatus.NOT_READY;
            }
        }

        public static enum HomeFragmentQueryStatus {

            READY,
            NO_FAVORITES,
            NOT_READY
        }
    }
}
