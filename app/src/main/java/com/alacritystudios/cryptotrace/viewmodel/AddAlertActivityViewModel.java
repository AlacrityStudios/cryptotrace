package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.ui.dialog.SelectParamsViewModelListener;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         A view model to assist in selecting the currencies to create alerts for.
 */

public class AddAlertActivityViewModel extends ViewModel implements SelectParamsViewModelListener{


    private MutableLiveData<AddAlertActivityQueryParams> params;

    private MutableLiveData<AddAlertActivityBreadCrumb> breadCrumbLiveData;

    @Inject
    public AddAlertActivityViewModel() {
        breadCrumbLiveData = new MutableLiveData<>();
        params = new MutableLiveData<>();
        params.setValue(new AddAlertActivityViewModel.AddAlertActivityQueryParams());
    }

    public static class AddAlertActivityQueryParams {

        public CryptoCurrency cryptoCurrency;
        public FiatCurrency fiatCurrency;
        public Exchange exchange;
        public Double price;

        public enum AddAlertActivityQueryParamsStatus {
            READY,
            NOT_READY
        }

        public AddAlertActivityViewModel.AddAlertActivityQueryParams.AddAlertActivityQueryParamsStatus getQueryParamsStatus() {
            if (cryptoCurrency == null || fiatCurrency == null || exchange == null|| price == null) {
                return AddAlertActivityViewModel.AddAlertActivityQueryParams.AddAlertActivityQueryParamsStatus.NOT_READY;
            } else {
                return AddAlertActivityViewModel.AddAlertActivityQueryParams.AddAlertActivityQueryParamsStatus.READY;
            }
        }
    }

    public MutableLiveData<AddAlertActivityQueryParams> getParams() {
        return params;
    }

    public MutableLiveData<AddAlertActivityBreadCrumb> getBreadCrumbLiveData() {
        return breadCrumbLiveData;
    }

    @Override
    public void setFromCurrency(CryptoCurrency cryptoCurrency) {
        AddAlertActivityViewModel.AddAlertActivityQueryParams addAlertFragmentQueryParams = params.getValue();
        if (addAlertFragmentQueryParams != null) {
            addAlertFragmentQueryParams.cryptoCurrency = cryptoCurrency;
            params.setValue(addAlertFragmentQueryParams);
        }
    }

    @Override
    public void setToCurrency(FiatCurrency fiatCurrency) {
        AddAlertActivityViewModel.AddAlertActivityQueryParams addAlertFragmentQueryParams = params.getValue();
        if (addAlertFragmentQueryParams != null) {
            addAlertFragmentQueryParams.fiatCurrency = fiatCurrency;
            params.setValue(addAlertFragmentQueryParams);
        }
    }

    @Override
    public void setExchange(Exchange exchange) {
        AddAlertActivityViewModel.AddAlertActivityQueryParams addAlertFragmentQueryParams = params.getValue();
        if (addAlertFragmentQueryParams != null) {
            addAlertFragmentQueryParams.exchange = exchange;
            params.setValue(addAlertFragmentQueryParams);
        }
    }

    public void setPrice(Double price) {
        AddAlertActivityViewModel.AddAlertActivityQueryParams addAlertFragmentQueryParams = params.getValue();
        if (addAlertFragmentQueryParams != null) {
            addAlertFragmentQueryParams.price = price;
            params.setValue(addAlertFragmentQueryParams);
        }
    }

    public void setBreadCrumbLiveData(AddAlertActivityBreadCrumb breadCrumb) {
        breadCrumbLiveData.setValue(breadCrumb);
    }

    public enum AddAlertActivityBreadCrumb {

        SELECT_CRYPTO_CURRENCY_FRAGMENT,
        SELECT_FIAT_CURRENCY_FRAGMENT,
        SELECT_EXCHANGE_CURRENCY_FRAGMENT,
        SELECT_PRICE_FRAGMENT
    }
}
