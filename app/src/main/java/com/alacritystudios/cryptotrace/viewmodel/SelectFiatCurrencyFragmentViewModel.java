package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.repository.FiatCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *      A viewmodel class to display a list of fiatcurrencies and select from them.
 */

public class SelectFiatCurrencyFragmentViewModel extends ViewModel {

    private MutableLiveData<String> toCurrencyNameLiveData;
    private LiveData<Resource<List<FiatCurrency>>> fiatCurrencyListLiveData;

    @Inject
    public SelectFiatCurrencyFragmentViewModel(FiatCurrencyRepository fiatCurrencyRepository) {
        toCurrencyNameLiveData = new MutableLiveData<>();
        setOptionsDependencies(fiatCurrencyRepository);
    }

    private void setOptionsDependencies(final FiatCurrencyRepository fiatCurrencyRepository) {
        fiatCurrencyListLiveData = Transformations.switchMap(toCurrencyNameLiveData, new Function<String, LiveData<Resource<List<FiatCurrency>>>>() {

            @Override
            public LiveData<Resource<List<FiatCurrency>>> apply(String input) {
                return fiatCurrencyRepository.fetchSimpleFiatCurrencyListByFiatCurrencyName(input);
            }
        });
    }

    public void setToCurrencyNameLiveData(String fromCurrencyName) {
        toCurrencyNameLiveData.setValue(fromCurrencyName);
    }

    public LiveData<Resource<List<FiatCurrency>>> getFiatCurrencyListLiveData() {
        return fiatCurrencyListLiveData;
    }
}
