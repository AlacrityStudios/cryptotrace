package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedPriceDetails;
import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         ViewModel for fragment showing price of crypto-currrency.
 */

public class CryptoCurrencyPriceFragmentViewModel extends ViewModel {

    private LiveData<Resource<CryptoCurrencyFormattedPriceDetails>> resourceLiveData;

    private final MutableLiveData<CryptoCurrencyPriceInformationParams> cryptoCurrencyPriceInformationParamsLiveData;


    @Inject
    public CryptoCurrencyPriceFragmentViewModel(CryptoCurrencyRepository cryptoCurrencyRepository) {
        cryptoCurrencyPriceInformationParamsLiveData = new MutableLiveData<>();
        cryptoCurrencyPriceInformationParamsLiveData.setValue(new CryptoCurrencyPriceInformationParams());
        setupDataRelationships(cryptoCurrencyRepository);
    }

    public void setupDataRelationships(final CryptoCurrencyRepository cryptoCurrencyRepository) {
        resourceLiveData = Transformations.switchMap(cryptoCurrencyPriceInformationParamsLiveData, new Function<CryptoCurrencyPriceInformationParams, LiveData<Resource<CryptoCurrencyFormattedPriceDetails>>>() {

            @Override
            public LiveData<Resource<CryptoCurrencyFormattedPriceDetails>> apply(CryptoCurrencyPriceInformationParams input) {
                return cryptoCurrencyRepository.fetchCryptoCurrencyPriceDetails(input);
            }
        });
    }

    public void setToCurrencyLiveData(String toCurrency) {
        CryptoCurrencyPriceInformationParams cryptoCurrencyPriceInformationParams = cryptoCurrencyPriceInformationParamsLiveData.getValue();
        if (cryptoCurrencyPriceInformationParams != null) {
            cryptoCurrencyPriceInformationParams.toCurrency = toCurrency;
        }
        cryptoCurrencyPriceInformationParamsLiveData.setValue(cryptoCurrencyPriceInformationParams);
    }

    public void setFromCurrencyLiveData(String fromCurrency) {
        CryptoCurrencyPriceInformationParams cryptoCurrencyPriceInformationParams = cryptoCurrencyPriceInformationParamsLiveData.getValue();
        if (cryptoCurrencyPriceInformationParams != null) {
            cryptoCurrencyPriceInformationParams.fromCurrency = fromCurrency;
        }
        cryptoCurrencyPriceInformationParamsLiveData.setValue(cryptoCurrencyPriceInformationParams);
    }

    public void setExchangeLiveData(String exchange) {
        CryptoCurrencyPriceInformationParams cryptoCurrencyPriceInformationParams = cryptoCurrencyPriceInformationParamsLiveData.getValue();
        if (cryptoCurrencyPriceInformationParams != null) {
            cryptoCurrencyPriceInformationParams.exchange = exchange;
        }
        cryptoCurrencyPriceInformationParamsLiveData.setValue(cryptoCurrencyPriceInformationParams);
    }

    public LiveData<Resource<CryptoCurrencyFormattedPriceDetails>> getResourceLiveData() {
        return resourceLiveData;
    }

    public void reloadData() {
        CryptoCurrencyPriceInformationParams cryptoCurrencyPriceInformationParams = cryptoCurrencyPriceInformationParamsLiveData.getValue();
        cryptoCurrencyPriceInformationParamsLiveData.setValue(cryptoCurrencyPriceInformationParams);
    }

    public static class CryptoCurrencyPriceInformationParams {

        public String fromCurrency;
        public String toCurrency;
        public String exchange;

        public CryptoCurrencyPriceInformationParamsStatus getStatusOfParams() {
            if (fromCurrency != null && toCurrency != null && exchange != null && fromCurrency.length() > 0 && toCurrency.length() > 0 && exchange.length() > 0) {
                return CryptoCurrencyPriceInformationParamsStatus.READY;
            } else {
                return CryptoCurrencyPriceInformationParamsStatus.NOT_READY;
            }
        }

        public enum CryptoCurrencyPriceInformationParamsStatus {

            READY,
            NOT_READY
        }
    }
}