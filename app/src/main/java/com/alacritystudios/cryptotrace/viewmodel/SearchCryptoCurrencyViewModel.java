package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         View model for searching cryptocurrencies.
 */

public class SearchCryptoCurrencyViewModel extends ViewModel {

    CryptoCurrencyRepository cryptoCurrencyRepository;
    MutableLiveData<String> query = new MutableLiveData<>();
    LiveData<Resource<List<CryptoCurrency>>> listLiveData = new MediatorLiveData<>();

    @Inject
    public SearchCryptoCurrencyViewModel(final CryptoCurrencyRepository cryptoCurrencyRepository) {
        this.cryptoCurrencyRepository = cryptoCurrencyRepository;
        listLiveData = Transformations.switchMap(query, new Function<String, LiveData<Resource<List<CryptoCurrency>>>>() {

            @Override
            public LiveData<Resource<List<CryptoCurrency>>> apply(String input) {
                return cryptoCurrencyRepository.fetchCryptoCurrencyListByFullName(input);
            }
        });
    }

    public MutableLiveData<String> getQuery() {
        return query;
    }

    public LiveData<Resource<List<CryptoCurrency>>> getListLiveData() {
        return listLiveData;
    }
}
