package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedSocialStatsDetails;
import com.alacritystudios.cryptotrace.repository.CryptoCurrencyRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencySocialStats;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         ViewModel for fragment showing social stats of crypto-currrency.
 */

public class CryptoCurrencySocialStatsFragmentViewModel extends ViewModel {

    LiveData<Resource<CryptoCurrencyFormattedSocialStatsDetails>> resultLiveData;

    MutableLiveData<String> cryptoCurrencyIdLiveData = new MutableLiveData<>();

    @Inject
    public CryptoCurrencySocialStatsFragmentViewModel(final CryptoCurrencyRepository cryptoCurrencyRepository) {
        resultLiveData = Transformations.switchMap(cryptoCurrencyIdLiveData, new Function<String, LiveData<Resource<CryptoCurrencyFormattedSocialStatsDetails>>>() {

            @Override
            public LiveData<Resource<CryptoCurrencyFormattedSocialStatsDetails>> apply(String input) {
                return cryptoCurrencyRepository.fetchCryptoCurrencySocialStats(input);
            }
        });
    }

    public LiveData<Resource<CryptoCurrencyFormattedSocialStatsDetails>> getResultLiveData() {
        return resultLiveData;
    }

    public void setFromCurrencyLiveData(String fromCurrency) {
        cryptoCurrencyIdLiveData.setValue(fromCurrency);
    }

    public void reloadData() {
        String fromCurrency = cryptoCurrencyIdLiveData.getValue();
        cryptoCurrencyIdLiveData.setValue(fromCurrency);
    }
}