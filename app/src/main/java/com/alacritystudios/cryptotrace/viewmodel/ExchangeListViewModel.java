package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;

import com.alacritystudios.cryptotrace.repository.ExchangeRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewobject.Exchange;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         ViewModel to persist exchange list data over configuration changes.
 */

public class ExchangeListViewModel extends ViewModel {

    private LiveData<Resource<List<Exchange>>> results = new MediatorLiveData<>();

    ExchangeRepository exchangeRepository;

    @Inject
    public ExchangeListViewModel(ExchangeRepository exchangeRepository) {
        this.exchangeRepository = exchangeRepository;
        results = exchangeRepository.fetchExchangeList();
    }

    public LiveData<Resource<List<Exchange>>> getResults() {
        return results;
    }
}
