package com.alacritystudios.cryptotrace.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.alacritystudios.cryptotrace.repository.ExchangeRepository;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.viewobject.Exchange;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *      A viewmodel class to display a list of exchanges and select from them.
 */

public class SelectExchangeFragmentViewModel extends ViewModel {

    private MutableLiveData<String> exchangeNameLiveData;
    private LiveData<Resource<List<Exchange>>> exchangeListLiveData;

    @Inject
    public SelectExchangeFragmentViewModel(ExchangeRepository exchangeRepository) {
        exchangeNameLiveData = new MutableLiveData<>();
        setOptionsDependencies(exchangeRepository);
    }

    private void setOptionsDependencies(final ExchangeRepository exchangeRepository) {
        exchangeListLiveData = Transformations.switchMap(exchangeNameLiveData, new Function<String, LiveData<Resource<List<Exchange>>>>() {

            @Override
            public LiveData<Resource<List<Exchange>>> apply(String input) {
                return exchangeRepository.fetchSimpleExchangeListByExchangeName(input);
            }
        });
    }

    public void setExchangeNameLiveData(String fromCurrencyName) {
        exchangeNameLiveData.setValue(fromCurrencyName);
    }

    public LiveData<Resource<List<Exchange>>> getExchangeListLiveData() {
        return exchangeListLiveData;
    }
}