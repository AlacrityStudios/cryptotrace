package com.alacritystudios.cryptotrace.repository;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.alacritystudios.cryptotrace.dao.CryptoCurrencyDao;
import com.alacritystudios.cryptotrace.dao.FavoriteCryptoCurrencyDao;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedPriceDetails;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedSocialStatsDetails;
import com.alacritystudios.cryptotrace.formattedviewobjects.CryptoCurrencyFormattedStatsWrapper;
import com.alacritystudios.cryptotrace.resource.ApiResponse;
import com.alacritystudios.cryptotrace.resource.LocallyBoundResource;
import com.alacritystudios.cryptotrace.resource.NetworkBoundCachedResource;
import com.alacritystudios.cryptotrace.resource.NetworkBoundResource;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.rest.CryptoCompareApiService;
import com.alacritystudios.cryptotrace.rest.CryptoCompareMinApiService;
import com.alacritystudios.cryptotrace.util.AppExecutor;
import com.alacritystudios.cryptotrace.util.CacheState;
import com.alacritystudios.cryptotrace.util.FormattingUtil;
import com.alacritystudios.cryptotrace.viewmodel.AddNewAlertActivityViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyPriceFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.CryptoCurrencyStatsFragmentViewModel;
import com.alacritystudios.cryptotrace.viewmodel.HomeFragmentViewModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrency;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyFormattedStats;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyHistogramWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyListWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPrice;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPriceCompleteDetailsWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencyPriceDisplayDetailsWrapperModel;
import com.alacritystudios.cryptotrace.viewobject.CryptoCurrencySocialStats;
import com.alacritystudios.cryptotrace.viewobject.FavoriteCryptoCurrency;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author Anuj Dutt
 *         Repository to perform data manipulations on CryptoCurrency List.
 */

public class CryptoCurrencyRepository {

    AppExecutor appExecutor;
    CryptoCurrencyDao cryptoCurrencyDao;
    FavoriteCryptoCurrencyDao favoriteCryptoCurrencyDao;
    CryptoCompareMinApiService cryptoCompareMinApiService;
    CryptoCompareApiService cryptoCompareApiService;
    CacheState cacheState;

    public CryptoCurrencyRepository(AppExecutor appExecutor, CryptoCurrencyDao cryptoCurrencyDao,
                                    FavoriteCryptoCurrencyDao favoriteCryptoCurrencyDao, CryptoCompareMinApiService cryptoCompareMinApiService,
                                    CryptoCompareApiService cryptoCompareApiService, CacheState cacheState) {
        this.appExecutor = appExecutor;
        this.cryptoCurrencyDao = cryptoCurrencyDao;
        this.favoriteCryptoCurrencyDao = favoriteCryptoCurrencyDao;
        this.cryptoCompareMinApiService = cryptoCompareMinApiService;
        this.cryptoCompareApiService = cryptoCompareApiService;
        this.cacheState = cacheState;
    }

    public LiveData<Resource<List<CryptoCurrency>>> updateAndFetchCryptoCurrencyList() {
        return new NetworkBoundCachedResource<CryptoCurrencyListWrapperModel, List<CryptoCurrency>>(appExecutor) {

            @NonNull
            @Override
            public LiveData<List<CryptoCurrency>> loadFromDb() {
                return cryptoCurrencyDao.fetchBasicCryptoCurrencyList();
            }

            @Override
            public boolean shouldFetchFromNetwork() {
                return ! cacheState.isCryptoCurrencyListUpdated();
            }

            @NonNull
            @Override
            public LiveData<ApiResponse<CryptoCurrencyListWrapperModel>> createCall() {
                return cryptoCompareMinApiService.getCryptoCurrencyList();
            }

            @Override
            public void saveCallResult(CryptoCurrencyListWrapperModel cryptoCurrencyListWrapperModel) {
                cryptoCurrencyDao.saveCryptoCurrencyList(new ArrayList<CryptoCurrency>(cryptoCurrencyListWrapperModel.data.values()));
                cacheState.setCryptoCurrencyListUpdated(true);
            }

        }.asLiveData();
    }

    public LiveData<Resource<List<CryptoCurrency>>> fetchCryptoCurrencyListByFullName(final String query) {
        return new LocallyBoundResource<List<CryptoCurrency>>(appExecutor) {

            @Override
            public LiveData<List<CryptoCurrency>> loadFromDb() {
                return cryptoCurrencyDao.getCryptoCurrencyListByFullName(query);
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<CryptoCurrency>>> fetchSimpleCryptoCurrencyListByFullName(final String query) {
        return new NetworkBoundCachedResource<CryptoCurrencyListWrapperModel, List<CryptoCurrency>>(appExecutor) {

            @NonNull
            @Override
            public LiveData<List<CryptoCurrency>> loadFromDb() {
                return cryptoCurrencyDao.getSimpleCryptoCurrencyListByFullName(query);
            }

            @NonNull
            @Override
            public LiveData<ApiResponse<CryptoCurrencyListWrapperModel>> createCall() {
                return cryptoCompareMinApiService.getCryptoCurrencyList();
            }

            @Override
            public boolean shouldFetchFromNetwork() {
                return ! cacheState.isCryptoCurrencyListUpdated();
            }

            @Override
            public void saveCallResult(CryptoCurrencyListWrapperModel cryptoCurrencyListWrapperModel) {
                cryptoCurrencyDao.saveCryptoCurrencyList(new ArrayList<CryptoCurrency>(cryptoCurrencyListWrapperModel.data.values()));
                cacheState.setCryptoCurrencyListUpdated(true);
            }
        }.asLiveData();
    }

    public void addCryptoCurrencyToFavoritesList(final FavoriteCryptoCurrency favoriteCryptoCurrency) {
        appExecutor.diskIO.execute(new Runnable() {

            @Override
            public void run() {
                favoriteCryptoCurrencyDao.saveFavoriteCryptoCurrency(favoriteCryptoCurrency);
            }
        });
    }

    public void deleteCryptoCurrencyFromFavoritesList(final FavoriteCryptoCurrency favoriteCryptoCurrency) {
        appExecutor.diskIO.execute(new Runnable() {

            @Override
            public void run() {
                favoriteCryptoCurrencyDao.deleteFavoriteCryptoCurrency(favoriteCryptoCurrency.cryptoCurrencyId);
            }
        });
    }

    public LiveData<List<FavoriteCryptoCurrency>> fetchFavoriteCryptoCurrencies() {
        return favoriteCryptoCurrencyDao.getAllFavoriteCryptoCurrencies();
    }

    public LiveData<List<CryptoCurrency>> fetchFavoriteCryptoCurrencyDetails() {
        return cryptoCurrencyDao.getAllFavoriteCryptoCurrencies();
    }

    public LiveData<CryptoCurrency> fetchFavoriteCryptoCurrencyDetailsById(String id) {
        return cryptoCurrencyDao.fetchFavoriteCryptoCurrencyDetailsById(id);
    }

    public LiveData<Resource<CryptoCurrencyFormattedStatsWrapper>> fetchCryptoCurrencyStatsByMinute(
            final CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams, final int limit) {

        return new NetworkBoundResource<CryptoCurrencyFormattedStatsWrapper, CryptoCurrencyHistogramWrapperModel>(appExecutor) {

            @Override
            public LiveData<ApiResponse<CryptoCurrencyHistogramWrapperModel>> createCall() {
                return cryptoCompareMinApiService.getCryptoCurrencyVariationsPerMinute(cryptoCurrencyStatsFragmentParams.fromCurrency,
                        cryptoCurrencyStatsFragmentParams.toCurrency, cryptoCurrencyStatsFragmentParams.exchange, limit);
            }

            @Override
            public void processNetworkCallResponse(final ApiResponse<CryptoCurrencyHistogramWrapperModel> cryptoCurrencyHistogramWrapperModelApiResponse) {

                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        if (cryptoCurrencyHistogramWrapperModelApiResponse.isSuccessful() && cryptoCurrencyHistogramWrapperModelApiResponse.body != null) {
                            postValue(Resource.success(FormattingUtil.getFormattedCryptoCurrencyStatsWrapper(cryptoCurrencyHistogramWrapperModelApiResponse.body, CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice._1H)));
                        } else {
                            postValue(Resource.error(cryptoCurrencyHistogramWrapperModelApiResponse.errorMessage, CryptoCurrencyFormattedStatsWrapper.getInstance()));
                        }
                    }
                });
            }

            @Override
            public void setInitialData() {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        postValue(Resource.loading(CryptoCurrencyFormattedStatsWrapper.getInstance()));
                    }
                });

            }

            @Override
            public boolean shouldFetchDataFromNetwork() {
                return cryptoCurrencyStatsFragmentParams.getStatusOfParams() == CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyStatsFragmentParamsStatus.READY;
            }
        }.asLiveData();
    }

    public LiveData<Resource<CryptoCurrencyFormattedStatsWrapper>> fetchCryptoCurrencyStatsByHour(
            final CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams, final int limit) {

        return new NetworkBoundResource<CryptoCurrencyFormattedStatsWrapper, CryptoCurrencyHistogramWrapperModel>(appExecutor) {

            @Override
            public LiveData<ApiResponse<CryptoCurrencyHistogramWrapperModel>> createCall() {
                return cryptoCompareMinApiService.getCryptoCurrencyVariationsPerHour(cryptoCurrencyStatsFragmentParams.fromCurrency,
                        cryptoCurrencyStatsFragmentParams.toCurrency, cryptoCurrencyStatsFragmentParams.exchange, limit);
            }

            @Override
            public void processNetworkCallResponse(final ApiResponse<CryptoCurrencyHistogramWrapperModel> cryptoCurrencyHistogramWrapperModelApiResponse) {

                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        if (cryptoCurrencyHistogramWrapperModelApiResponse.isSuccessful() && cryptoCurrencyHistogramWrapperModelApiResponse.body != null) {
                            postValue(Resource.success(FormattingUtil.getFormattedCryptoCurrencyStatsWrapper(cryptoCurrencyHistogramWrapperModelApiResponse.body, CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyHistogramChoice._1D)));
                        } else {
                            postValue(Resource.error(cryptoCurrencyHistogramWrapperModelApiResponse.errorMessage, CryptoCurrencyFormattedStatsWrapper.getInstance()));
                        }
                    }
                });
            }

            @Override
            public void setInitialData() {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        postValue(Resource.loading(CryptoCurrencyFormattedStatsWrapper.getInstance()));
                    }
                });

            }

            @Override
            public boolean shouldFetchDataFromNetwork() {
                return cryptoCurrencyStatsFragmentParams.getStatusOfParams() == CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyStatsFragmentParamsStatus.READY;
            }
        }.asLiveData();
    }

    public LiveData<Resource<CryptoCurrencyFormattedStatsWrapper>> fetchCryptoCurrencyStatsByDay(
            final CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams cryptoCurrencyStatsFragmentParams, final int limit) {

        return new NetworkBoundResource<CryptoCurrencyFormattedStatsWrapper, CryptoCurrencyHistogramWrapperModel>(appExecutor) {

            @Override
            public LiveData<ApiResponse<CryptoCurrencyHistogramWrapperModel>> createCall() {
                return cryptoCompareMinApiService.getCryptoCurrencyVariationsPerDay(cryptoCurrencyStatsFragmentParams.fromCurrency,
                        cryptoCurrencyStatsFragmentParams.toCurrency, cryptoCurrencyStatsFragmentParams.exchange, limit);
            }

            @Override
            public void processNetworkCallResponse(final ApiResponse<CryptoCurrencyHistogramWrapperModel> cryptoCurrencyHistogramWrapperModelApiResponse) {

                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        if (cryptoCurrencyHistogramWrapperModelApiResponse.isSuccessful() && cryptoCurrencyHistogramWrapperModelApiResponse.body != null) {
                            postValue(Resource.success(FormattingUtil.getFormattedCryptoCurrencyStatsWrapper(cryptoCurrencyHistogramWrapperModelApiResponse.body, cryptoCurrencyStatsFragmentParams.choice)));
                        } else {
                            postValue(Resource.error(cryptoCurrencyHistogramWrapperModelApiResponse.errorMessage, CryptoCurrencyFormattedStatsWrapper.getInstance()));
                        }
                    }
                });
            }

            @Override
            public void setInitialData() {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        postValue(Resource.loading(CryptoCurrencyFormattedStatsWrapper.getInstance()));
                    }
                });

            }

            @Override
            public boolean shouldFetchDataFromNetwork() {
                return cryptoCurrencyStatsFragmentParams.getStatusOfParams() == CryptoCurrencyStatsFragmentViewModel.CryptoCurrencyStatsFragmentParams.CryptoCurrencyStatsFragmentParamsStatus.READY;
            }
        }.asLiveData();
    }

    public LiveData<Resource<CryptoCurrencyFormattedPriceDetails>> fetchCryptoCurrencyPriceDetails(final CryptoCurrencyPriceFragmentViewModel.CryptoCurrencyPriceInformationParams cryptoCurrencyPriceInformationParams) {
        return new NetworkBoundResource<CryptoCurrencyFormattedPriceDetails, CryptoCurrencyPriceCompleteDetailsWrapperModel>(appExecutor) {

            @Override
            public LiveData<ApiResponse<CryptoCurrencyPriceCompleteDetailsWrapperModel>> createCall() {
                return cryptoCompareMinApiService.getCryptoCurrencyCompletePriceDetails(cryptoCurrencyPriceInformationParams.fromCurrency, cryptoCurrencyPriceInformationParams.toCurrency, cryptoCurrencyPriceInformationParams.exchange);
            }

            @Override
            public void processNetworkCallResponse(final ApiResponse<CryptoCurrencyPriceCompleteDetailsWrapperModel> completeDetailsWrapperModelApiResponse) {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        if (completeDetailsWrapperModelApiResponse.isSuccessful() && completeDetailsWrapperModelApiResponse.body != null) {
                            postValue(Resource.success(FormattingUtil.getFormattedCryptoCurrencyPrice(completeDetailsWrapperModelApiResponse.body, cryptoCurrencyPriceInformationParams)));
                        } else {
                            postValue(Resource.error(completeDetailsWrapperModelApiResponse.errorMessage, CryptoCurrencyFormattedPriceDetails.getClearedPriceInformation()));
                        }
                    }
                });
            }

            @Override
            public void setInitialData() {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        postValue(Resource.loading(CryptoCurrencyFormattedPriceDetails.getClearedPriceInformation()));
                    }
                });

            }

            @Override
            public boolean shouldFetchDataFromNetwork() {
                return cryptoCurrencyPriceInformationParams.getStatusOfParams() == CryptoCurrencyPriceFragmentViewModel.CryptoCurrencyPriceInformationParams.CryptoCurrencyPriceInformationParamsStatus.READY;
            }
        }.asLiveData();
    }

    @SuppressLint("StaticFieldLeak")
    public LiveData<Resource<CryptoCurrencyFormattedSocialStatsDetails>> fetchCryptoCurrencySocialStats(final String cryptoCurrencyId) {
        return new NetworkBoundResource<CryptoCurrencyFormattedSocialStatsDetails, CryptoCurrencySocialStats>(appExecutor) {

            @Override
            public LiveData<ApiResponse<CryptoCurrencySocialStats>> createCall() {
                return cryptoCompareApiService.getCryptoCurrencySocialStats(cryptoCurrencyId);
            }

            @Override
            public void processNetworkCallResponse(final ApiResponse<CryptoCurrencySocialStats> cryptoCurrencySocialStatsApiResponse) {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        if (cryptoCurrencySocialStatsApiResponse.isSuccessful()) {
                            if (cryptoCurrencySocialStatsApiResponse.body != null && cryptoCurrencySocialStatsApiResponse.body.data != null) {
                                postValue(Resource.<CryptoCurrencyFormattedSocialStatsDetails>success(FormattingUtil.getFormattedSocialStatsDetails(cryptoCurrencySocialStatsApiResponse.body.data)));
                            } else {
                                postValue(Resource.<CryptoCurrencyFormattedSocialStatsDetails>error("Some unknown API error occurred. Please try again after some time.", CryptoCurrencyFormattedSocialStatsDetails.getNewInstance()));
                            }
                        } else {
                            postValue(Resource.<CryptoCurrencyFormattedSocialStatsDetails>error(cryptoCurrencySocialStatsApiResponse.errorMessage, CryptoCurrencyFormattedSocialStatsDetails.getNewInstance()));
                        }
                    }
                });

            }

            @Override
            public void setInitialData() {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        postValue(Resource.<CryptoCurrencyFormattedSocialStatsDetails>loading(CryptoCurrencyFormattedSocialStatsDetails.getNewInstance()));
                    }
                });
            }

            @Override
            public boolean shouldFetchDataFromNetwork() {
                return true;
            }
        }.asLiveData();
    }

    public LiveData<Resource<Double>> fetchCryptoCurrencyPriceForAlert(final AddNewAlertActivityViewModel.AddNewAlertActivityQueryParams addNewAlertActivityQueryParams) {
        return new NetworkBoundResource<Double,HashMap<String, Double>>(appExecutor) {

            @Override
            public LiveData<ApiResponse<HashMap<String, Double>>> createCall() {
                return cryptoCompareMinApiService.getCryptoCurrencyPrice(addNewAlertActivityQueryParams.cryptoCurrency.symbol,
                        addNewAlertActivityQueryParams.fiatCurrency.name, addNewAlertActivityQueryParams.exchange.internalName);
            }

            @Override
            public void processNetworkCallResponse(final ApiResponse<HashMap<String, Double>> hashMapApiResponse) {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        if (hashMapApiResponse.isSuccessful() && hashMapApiResponse.body != null) {
                            try {
                                postValue(Resource.success(hashMapApiResponse.body.get(addNewAlertActivityQueryParams.fiatCurrency.name)));
                            } catch (Exception e) {
                                postValue(Resource.<Double>error("Please change exchange and try again.", null));
                            }
                        } else {
                            postValue(Resource.<Double>error("Please change exchange and try again.", null));
                        }
                    }
                });
            }

            @Override
            public void setInitialData() {
                postValue(Resource.<Double>loading(null));
            }

            @Override
            public boolean shouldFetchDataFromNetwork() {
                return addNewAlertActivityQueryParams.getQueryParamsStatus() == AddNewAlertActivityViewModel.AddNewAlertActivityQueryParams.AddNewAlertActivityQueryParamsStatus.READY;
            }
        }.asLiveData();
    }


    @SuppressLint("StaticFieldLeak")
    public LiveData<Resource<List<CryptoCurrencyPrice>>> fetchFavoriteCryptoCurrencyPrices(final HomeFragmentViewModel.HomeFragmentQueryParams homeFragmentQueryParams) {
        return new NetworkBoundResource<List<CryptoCurrencyPrice>, CryptoCurrencyPriceDisplayDetailsWrapperModel>(appExecutor) {

            @Override
            public LiveData<ApiResponse<CryptoCurrencyPriceDisplayDetailsWrapperModel>> createCall() {
                return cryptoCompareMinApiService.getCryptoCurrencyDisplayPriceDetails(homeFragmentQueryParams.fromSymbols, homeFragmentQueryParams.toSymbol);
            }

            @Override
            public void setInitialData() {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        List<CryptoCurrencyPrice> cryptoCurrencyPrices = new ArrayList<>();
                        if (homeFragmentQueryParams.getQueryStatus() == HomeFragmentViewModel.HomeFragmentQueryParams.HomeFragmentQueryStatus.READY) {
                            for (CryptoCurrency cryptoCurrency : homeFragmentQueryParams.favoriteCryptoCurrencies) {
                                CryptoCurrencyPrice cryptoCurrencyPrice = new CryptoCurrencyPrice();
                                cryptoCurrencyPrice.fromCurrencySymbol = cryptoCurrency.symbol;
                                cryptoCurrencyPrice.fromCurrencyName = cryptoCurrency.coinName;
                                cryptoCurrencyPrice.fromCurrencyId = cryptoCurrency.id;
                                cryptoCurrencyPrice.fromCurrencySymbolImageUrl = cryptoCurrency.imageUrl;
                                cryptoCurrencyPrice.price = "0.0";
                                cryptoCurrencyPrice.change = "0.0";
                                cryptoCurrencyPrice.changePercent = "0.0";
                                cryptoCurrencyPrice.toCurrencySymbol = "";
                                cryptoCurrencyPrices.add(cryptoCurrencyPrice);
                            }
                            postValue(Resource.<List<CryptoCurrencyPrice>>loading(cryptoCurrencyPrices));
                        } else if (homeFragmentQueryParams.getQueryStatus() == HomeFragmentViewModel.HomeFragmentQueryParams.HomeFragmentQueryStatus.NO_FAVORITES) {
                            postValue(Resource.<List<CryptoCurrencyPrice>>error("No favorite crypto-currencies. Please add favorite crypto-currencies.", cryptoCurrencyPrices));
                        }
                    }
                });
            }

            @Override
            public void processNetworkCallResponse(final ApiResponse<CryptoCurrencyPriceDisplayDetailsWrapperModel> hashMapApiResponse) {
                appExecutor.workerThread.execute(new Runnable() {

                    @Override
                    public void run() {
                        if (hashMapApiResponse.isSuccessful()) {
                            if (hashMapApiResponse.body != null) {
                                List<CryptoCurrencyPrice> cryptoCurrencyPrices = new ArrayList<>();
                                if (homeFragmentQueryParams.favoriteCryptoCurrencies != null && homeFragmentQueryParams.favoriteCryptoCurrencies.size() > 0) {
                                    for (CryptoCurrency cryptoCurrency : homeFragmentQueryParams.favoriteCryptoCurrencies) {
                                        CryptoCurrencyPrice cryptoCurrencyPrice = new CryptoCurrencyPrice();
                                        cryptoCurrencyPrice.fromCurrencySymbol = cryptoCurrency.symbol;
                                        cryptoCurrencyPrice.fromCurrencyName = cryptoCurrency.coinName;
                                        cryptoCurrencyPrice.fromCurrencyId = cryptoCurrency.id;
                                        cryptoCurrencyPrice.fromCurrencySymbolImageUrl = cryptoCurrency.imageUrl;
                                        try {
                                            cryptoCurrencyPrice.price = hashMapApiResponse.body.displayCryptoCurrencyDetails.get(cryptoCurrency.symbol).get(homeFragmentQueryParams.toSymbol).price;
                                        } catch (Exception ex) {
                                            cryptoCurrencyPrice.price = "0.0";
                                        }
                                        try {
                                            cryptoCurrencyPrice.change = hashMapApiResponse.body.displayCryptoCurrencyDetails.get(cryptoCurrency.symbol).get(homeFragmentQueryParams.toSymbol).change24Hour;
                                        } catch (Exception ex) {
                                            cryptoCurrencyPrice.change = "0.0";
                                        }
                                        try {
                                            cryptoCurrencyPrice.changePercent = hashMapApiResponse.body.displayCryptoCurrencyDetails.get(cryptoCurrency.symbol).get(homeFragmentQueryParams.toSymbol).changePct24Hour;
                                        } catch (Exception ex) {
                                            cryptoCurrencyPrice.changePercent = "0.0";
                                        }
                                        try {
                                            cryptoCurrencyPrice.toCurrencySymbol = hashMapApiResponse.body.displayCryptoCurrencyDetails.get(cryptoCurrency.symbol).get(homeFragmentQueryParams.toSymbol).toSymbol;
                                        } catch (Exception ex) {
                                            cryptoCurrencyPrice.toCurrencySymbol = "";
                                        }
                                        cryptoCurrencyPrices.add(cryptoCurrencyPrice);
                                    }
                                    postValue(Resource.success(cryptoCurrencyPrices));
                                } else {
                                    postValue(Resource.<List<CryptoCurrencyPrice>>error("No favorite crypto-currencies. Please add favorite crypto-currencies.", cryptoCurrencyPrices));
                                }
                            } else {
                                List<CryptoCurrencyPrice> cryptoCurrencyPrices = new ArrayList<>();
                                if (homeFragmentQueryParams.favoriteCryptoCurrencies != null && homeFragmentQueryParams.favoriteCryptoCurrencies.size() > 0) {
                                    for (CryptoCurrency cryptoCurrency : homeFragmentQueryParams.favoriteCryptoCurrencies) {
                                        CryptoCurrencyPrice cryptoCurrencyPrice = new CryptoCurrencyPrice();
                                        cryptoCurrencyPrice.fromCurrencySymbol = cryptoCurrency.symbol;
                                        cryptoCurrencyPrice.fromCurrencyName = cryptoCurrency.coinName;
                                        cryptoCurrencyPrice.fromCurrencyId = cryptoCurrency.id;
                                        cryptoCurrencyPrice.fromCurrencySymbolImageUrl = cryptoCurrency.imageUrl;
                                        cryptoCurrencyPrice.price = "0.0";
                                        cryptoCurrencyPrice.toCurrencySymbol = "";
                                        cryptoCurrencyPrices.add(cryptoCurrencyPrice);
                                    }
                                    postValue(Resource.error("Some unknown API error occurred. Please try again after some time.", cryptoCurrencyPrices));
                                } else {
                                    postValue(Resource.<List<CryptoCurrencyPrice>>error("No favorite crypto-currencies. Please add favorite crypto-currencies.", cryptoCurrencyPrices));
                                }
                            }
                        } else {
                            List<CryptoCurrencyPrice> cryptoCurrencyPrices = new ArrayList<>();
                            if (homeFragmentQueryParams.favoriteCryptoCurrencies != null && homeFragmentQueryParams.favoriteCryptoCurrencies.size() > 0) {
                                for (CryptoCurrency cryptoCurrency : homeFragmentQueryParams.favoriteCryptoCurrencies) {
                                    CryptoCurrencyPrice cryptoCurrencyPrice = new CryptoCurrencyPrice();
                                    cryptoCurrencyPrice.fromCurrencySymbol = cryptoCurrency.symbol;
                                    cryptoCurrencyPrice.fromCurrencyName = cryptoCurrency.coinName;
                                    cryptoCurrencyPrice.fromCurrencyId = cryptoCurrency.id;
                                    cryptoCurrencyPrice.fromCurrencySymbolImageUrl = cryptoCurrency.imageUrl;
                                    cryptoCurrencyPrice.price = "0.0";
                                    cryptoCurrencyPrice.change = "0.0";
                                    cryptoCurrencyPrice.changePercent = "0.0";
                                    cryptoCurrencyPrice.toCurrencySymbol = "";
                                    cryptoCurrencyPrices.add(cryptoCurrencyPrice);
                                }
                                postValue(Resource.error(hashMapApiResponse.errorMessage, cryptoCurrencyPrices));
                            } else {
                                postValue(Resource.<List<CryptoCurrencyPrice>>error("No favorite crypto-currencies. Please add favorite crypto-currencies.", cryptoCurrencyPrices));
                            }
                        }
                    }
                });
            }

            @Override
            public boolean shouldFetchDataFromNetwork() {
                return homeFragmentQueryParams.getQueryStatus() == HomeFragmentViewModel.HomeFragmentQueryParams.HomeFragmentQueryStatus.READY;
            }
        }.asLiveData();
    }


}
