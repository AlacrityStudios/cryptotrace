package com.alacritystudios.cryptotrace.repository;

import android.arch.lifecycle.LiveData;

import com.alacritystudios.cryptotrace.dao.AlertDao;
import com.alacritystudios.cryptotrace.dao.AlertDetailsDao;
import com.alacritystudios.cryptotrace.resource.LocallyBoundResource;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.util.AppExecutor;
import com.alacritystudios.cryptotrace.viewobject.Alert;
import com.alacritystudios.cryptotrace.viewobject.AlertDisplayDetails;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         A repository to perform database operations on the Alert table.
 */

public class AlertRepository {

    private AlertDao alertDao;
    private AlertDetailsDao alertDetailsDao;
    private AppExecutor appExecutor;

    @Inject
    public AlertRepository(AlertDao alertDao, AlertDetailsDao alertDetailsDao, AppExecutor appExecutor) {
        this.alertDao = alertDao;
        this.alertDetailsDao = alertDetailsDao;
        this.appExecutor = appExecutor;
    }

    public LiveData<Resource<List<Alert>>> fetchAlertList() {
        return new LocallyBoundResource<List<Alert>>(appExecutor) {

            @Override
            public LiveData<List<Alert>> loadFromDb() {
                return alertDao.fetchAlertList();
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<AlertDisplayDetails>>> fetchAlertDetailsList() {
        return new LocallyBoundResource<List<AlertDisplayDetails>>(appExecutor) {

            @Override
            public LiveData<List<AlertDisplayDetails>> loadFromDb() {
                return alertDetailsDao.fetchAllAlertDisplayDetails();
            }
        }.asLiveData();
    }

    public void saveNewAlert(final Alert alert) {
        appExecutor.diskIO.execute(new Runnable() {

            @Override
            public void run() {
                alertDao.insertAlert(alert);
            }
        });
    }
}
