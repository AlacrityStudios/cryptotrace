package com.alacritystudios.cryptotrace.repository;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.alacritystudios.cryptotrace.resource.Listing;
import com.alacritystudios.cryptotrace.rest.CryptoCompareMinApiService;
import com.alacritystudios.cryptotrace.rest.NewsListDataSource;
import com.alacritystudios.cryptotrace.rest.NewsListDataSourceFactory;
import com.alacritystudios.cryptotrace.util.AppExecutor;
import com.alacritystudios.cryptotrace.viewobject.News;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Anuj Dutt
 *  Repository to load News items from a Paged DataSource.
 */

public class NewsRepository {

    CryptoCompareMinApiService cryptoCompareMinApiService;
    AppExecutor appExecutor;

    @Inject
    public NewsRepository(@Named("Default") CryptoCompareMinApiService cryptoCompareMinApiService, AppExecutor appExecutor) {
        this.cryptoCompareMinApiService = cryptoCompareMinApiService;
        this.appExecutor = appExecutor;
    }

    public Listing<News> fetchNewsList(Long lesserThanTime) {
        final NewsListDataSourceFactory newsListDataSourceFactory = new NewsListDataSourceFactory(cryptoCompareMinApiService);
        PagedList.Config pagedListConfig = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(100)
                .setPageSize(50)
                .build();

        LiveData<PagedList<News>> pagedList = new LivePagedListBuilder<>(newsListDataSourceFactory, pagedListConfig)
                .setBackgroundThreadExecutor(appExecutor.networkIO)
                .build();

        LiveData<Listing.NetworkState> networkState = Transformations.switchMap(newsListDataSourceFactory.sourceLiveData, new Function<NewsListDataSource, LiveData<Listing.NetworkState>>() {

            @Override
            public LiveData<Listing.NetworkState> apply(NewsListDataSource input) {
                return input.networkState;
            }
        });

        LiveData<Listing.NetworkState> refreshState = Transformations.switchMap(newsListDataSourceFactory.sourceLiveData, new Function<NewsListDataSource, LiveData<Listing.NetworkState>>() {

            @Override
            public LiveData<Listing.NetworkState> apply(NewsListDataSource input) {
                return input.initialLoad;
            }
        });

        return new Listing<News>(pagedList, networkState, refreshState) {

            @Override
            public void refresh() {
                if (newsListDataSourceFactory.sourceLiveData.getValue() != null) {
                    newsListDataSourceFactory.sourceLiveData.getValue().invalidate();
                }
            }

            @Override
            public void retry() {

            }
        };
    }
}