package com.alacritystudios.cryptotrace.repository;

import android.arch.lifecycle.LiveData;

import com.alacritystudios.cryptotrace.dao.FiatCurrencyDao;
import com.alacritystudios.cryptotrace.resource.LocallyBoundResource;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.util.AppExecutor;
import com.alacritystudios.cryptotrace.viewobject.FiatCurrency;

import java.util.List;

/**
 * @author Anuj Dutt
 *         Repository to perform data manipulations on FiatCurrency List.
 */

public class FiatCurrencyRepository {

    public AppExecutor appExecutor;
    public FiatCurrencyDao fiatCurrencyDao;

    public FiatCurrencyRepository(AppExecutor appExecutor, FiatCurrencyDao fiatCurrencyDao) {
        this.appExecutor = appExecutor;
        this.fiatCurrencyDao = fiatCurrencyDao;
    }

    public LiveData<FiatCurrency> fetchFiatCurrencyBySymbol(String symbol) {
        return fiatCurrencyDao.fetchFiatCurrencyBySymbol(symbol);
    }

    public LiveData<Resource<List<FiatCurrency>>> fetchSimpleFiatCurrencyListByFiatCurrencyName(final String fiatCurrency) {
        return new LocallyBoundResource<List<FiatCurrency>>(appExecutor) {

            @Override
            public LiveData<List<FiatCurrency>> loadFromDb() {
                return fiatCurrencyDao.getSimpleFiatCurrencyListByName(fiatCurrency);
            }
        }.asLiveData();
    }
}
