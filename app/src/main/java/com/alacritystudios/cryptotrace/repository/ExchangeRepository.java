package com.alacritystudios.cryptotrace.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.alacritystudios.cryptotrace.dao.ExchangeDao;
import com.alacritystudios.cryptotrace.resource.ApiResponse;
import com.alacritystudios.cryptotrace.resource.NetworkBoundCachedResource;
import com.alacritystudios.cryptotrace.resource.Resource;
import com.alacritystudios.cryptotrace.rest.CryptoCompareApiService;
import com.alacritystudios.cryptotrace.util.AppExecutor;
import com.alacritystudios.cryptotrace.util.CacheState;
import com.alacritystudios.cryptotrace.viewobject.Exchange;
import com.alacritystudios.cryptotrace.viewobject.ExchangeListWrapperModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * @author Anuj Dutt
 *         A repository to perform database operations on the Exchange table.
 */

public class ExchangeRepository {

    ExchangeDao exchangeDao;
    CryptoCompareApiService cryptoCompareApiService;
    AppExecutor appExecutor;
    CacheState cacheState;

    @Inject
    public ExchangeRepository(ExchangeDao exchangeDao, CryptoCompareApiService cryptoCompareApiService, AppExecutor appExecutor, CacheState cacheState) {
        this.exchangeDao = exchangeDao;
        this.cryptoCompareApiService = cryptoCompareApiService;
        this.appExecutor = appExecutor;
        this.cacheState = cacheState;
    }

    public LiveData<Resource<List<Exchange>>> fetchExchangeList() {
        return new NetworkBoundCachedResource<ExchangeListWrapperModel, List<Exchange>>(appExecutor) {

            @Override
            public LiveData<List<Exchange>> loadFromDb() {
                return exchangeDao.fetchExchangeList();
            }

            @Override
            public boolean shouldFetchFromNetwork() {
                return !cacheState.isExchangeListUpdated();
            }

            @Override
            public LiveData<ApiResponse<ExchangeListWrapperModel>> createCall() {
                return cryptoCompareApiService.getExchangeList();
            }

            @Override
            public void saveCallResult(ExchangeListWrapperModel exchangeListWrapperModel) {
                exchangeDao.saveExchangeList(new ArrayList<Exchange>(exchangeListWrapperModel.data.values()));
            }
        }.asLiveData();
    }

    public LiveData<Exchange> getExchangeDetailsByExchangeSymbol(String exchangeSymbol) {
        return exchangeDao.fetchExchangeDetailsBySymbol(exchangeSymbol);
    }

    public LiveData<Resource<List<Exchange>>> getExchangeListByExchangeName(final String exchangeName) {
        return new NetworkBoundCachedResource<ExchangeListWrapperModel, List<Exchange>>(appExecutor) {

            @NonNull
            @Override
            public LiveData<List<Exchange>> loadFromDb() {
                return exchangeDao.fetchExchangeListByName(exchangeName);
            }

            @NonNull
            @Override
            public LiveData<ApiResponse<ExchangeListWrapperModel>> createCall() {
                return cryptoCompareApiService.getExchangeList();
            }

            @Override
            public boolean shouldFetchFromNetwork() {
                return !cacheState.isExchangeListUpdated();
            }

            @Override
            public void saveCallResult(ExchangeListWrapperModel exchangeListWrapperModel) {
                exchangeDao.saveExchangeList(new ArrayList<Exchange>(exchangeListWrapperModel.data.values()));
            }
        }.asLiveData();
    }


    public LiveData<Resource<List<Exchange>>> fetchSimpleExchangeListByExchangeName(final String exchangeName) {
        return new NetworkBoundCachedResource<ExchangeListWrapperModel, List<Exchange>>(appExecutor) {

            @NonNull
            @Override
            public LiveData<List<Exchange>> loadFromDb() {
                return exchangeDao.fetchSimpleExchangeListByName(exchangeName);
            }

            @NonNull
            @Override
            public LiveData<ApiResponse<ExchangeListWrapperModel>> createCall() {
                return cryptoCompareApiService.getExchangeList();
            }

            @Override
            public boolean shouldFetchFromNetwork() {
                return !cacheState.isExchangeListUpdated();
            }

            @Override
            public void saveCallResult(ExchangeListWrapperModel exchangeListWrapperModel) {
                exchangeDao.saveExchangeList(new ArrayList<Exchange>(exchangeListWrapperModel.data.values()));
            }
        }.asLiveData();
    }
}
